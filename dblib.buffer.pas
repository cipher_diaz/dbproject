unit dblib.buffer;

{$I 'dblib.inc'}

interface

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections
  {$IFDEF HEX_SUPPORT_VARIANTS}
  System.Variants
  {$ENDIF}
  {$IFDEF HEX_SUPPORT_ZLIB}
    , System.Zlib
  {$ENDIF}
  {$IFDEF HEX_SUPPORT_INTERNET}
    , IdZLibCompressorBase
    , IdCompressorZLib
    , IdBaseComponent
    , IdComponent
    , IdTCPConnection
    , IdTCPClient
    , IdHTTP
    , idMultipartFormData
  {$ENDIF}
  , dblib.common
  , dblib.encoder
  , dblib.bitbuffer
  ;

const
  // IO-cache size, IO is done in chunks of this size
  CNT_HEX_IOCACHE_SIZE = 1024 * 10;

type

  // Forward declarations
  TDbLibBuffer        = class;
  TDbLibStreamAdapter = class;

  // Custom exceptions
  EDbLibBufferError   = class(EDbLibError);
  EDbLibStreamAdapter = class(EDbLibError);

  // Event declarations
  TDbLibProcessBeginsEvent = procedure (const Sender: TObject; const Primary: integer) of object;
  TDbLibProcessUpdateEvent = procedure (const Sender: TObject; const Value, Primary: integer) of object;
  TDbLibProcessEndsEvent = procedure (const Sender: TObject; const Primary, Secondary: integer) of object;
  {$IFDEF HEX_SUPPORT_INTERNET}
  TDbLibMultipartCallback = reference to procedure(const Buffer: TDbLibBuffer; const Stream: TIdMultiPartFormDataStream);
  {$ENDIF}

  (* Buffer capability enum. Different buffer types can have different
     rules. Some buffers can be read only, depending on the application,
     while others may be static - meaning that it cannot grow in size *)
  TDbLibBufferCapabilities = set of (
      mcScale,  // Can scale
      mcOwned,  // Buffer originates elsewhere, dont explicitly release
      mcRead,   // Buffer can be read
      mcWrite   // Buffer can be written to
    );

  // Options for text output of buffer
  TDbLibDumpOptions = set of (
      hdSign,     // sign values with prefix
      hdZeroPad   // pad out of range bytes to match boundary
    );

  (* Data cache byte array type. Read and write operations process data in
     fixed chunks of this type *)
  TDbLibIOCache = packed array [1..CNT_HEX_IOCACHE_SIZE] of byte;

  // Custom 3byte record for our FillTriple class procedure
  TDbLibTriplebyte = packed record
    a:  byte;
    b:  byte;
    c:  byte;
  end;
  PDbTriplebyte = ^TDbLibTriplebyte;

  (* This class allows you to access a buffer regardless of how its
     implemented, through a normal stream. This makes it very easy to
     use buffers with standard VCL components and classes *)
  TDbLibStreamAdapter = class(TStream)
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  private
    FBufObj:    TDbLibBuffer;
    FOffset:    int64;

  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  protected
    function    GetSize: Int64; override;
    procedure   SetSize(const NewSize: Int64); override;

  public
    property    BufferObj: TDbLibBuffer read FBufObj;

    function    Read(var Buffer; Count: longint):  longint; override;
    function    Write(const Buffer;Count: longint):  longint; override;
    function    Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    constructor Create(const SourceBuffer: TDbLibBuffer); reintroduce;
  end;

  TDbLibStackOrientation = (
    soTopDown,  // Data is inserted / removed from the start of the buffer (costly!)
    soDownUp    // Data is appended / removed from the end of the buffer (fast!)
    );

  (* This is the basic buffer class. It is an abstract class and should not be
     created. Create objects from classes that decend from this and that
     implements the buffering you need to work with (memory, stream, temp etc *)
  TDbLibBuffer = class(TPersistent)
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  private
    // Buffer capabilities. I.E: Readable, writeable etc.
    FCaps: TDbLibBufferCapabilities;

    {$IFDEF HEX_SUPPORT_ZLIB}
    FOnCompressionBegins:   TDbLibProcessBeginsEvent;
    FOnCompressionUpdate:   TDbLibProcessUpdateEvent;
    FOnCompressionEnds:     TDbLibProcessEndsEvent;
    FOnDeCompressionBegins: TDbLibProcessBeginsEvent;
    FOnDeCompressionUpdate: TDbLibProcessUpdateEvent;
    FOnDeCompressionEnds:   TDbLibProcessEndsEvent;
    {$ENDIF}

    {$IFDEF HEX_SUPPORT_ENCRYPTION}
    FEncryption: TDbLibEncoder;
    {$ENDIF}

    procedure SetSize(const NewSize: Int64);
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  protected
    (*  Standard persistence. Please note that we call the function
        ObjectHasData() to determine if there is anything to save.
        See extended persistence below for more information.

        NOTE: Do not override these methods, use the functions defined
              in extended persistance when modifying this object *)
    procedure ReadObjBin(Stream: TStream);
    procedure WriteObjBin(Stream: TStream);
    procedure DefineProperties(Filer: TFiler); override;

    (*  Extended persistence.
        The function ObjectHasData() is called by the normal VCL
        DefineProperties to determine if there is any data to save.
        The other methods are invoked before and after either loading or
        saving object data.

        NOTE: To extend the functionality of this object please override
        ReadObject() and WriteObject(). The object will take care of
        everything else. *)
    function  ObjectHasData: boolean;
    procedure BeforeReadObject; virtual;
    procedure AfterReadObject; virtual;
    procedure BeforeWriteObject; virtual;
    procedure AfterWriteObject; virtual;
    procedure ReadObject(const Reader: TReader); virtual;
    procedure WriteObject(const Writer: TWriter); virtual;

    (* Call this to determine if the object is empty (holds no data) *)
    function GetEmpty: boolean; virtual;

    {$IFDEF HEX_SUPPORT_ENCRYPTION}
    procedure SetEncryption(const NewEncoder: TDbLibEncoder); virtual;
    {$ENDIF}

    (* Actual Buffer implementation. Decendants must override and
       implement these methods. It does not matter where or how the
       data is stored - this is up to the implementation. *)
    function  DoGetCapabilities: TDbLibBufferCapabilities; virtual;abstract;
    function  DoGetDataSize: Int64; virtual;abstract;
    procedure DoReleaseData; virtual;abstract;
    procedure DoGrowDataBy(const Value: integer); virtual;abstract;
    procedure DoShrinkDataBy(const Value: integer); virtual;abstract;
    procedure DoReadData(Start: Int64; var Buffer; BufLen: integer); virtual;abstract;
    procedure DoWriteData(Start: int64; const Buffer; BufLen: integer); virtual;abstract;
    procedure DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer); virtual;
    procedure DoZeroData; virtual;
  public
    property    Empty: boolean read GetEmpty;
    property    Capabilities: TDbLibBufferCapabilities read FCaps;

    procedure   Assign(Source: TPersistent); override;

    (* Read from buffer into memory *)
    function    Read(const ByteIndex: int64; DataLength: integer; var Data):  integer; overload;

    (* Write to buffer from memory *)
    function    Write(const ByteIndex: int64; DataLength: integer; const Data):  integer; overload;

    (* Append data to end-of-buffer from various sources *)
    procedure   Append(const Buffer:TDbLibBuffer); overload;
    procedure   Append(const Stream: TStream); overload;
    procedure   Append(const Data;const DataLength:integer); overload;

                // Fill the buffer with a repeating pattern of data *)
    function    Fill(const ByteIndex: int64;
                const FillLength: int64;
                const DataSource; const DataSourceLength: integer):  int64;

                // Fill the buffer with the value zero
    procedure   Zero;

                (*  Insert data into the buffer. Note: This is not a simple "overwrite"
                    insertion. It will inject the new data and push whatever data is
                    successive to the byteindex forward *)
    procedure   Insert(const ByteIndex: int64;const Source; DataLength: integer); overload;
    procedure   Insert(ByteIndex: int64; const Source:TDbLibBuffer); overload;

                // Remove X number of bytes from the buffer at any given position
    procedure   Remove(const ByteIndex: int64; DataLength: integer);

                // Simple binary search inside the buffer
    function    Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;

                (*  Push data into the buffer from the beginning, which moves the current
                    data already present forward *)
    function    Push(const Source; DataLength: integer;const Orientation: TDbLibStackOrientation = soDownUp):  integer;

                (*  Poll data out of buffer, again starting at the beginning of the buffer.
                    The polled data is removed from the buffer *)
    function    Pull(Var Target; DataLength: integer; const Orientation: TDbLibStackOrientation = soDownUp):  integer;

                // Generate a normal DWORD ELF-hashcode from the content
    function    HashCode: cardinal; virtual;

    (* Standard IO methods. Please note that these are different from those
       used by persistence. These methods does not tag the content but loads
       it directly. The methods used by persistentse will tag the data with
       a length variable *)
    procedure   LoadFromFile(Filename: string);
    procedure   SaveToFile(Filename: string);
    procedure   SaveToStream(const Stream: TStream);
    procedure   LoadFromStream(const Stream: TStream);

    function    ExportTo(ByteIndex: Int64; DataLength: integer; const Writer: TWriter):  integer;
    function    ImportFrom(ByteIndex: Int64; DataLength: integer; const Reader: TReader):  integer;

    {$IFDEF HEX_SUPPORT_ZLIB}
    procedure   CompressTo(const Target: TDbLibBuffer); overload;
    procedure   DeCompressFrom(const Source: TDbLibBuffer); overload;
    function    CompressTo: TDbLibBuffer; overload;
    function    DecompressTo: TDbLibBuffer; overload;
    procedure   Compress;
    procedure   Decompress;
    {$ENDIF}

    {$IFDEF HEX_SUPPORT_ENCRYPTION}
    property    Encryption: TDbLibEncoder read FEncryption write SetEncryption;
    procedure   EncryptTo(const Target: TDbLibBuffer); overload;
    procedure   DecryptFrom(const Source: TDbLibBuffer); overload;
    function    EncryptTo: TDbLibBuffer; overload;
    function    DecryptTo: TDbLibBuffer; overload;
    procedure   Encrypt;
    procedure   Decrypt;
    {$ENDIF}

    (* release the current content of the buffer *)
    procedure   Release;
    procedure   AfterConstruction; override;
    procedure   BeforeDestruction; override;

    constructor Create; virtual;

    (* Generic ELF-HASH methods *)
    class function ElfHash(const Data; DataLength: integer): cardinal; overload;
    class function ElfHash(const Text: string):  cardinal; overload;

    (* Kernigan and Ritchie Hash ["the C programming language"] *)
    class function KAndRHash(const Data; DataLength: integer):  cardinal; overload;
    class function KAndRHash(const Text: string):  cardinal; overload;

    (* Generic Adler32 hash *)
    class function AdlerHash(const Adler: Cardinal; const Data; DataLength: integer):  cardinal; overload;
    class function AdlerHash(const Data; DataLength: integer):  cardinal; overload;
    class function AdlerHash(const Text: string):  cardinal; overload;

    class function BorlandHash(const Data; DataLength: integer):  cardinal; overload;
    class function BorlandHash(const Text: string):  cardinal; overload;

    class function BobJenkinsHash(const Data; DataLength: integer):  cardinal; overload;
    class function BobJenkinsHash(const Text: string):  cardinal; overload;

    {$IFDEF HEX_SUPPORT_INTERNET}
    procedure   HttpGet(RemoteURL: string);
    procedure   HttpPost(RemoteURL: string; const ContentType: string); overload;
    procedure   HttpPost(RemoteURL: string; const Response: TDbLibBuffer; const ContentType: string); overload;
    procedure   HttpPost(RemoteURL: string; const FormFields: TStrings); overload;
    procedure   HttpPost(RemoteURL: string; const Populate: TDbLibMultipartCallback); overload;
    {$ENDIF}

    function  ToString(BytesPerRow: integer = 16;
              const Options: TDbLibDumpOptions = [hdSign, hdZeroPad]) : string;
              reintroduce; virtual;

    (* Generic memory fill methods *)
    class procedure FillByte(Target: Pbyte;
          const FillSize: integer; const Value: byte); inline;

    class procedure FillWord(Target: PWord;
            const FillSize: integer; const Value: word); inline;

    class procedure FillTriple(dstAddr: PDbTriplebyte;
          const InCount: integer; const Value: TDbLibTriplebyte); inline;

    class procedure FillLong(dstAddr:Pcardinal;
          const InCount: integer; const Value: cardinal); inline;

    property Size: int64 read DoGetDataSize write SetSize;

    {$IFDEF HEX_SUPPORT_ZLIB}
    property OnCompressionBegins:  TDbLibProcessBeginsEvent read FOnCompressionBegins write FOnCompressionBegins;
    property OnCompressionUpdate:  TDbLibProcessUpdateEvent read FOnCompressionUpdate write FOnCompressionUpdate;
    property OnCompressionEnds:    TDbLibProcessEndsEvent read FOnCompressionEnds write FOnCompressionEnds;
    property OnDeCompressionBegins:  TDbLibProcessBeginsEvent read FOnDeCompressionBegins write FOnDeCompressionBegins;
    property OnDeCompressionUpdate:  TDbLibProcessUpdateEvent read FOnDeCompressionUpdate write FOnDeCompressionUpdate;
    property OnDeCompressionEnds:    TDbLibProcessEndsEvent read FOnDeCompressionEnds write FOnDeCompressionEnds;
    {$ENDIF}
  end;


  // Shared error messages
  resourcestring
    CNT_ERR_BTRG_BASE  = 'Method %s threw exception %s with %s';
    CNT_ERR_BTRG_RELEASENOTSUPPORTED  = 'Buffer capabillities does not allow release';
    CNT_ERR_BTRG_SCALENOTSUPPORTED    = 'Buffer capabillities does not allow scaling';
    CNT_ERR_BTRG_READNOTSUPPORTED     = 'Buffer capabillities does not allow read';
    CNT_ERR_BTRG_WRITENOTSUPPORTED    = 'Buffer capabillities does not allow write';
    CNT_ERR_BTRG_SOURCEREADNOTSUPPORTED = 'Capabillities of datasource does not allow read';
    CNT_ERR_BTRG_TARGETWRITENOTSUPPORTED  = 'Capabillities of data-target does not allow write';
    CNT_ERR_BTRG_SCALEFAILED          = 'Memory scale operation failed: %s';
    CNT_ERR_BTRG_BYTEINDEXVIOLATION   = 'Memory byte index violation, expected %d..%d not %d';
    CNT_ERR_BTRG_INVALIDDATASOURCE    = 'Invalid data-source for operation';
    CNT_ERR_BTRG_INVALIDDATASOURCE_Size= 'Invalid data-source for operation, size is zero';
    CNT_ERR_BTRG_INVALIDDATATARGET    = 'Invalid data-target for operation';
    CNT_ERR_BTRG_EMPTY                = 'Memory resource contains no data error';
    CNT_ERR_BTRG_NOTACTIVE            = 'File is not active error';
    CNT_ERR_BTRGSTREAM_INVALIDBUFFER  = 'Invalid buffer error, buffer is NIL';

implementation

uses
  dblib.readwrite,
  dblib.buffer.memory;

//##########################################################################
// TDbLibBuffer
//##########################################################################

constructor TDbLibBuffer.Create;
begin
  inherited;
end;

procedure TDbLibBuffer.Afterconstruction;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  inherited;

  (* Get memory capabillities *)
  FCaps := DoGetCapabilities;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Afterconstruction',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.BeforeDestruction;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  (* release memory if capabillities allow it *)
  Release;
  inherited;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['BeforeDestruction',e.classname,e.message]);
  end;
  {$ENDIF}
end;

{$IFDEF HEX_SUPPORT_ENCRYPTION}
procedure TDbLibBuffer.SetEncryption(const NewEncoder: TDbLibEncoder);
begin
  if NewEncoder <> FEncryption then
  begin
    { we already have one }
    If assigned(FEncryption) then
      FEncryption.RemoveFreeNotification(self);

    FEncryption := NewEncoder;

    { Set free notification }
    if Assigned(FEncryption) then
      FEncryption.FreeNotification(self);
  end;
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_INTERNET}
procedure TDbLibBuffer.HttpGet(RemoteURL: string);
var
  LHttp:  TIdHTTP;
  LZLib:  TIdCompressorZLib;
  LStream: TStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  // Release current content
  if not Empty then
    Release();

  // Setup a stream interface for the buffer. This maps directly to our
  // content so we dont have to do anything - it will fill itself :)
  LStream := TDbLibStreamAdapter.Create(self);
  try
    LHttp := TIdHTTP.Create(nil);
    try
      LZLib := TIdCompressorZLib.Create(nil);
      try
        // Add support for compressed streams, this will show up in
        // the http request headers generated by Indy
        LHttp.Compressor := LZLib;

        try
          LHttp.Get(RemoteUrl, LStream);
        except
          on e: exception do
          raise EDbLibBufferError.CreateFmt
          ('Failed to download file [%s], system threw exception %s with message %s',
          [RemoteURL, e.ClassName, e.Message]);
        end;

      finally
        LZLib.Free;
      end;
    finally
      LHttp.Free;
    end;
  finally
    LStream.Free;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['HttpGet',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.HttpPost(RemoteURL: string;
          const Populate: TDbLibMultipartCallback);
var
  LHttp:  TIdHTTP;
  LZLib:  TIdCompressorZLib;
  LStream: TStream;
  LParts: TIdMultiPartFormDataStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  // Flush this buffer if it's not empty
  if not Empty then
    Release();

  // Make sure anonymous callback is valid
  if not assigned(Populate) then
  raise EDbLibBufferError.Create('HttpPost failed, no multipartform callback provided error');

  // Setup the multipart stream
  LParts := TIdMultiPartFormDataStream.Create;
  try
    // Setup a stream interface for this buffer. Data to post will be
    // in the multipart-stream, while the response ends up in this buffer
    LStream := TDbLibStreamAdapter.Create(self);
    try
      LHttp := TIdHTTP.Create(nil);
      try
        LZLib := TIdCompressorZLib.Create(nil);
        try
          // Add support for compressed streams, this will show up in
          // the http request headers generated by Indy
          LHttp.Compressor := LZLib;

          // Execute the callback
          Populate(self, LParts);

          try
            LHttp.Post(RemoteUrl, LParts, LStream);
          except
            on e: exception do
            raise EDbLibBufferError.CreateFmt
            ('Failed to download file [%s], system threw exception %s with message %s',
            [RemoteURL, e.ClassName, e.Message]);
          end;

        finally
          LZLib.Free;
        end;
      finally
        LHttp.Free;
      end;
    finally
      LStream.Free;
    end;
  finally
    LParts.Free;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['HttpPost',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.HttpPost(RemoteURL: string; const FormFields: TStrings);
var
  LHttp:  TIdHTTP;
  LZLib:  TIdCompressorZLib;
  LTarget: TStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FormFields = nil then
    raise EDbLibError.Create('HttpPost failed, http form-fields cannot be nil error');

  // Setup a stream interface for the target buffer, indy will drop data there
  LTarget := TDbLibStreamAdapter.Create(self);
  try
    LHttp := TIdHTTP.Create(nil);
    try
      LZLib := TIdCompressorZLib.Create(nil);
      try
        // Add support for compressed streams, this will show up in
        // the http request headers generated by Indy
        LHttp.Compressor := LZLib;

        try
          LHttp.Post(RemoteUrl, FormFields, LTarget);
        except
          on e: exception do
          raise EDbLibBufferError.CreateFmt
          ('Failed to download file [%s], system threw exception %s with message %s',
          [RemoteURL, e.ClassName, e.Message]);
        end;

      finally
        LZLib.Free;
      end;
    finally
      LHttp.Free;
    end;
  finally
    LTarget.Free;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['HttpPost',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.HttpPost(RemoteURL: string; const Response: TDbLibBuffer; const ContentType: string);
var
  LHttp:  TIdHTTP;
  LZLib:  TIdCompressorZLib;
  LStream: TStream;
  LTarget: TStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if Response = nil then
    raise EDbLibBufferError.Create('HttpPost failed, response buffer must be set');

  if (Response = self) then
    raise EDbLibBufferError.Create('HttpPost failed, response buffer cannot be same as send buffer');

  // Setup a stream interface for the buffer. Indy will send the content
  LStream := TDbLibStreamAdapter.Create(self);
  try
    LTarget := TDbLibStreamAdapter.Create(Response);
    try

      LHttp := TIdHTTP.Create(nil);
      try
        LZLib := TIdCompressorZLib.Create(nil);
        try
          // Add support for compressed streams, this will show up in
          // the http request headers generated by Indy
          LHttp.Compressor := LZLib;


          try
            LHttp.Post(RemoteUrl, LStream, LTarget);
          except
            on e: exception do
            raise EDbLibBufferError.CreateFmt
            ('Failed to post data [%s], system threw exception %s with message %s',
            [RemoteURL, e.ClassName, e.Message]);
          end;

        finally
          LZLib.Free;
        end;
      finally
        LHttp.Free;
      end;

    finally
      LTarget.Free;
    end;
  finally
    LStream.Free;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['HttpPost',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.HttpPost(RemoteURL: string; const ContentType: string);
var
  LHttp:  TIdHTTP;
  LZLib:  TIdCompressorZLib;
  LStream: TStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  // Setup a stream interface for the buffer. Indy will send the content
  LStream := TDbLibStreamAdapter.Create(self);
  try
    LHttp := TIdHTTP.Create(nil);
    try
      LZLib := TIdCompressorZLib.Create(nil);
      try
        // Add support for compressed streams, this will show up in
        // the http request headers generated by Indy
        LHttp.Compressor := LZLib;

        try
          LHttp.Post(RemoteUrl, LStream);
        except
          on e: exception do
          raise EDbLibBufferError.CreateFmt
          ('Failed to post data [%s], system threw exception %s with message %s',
          [RemoteURL, e.ClassName, e.Message]);
        end;

      finally
        LZLib.Free;
      end;
    finally
      LHttp.Free;
    end;
  finally
    LStream.Free;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['HttpPost',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}


//Note: This version is reentrant, The Adler parameter is
//      typically set to zero on the first call, and then
//      the result for the next calls until the buffer has been hashed
class function TDbLibBuffer.AdlerHash(const Adler: Cardinal;
  const Data; DataLength: integer):  cardinal;
var
  s1, s2: cardinal;
  i, n: integer;
  p: Pbyte;
begin
  if DataLength>0 then
  begin
    s1 := LongRec(Adler).Lo;
    s2 := LongRec(Adler).Hi;
    p := @Data;
    while DataLength>0 do
    begin
      if DataLength<5552 then
      n := DataLength else
      n := 5552;

      for i := 1 to n do
      begin
        inc(s1,p^);
        inc(p);
        inc(s2,s1);
      end;

      s1 := s1 mod 65521;
      s2 := s2 mod 65521;
      dec(DataLength,n);
    end;
    result := word(s1) + cardinal(word(s2)) shl 16;
  end else
  result := Adler;
end;

class function TDbLibBuffer.AdlerHash(const Data; DataLength: integer):  cardinal;
var
  LAdler: Cardinal;
begin
  LAdler := $0;
  result :=AdlerHash(LAdler, Data, DataLength);
end;

class function TDbLibBuffer.AdlerHash(const Text: string):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.UTF8.GetBytes(Text);
    result := KAndRHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TDbLibBuffer.BobJenkinsHash(const Data; DataLength: integer):  cardinal;
var
  x: integer;
  LHash: cardinal;
  P: PByte;
begin
  LHash := 0;
  if DataLength > 0 then
  begin
    P := @Data;
    for x:=1 to Datalength do
    begin
      LHash := LHash + p^;
      LHash := LHash shl 10;
      LHash := LHash shr 6;
      inc(p);
    end;

    LHash := LHash + (LHash shl 3);
    LHash := LHash + (LHash shr 11);
    LHash := LHash + (LHash shl 15);
  end;
  result := LHash;
end;

class function TDbLibBuffer.BobJenkinsHash(const Text: string):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(text) > 0 then
  begin
    LBytes := TEncoding.UTF8.GetBytes(Text);
    result := BorlandHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TDbLibBuffer.BorlandHash(const Data; DataLength: integer):  cardinal;
var
  I: integer;
  p: Pbyte;
begin
  result := 0;
  if DataLength > 0 then
  begin
    p := @Data;
    for I := 1 to DataLength do
    begin
      result := ((result shl 2) or (result shr ( SizeOf(result) * 8 - 2))) xor p^;
      inc(p);
    end;
  end;
end;

class function TDbLibBuffer.BorlandHash(const Text: string):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.UTF8.GetBytes(Text);
    result := BorlandHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TDbLibBuffer.KAndRHash(const Data; DataLength: integer):  cardinal;
var
  x:  integer;
  LAddr: PByte;
  LCrc: cardinal;
begin
  LCrc := $0;
  if DataLength > 0 then
  begin
    LAddr := @Data;
    for x:=1 to datalength do
    begin
      LCrc := ( (LAddr^ + LCrc) * 31);
    end;
  end;
  result := LCrc;
end;

class function TDbLibBuffer.KAndRHash(const Text: string):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.UTF8.GetBytes(Text);
    result := KAndRHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TDbLibBuffer.ElfHash(const Data; DataLength: integer): cardinal;
var
  i:    integer;
  x:    cardinal;
  LSrc: Pbyte;
begin
  result := 0;
  if DataLength > 0 then
  begin
    LSrc := @Data;
    for i := 1 to DataLength do
    begin
      result := (result shl 4) + LSrc^;
      x := result and $F0000000;
      if x <> 0 then
        result := result xor (x shr 24) ;
      result := result and (not x);
      inc(LSrc);
    end;
  end;
end;

class function TDbLibBuffer.ElfHash(const Text: string): cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(text) > 0 then
  begin
    LBytes := TEncoding.UTF8.GetBytes(Text);
    result := ElfHash(LBytes[0], Length(LBytes) );
  end;
end;

class procedure TDbLibBuffer.Fillbyte(Target: Pbyte;
  const FillSize: integer; const Value: byte);
var
  LBytesToFill: integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  LBytesToFill := FillSize;
  While LBytesToFill > 0 do
  begin
    Target^ := Value;
    dec(LBytesToFill);
    inc(Target);
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Fillbyte',e.classname,e.message]);
  end;
  {$ENDIF}
end;

class procedure TDbLibBuffer.FillWord(Target: PWord;
          const FillSize: integer; const Value: word);
var
  FTemp:  cardinal;
  FLongs: integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  FTemp := Value shl 16 or Value;
  FLongs := FillSize shr 3;
  while FLongs>0 do
  begin
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    dec(FLongs);
  end;

  Case FillSize mod 8 of
  1:  Target^:=Value;
  2:  Pcardinal(Target)^:=FTemp;
  3:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  4:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp;
      end;
  5:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  6:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp;
      end;
  7:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['FillWord',e.classname,e.message]);
  end;
  {$ENDIF}
end;


class procedure TDbLibBuffer.FillTriple(dstAddr: PDbTriplebyte;
          const inCount: integer;const Value: TDbLibTriplebyte);
var
  FLongs: integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  FLongs := inCount shr 3;
  While FLongs>0 do
  begin
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dec(FLongs);
  end;

  case (inCount mod 8) of
  1:  dstAddr^ := Value;
  2:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  3:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  4:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  5:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  6:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  7:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['FillTriple',e.classname,e.message]);
  end;
  {$ENDIF}
end;

class procedure TDbLibBuffer.FillLong(dstAddr:Pcardinal;
      const inCount: integer;const Value:cardinal);
var
  FLongs: integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  FLongs := inCount shr 3;
  While FLongs>0 do
  begin
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dstAddr^:=Value; inc(dstAddr);
    dec(FLongs);
  end;

  Case inCount mod 8 of
  1:  dstAddr^:=Value;
  2:  begin
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value;
      end;
  3:  begin
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value;
      end;
  4:  begin
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value;
      end;
  5:  begin
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value;
      end;
  6:  begin
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value;
      end;
  7:  begin
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value; inc(dstAddr);
        dstAddr^:=Value;
      end;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['FillLong',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.Assign(Source:TPersistent);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if Source<>NIl then
  begin
    if (Source is TDbLibBuffer) then
    begin
      Release;
      Append(TDbLibBuffer(source));
    end else
    Inherited;
  end else
  Inherited;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Assign',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBuffer.ObjectHasData: boolean;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  result := DoGetDataSize>0;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['ObjectHasData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBuffer.GetEmpty: boolean;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  result := DoGetDataSize<=0;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['GetEmpty',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.BeforeReadObject;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if (mcOwned in FCaps) then
    Release()
  else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_RELEASENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['BeforeReadObject',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.AfterReadObject;
begin
end;

procedure TDbLibBuffer.BeforeWriteObject;
begin
end;

procedure TDbLibBuffer.AfterWriteObject;
begin
end;

procedure TDbLibBuffer.ReadObject(const Reader:TReader);
var
  lTotal: Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  Reader.Read(lTotal, SizeOf(lTotal));
  if lTotal > 0 then
    ImportFrom(0, lTotal, Reader);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['ReadObject',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.WriteObject(const Writer:TWriter);
var
  lSize:  Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  lSize := Size;
  Writer.write(lSize, SizeOf(lSize));
  if lSize>0 then
    self.ExportTo(0, lSize, Writer);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['WriteObject',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.ReadObjBin(Stream: TStream);
var
  lReader:  TReader;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  lReader := TReader.Create(Stream, 1024);
  try
    BeforeReadObject();
    ReadObject(lReader);
  finally
    lReader.free;
    AfterReadObject();
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['ReadObjBin', e.classname, e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.WriteObjBin(Stream: TStream);
var
  lWriter:  TWriter;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  lWriter := TWriter.Create(Stream, 1024);
  try
    BeforeWriteObject();
    WriteObject(lWriter);
  finally
    lWriter.FlushBuffer();
    lWriter.free;
    AfterWriteObject();
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['WriteObjBin',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.DefineProperties(Filer:TFiler);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  inherited;
  Filer.DefineBinaryproperty('IO_BIN', ReadObjBin, WriteObjBin, ObjectHasData);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DefineProperties',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.DoFillData(Start: Int64; FillLength: Int64;
          const Data; DataLen: integer);
var
  mToWrite: integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  While FillLength>0 do
  begin
    mToWrite:=EnsureRange(Datalen,1,FillLength);
    DoWriteData(Start,Data,mToWrite);
    FillLength:=FillLength - mToWrite;
    Start:=Start + mToWrite;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoFillData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBuffer.Fill(const ByteIndex: Int64;
         const FillLength: Int64;
         const DataSource; const DataSourceLength: integer): Int64;
var
  LTotal: Int64;
  LTemp:  Int64;
  LAddr:  pbyte;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result:=0;

  (* Are we empty? *)
  if not Empty then
  begin
    (* Check write capabilities *)
    if mcWrite in FCaps then
    begin
      (* Check length[s] of data *)
      if  (FillLength>0)
      and (DataSourceLength>0) then
      begin
        (* check data-source *)
        LAddr:=addr(DataSource);
        if LAddr<>NIl then
        begin

          (* Get total range *)
          LTotal:=DoGetDataSize;

          (* Check range entrypoint *)
          if (ByteIndex>=0) and (ByteIndex<LTotal) then
          begin

            (* Does fill exceed range? *)
            LTemp:=ByteIndex + FillLength;
            if LTemp>LTotal then
            LTemp:=(LTotal - ByteIndex) else // Yes, clip it
            LTemp:=FillLength;               // No, length is fine

            (* fill range *)
            DoFillData(ByteIndex,LTemp,LAddr^,DataSourceLength);

            (* return size of region filled *)
            result:=LTemp;

          end else
          raise EDbLibBufferError.CreateFmt
          (CNT_ERR_BTRG_BYTEINDEXVIOLATION,[0,LTotal-1,ByteIndex]);

        end else
        raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);
      end;
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Fill',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.DoZeroData;
var
  mSize:  Int64;
  mAlign: Int64;
  mCache: TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Get size in bytes of buffer *)
  mSize := DoGetDataSize;

  (* Take widechar into account *)
  mAlign := mSize div SizeOf(char);

  (* fill our temp buffer *)
  TDbLibBuffer.Fillbyte(@mCache, mAlign, 0);

  (* Perform internal fill *)
  Fill(0, mSize, mCache, SizeOf(mCache));
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoZeroData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

{$WARNINGS OFF}
function  TDbLibBuffer.ToString(BytesPerRow: integer = 16;
          const Options: TDbLibDumpOptions = [hdSign, hdZeroPad]): string;
var
  x, y:   integer;
  LCount: integer;
  LPad:   integer;
  LDump:  array of byte;
  LCache: byte;

  procedure AddToCache(const Value: byte);
  var
    _len: integer;
  begin
    _Len := length(LDump);
    SetLength(LDump, _Len+1);
    LDump[_len] := Value;
  end;

begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if not Empty then
  Begin
    BytesPerRow := EnsureRange(BytesPerRow, 2, 64);
    LCount:=0;
    result := '';

    for x := 0 to Size-1 do
    begin

      if Read(x, SizeOf(LCache), LCache) = SizeOf(LCache) then
        AddToCache(LCache)
      else
        break;

      if (hdSign in Options) then
        result := result + '$' + IntToHex(LCache,2)
      else
        result := result + IntToHex(LCache,2);

      inc(LCount);

      if LCount >= BytesPerRow then
      begin
        if Length(LDump) > 0 then
        begin
          result := result + ' ';
          for y := 0 to length(LDump)-1 do
          begin
            if chr(LDump[y]) in
              ['A'..'Z',
               'a'..'z',
               '0'..'9',
               ',',';','<','>','{','}','[',']','-','_','#','$','%','&','/',
              '(',')','!','�','^',':',',','?'] then
            result := result + chr(LDump[y]) else
            result := result + '_';
          end;
        end;
        setlength(LDump, 0);

        result := result + #13 + #10;
        LCount := 0;
      end else
      result := result + ' ';
    end;

    if (hdZeroPad in Options) and (LCount >0 ) then
    begin
      LPad := BytesPerRow - lCount;
      for x:=1 to LPad do
      Begin
        result := result + '--';
        if (hdSign in Options) then
        result := result + '-';

        inc(LCount);
        if LCount>=BytesPerRow then
        begin
          result := result + #13 + #10;
          LCount := 0;
        end else
        result := result + ' ';
      end;
    end;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['ToString',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$WARNINGS ON}

procedure TDbLibBuffer.Zero;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if not Empty then
  begin
    if mcWrite in FCaps then
      DoZeroData()
    else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Zero',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.Append(const Buffer:TDbLibBuffer);
var
  mOffset:      Int64;
  mTotal:       Int64;
  mRead:        integer;
  mbytesToRead: integer;
  mCache:       TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if mcScale in FCaps then
  begin
    if mcWrite in FCaps then
    begin
      if Buffer <> nil then
      begin
        (* does the source support read caps? *)
        if (mcRead in Buffer.Capabilities) then
        begin

          mOffset := 0;
          mTotal := Buffer.Size;

          repeat
            mbytesToRead := EnsureRange(SizeOf(mCache), 0, mTotal);
            mRead := Buffer.Read(mOffset, mbytesToRead, mCache);
            if mRead>0 then
            begin
              Append(mCache, mRead);
              mTotal := mTotal - mRead;
              mOffset := mOffset + mRead;
            end;
          until (mbytesToRead < 1) or (mRead < 1);

        end else
        raise EDbLibBufferError.Create(CNT_ERR_BTRG_SOURCEREADNOTSUPPORTED);
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Append',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.Append(const Stream: TStream);
var
  mTotal:       Int64;
  mRead:        integer;
  mbytesToRead: integer;
  mCache:       TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if mcScale in FCaps then
  begin
    if mcWrite in FCaps then
    begin
      if Stream <> nil then
      begin
        mTotal:=(Stream.Size-Stream.Position);
        if mTotal>0 then
        begin

          Repeat
            mbytesToRead := EnsureRange(SizeOf(mCache), 0, mTotal);
            mRead := Stream.Read(mCache, mbytesToRead);
            if mRead > 0 then
            begin
              Append(mCache, mRead);
              mTotal:=mTotal - mRead;
            end;
          Until (mbytesToRead<1) or (mRead<1);

        end;
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Append',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBuffer.Append(const Data;const DataLength:integer);
var
  mOffset: Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if mcScale in FCaps then
  begin
    if mcWrite in FCaps then
    begin
      if DataLength>0 then
      begin
        mOffset:=DoGetDataSize;
        DoGrowDataBy(DataLength);
        DoWriteData(mOffset,Data,DataLength);
      end;
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Append',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Release()
    Purpose:  This method releases any content contained by the
              buffer. It is equal to Freemem in function *)
procedure TDbLibBuffer.Release;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Is the content owned/managed by us? *)
  if mcOwned in FCaps then
  DoReleaseData else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_RELEASENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Release',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Search()
    Purpose:  This method is used to define a new size of the current
              buffer. It will scale the buffer to fit the new size,
              including grow or shrink the data *)
procedure TDbLibBuffer.SetSize(const NewSize: Int64);
var
  mFactor:  Int64;
  mOldSize: Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if (mcScale in FCaps) then
  begin
    if NewSize>0 then
    begin
      (* Get current size *)
      mOldSize:=DoGetDataSize;

      (* Get difference between current size & new size *)
      mFactor:=abs(mOldSize-NewSize);

      (* only act if we need to *)
      if mFactor>0 then
      begin
        try
          (* grow or shrink? *)
          if NewSize>mOldSize then
          DoGrowDataBy(mFactor) else

          if NewSize<mOldSize then
          DoShrinkDataBy(mFactor);
        except
          on e: exception do
          raise EDbLibBufferError.CreateFmt
          (CNT_ERR_BTRG_SCALEFAILED,[e.message]);
        end;
      end;
    end else
    Release;
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['SetSize',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Search()
    Purpose:  The search method allows you to perform a simple binary
              search inside the buffer.
    Comments: The search does not yet deploy caching of data, so on
              larger buffers it may be slow *)
function  TDbLibBuffer.Search(const Data; const DataLength: integer;
    var FoundbyteIndex: int64):  boolean;
var
  mTotal:   Int64;
  mToScan:  Int64;
  src:      Pbyte;
  mbyte:    byte;
  mOffset:  Int64;
  x:        Int64;
  y:        Int64;
  mRoot:    Pbyte;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result:=False;
  FoundbyteIndex:=-1;

  (* Get search pointer *)
  mRoot:=addr(Data);

  (* Valid pointer? *)
  if (mRoot<>NIl) and (DataLength>0) then
  begin

    (* Check read capabilities of buffer *)
    if (mcRead in FCaps) then
    begin
      (* get total size to scan *)
      mTotal:=doGetDataSize;

      (* do we have anything to work with? *)
      if (mTotal>0) and (mTotal>=DataLength) then
      begin

        (* how many bytes must we scan? *)
        mToScan:=mTotal - DataLength;

        x:=0;
        While (x<=mToScan) do
        begin
          (* setup source PTR *)
          src:=Addr(Data);

          (* setup target offset *)
          mOffset:=x;

          (* check memory by sampling *)
          y:=1;
          while y<DataLength do
          begin
            (* break if not equal *)
            Read(mOffset,1,mbyte);
            result:=src^=mbyte;
            if not result then
            break;

            inc(src);
            mOffset:=mOffset + 1;
            Y:=Y + 1;
          end;

          (* success? *)
          if result then
          begin
            FoundbyteIndex:=x;
            Break;
          end;

          x:=x + 1;
        end;
      end;
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Search',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Insert()
    Purpose:  This function allows you to insert the content of another
              buffer anywhere in the current buffer. This operation does
              not overwrite any data currently in the buffer, but rather
              the new data is injected into the buffer - expanding the size
              and pushing the succeeding data forward *)
procedure TDbLibBuffer.Insert(ByteIndex:Int64;const Source: TDbLibBuffer);
var
  lTotal:   Int64;
  lCache:   TDbLibIOCache;
  lRead:    integer;
  lToRead:  integer;
  lEntry:   Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  // Validate source PTR
  if Source<>NIl then
  begin
    // Check that buffer supports scaling
    if (mcScale in FCaps) then
    begin
      // Check that buffer support write access
      if (mcWrite in FCaps) then
      begin
        // Check for read-access
        if (mcRead in Source.Capabilities) then
        begin
          // Get size of source
          lTotal := Source.Size;

          // Validate entry index
          if ByteIndex >= 0  then
          begin

            // anything to work with?
            if lTotal > 0 then
            begin

              lEntry := 0;
              While lTotal > 0 do
              begin
                // Clip data to cache boundaries
                lToRead := SizeOf(lCache);
                if lToRead > lTotal then
                  lToRead := lTotal;

                // Read data from source
                lRead := Source.Read(lEntry, lToRead, lCache);
                if lRead > 0 then
                begin
                  // Write data to our buffer layer
                  Insert(ByteIndex, lCache, lRead);

                  // update positions
                  lEntry := lEntry + lRead;
                  ByteIndex := ByteIndex + lRead;
                  lTotal := lTotal - lRead;
                end else
                Break;
              end;
            end;

          end else
          raise EDbLibBufferError.CreateFmt
          (CNT_ERR_BTRG_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);

        end else
        raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Insert',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Insert()
    Purpose:  This function allows you to insert X number of bytes
              anywhere in the buffer. This operation does not overwrite
              any data currently in the buffer, but rather the new
              data is injected into the buffer - expanding the size
              and pushing the succeeding data forward *)
procedure TDbLibBuffer.Insert(const ByteIndex: int64;const Source; DataLength: integer);
var
  mTotal:       Int64;
  mbytesToPush: Int64;
  mbytesToRead: integer;
  mPosition:    Int64;
  mFrom:        Int64;
  mTo:          Int64;
  mData:        Pbyte;
  mCache:       TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  // Check that buffer supports scaling
  if (mcScale in FCaps) then
  begin
    // Check that buffer support write access
    if (mcWrite in FCaps) then
    begin
      // Make sure buffer supports read operations
      if (mcRead in FCaps) then
      begin
        // Check length
        if DataLength>0 then
        begin
          // Check data-source
          mData:=@Source;
          if mData<>NIL then
          begin

            // get current size
            mTotal := DoGetDataSize;

            // Insert into data?
            if (ByteIndex >= 0) and (ByteIndex < mTotal) then
            begin
              // How many bytes should we push?
              mbytesToPush:=mTotal - ByteIndex;
              if mbytesToPush>0 then
              begin
                // grow media to fit new data
                DoGrowDataBy(DataLength);

                // calculate start position
                mPosition := ByteIndex + mbytesToPush;

                While mbytesToPush>0 do
                begin
                  // calculate how much data to read
                  mbytesToRead := EnsureRange(SizeOf(mCache), 0, mbytesToPush);

                  // calculate read & write positions
                  mFrom := mPosition - mbytesToRead;
                  mTo := mPosition - (mbytesToRead - DataLength);

                  // read data from the end
                  DoReadData(mFrom, mCache, mbytesToRead);

                  // write data upwards
                  DoWriteData(mTo, mCache, mbytesToRead);

                  // update offset values
                  mPosition := mPosition - mbytesToRead;
                  mbytesToPush := mbytesToPush - mbytesToRead;
                end;

                // insert new data
                DoWriteData(mPosition, Source, DataLength);

              end else
              DoWriteData(mTotal,Source,DataLength);
            end else

            // if @ end, use append instead
            if (ByteIndex = mTotal) then
              Append(Source,DataLength)
            else
              raise EDbLibBufferError.CreateFmt
              (CNT_ERR_BTRG_BYTEINDEXVIOLATION,[0,mTotal-1,ByteIndex]);

          end else
          raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);

        end; {:length}
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Insert',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Remove()
    Purpose:  This function allows you to remove X bytes of data from
              anywhere within the buffer. This is extremely handy
              when working with binary files, cabinet-files and other
              advanced file operations *)
procedure TDbLibBuffer.Remove(const ByteIndex: int64; DataLength: integer);
var
  mTemp:      integer;
  mTop:       Int64;
  mBottom:    Int64;
  mToRead:    integer;
  mToPoll:    Int64;
  mPosition:  Int64;
  mTotal:     Int64;
  mCache:     TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Check that buffer supports scaling *)
  if (mcScale in FCaps) then
  begin
    (* Check that buffer support write access *)
    if (mcWrite in FCaps) then
    begin
      (* Make sure buffer supports read operations *)
      if mcRead in FCaps then
      begin
        (* Validate remove length *)
        if DataLength>0 then
        begin
          // get current size
          mTotal := DoGetDataSize;

          // Remove from data, or the whole thing?
          if (ByteIndex >= 0) and (ByteIndex<mTotal) then
          begin
            mTemp := ByteIndex + DataLength;
            if DataLength <> mTotal then
            begin
              if mTemp < mTotal then
              begin
                mToPoll := mTotal - (ByteIndex + DataLength);
                mTop := ByteIndex;
                mBottom := ByteIndex + DataLength;

                While mToPoll > 0 do
                begin
                  mPosition:=mBottom;
                  mToRead:=EnsureRange(SizeOf(mCache),0,mToPoll);

                  DoReadData(mPosition, mCache, mToRead);
                  DoWriteData(mTop, mCache, mToRead);

                  mTop := mTop + mToRead;
                  mBottom := mBottom + mToRead;
                  mToPoll := mToPoll - mToRead;
                end;
                DoShrinkDataBy(DataLength);
              end else
              Release;
            end else
            begin
              (* Release while buffer? Or just clip at the end? *)
              if mTemp>mTotal then
              Release else
              DoShrinkDataBy(mTotal - DataLength);
            end;

          end else
          raise EDbLibBufferError.CreateFmt
          (CNT_ERR_BTRG_BYTEINDEXVIOLATION,[0,mTotal-1,ByteIndex]);
        end;
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Remove',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Push()
    Purpose:  Allows you to insert X number of bytes at the beginning
              of the buffer. This is very handy and allows a buffer to
              be used in a "stack" fashion. *)
function TDbLibBuffer.Push(const Source; DataLength: integer; const Orientation: TDbLibStackOrientation = soDownUp):  integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  result := 0;

  case Orientation of
  soTopDown:
    begin
      // Data is inserted at the start of the buffer
      if not Empty then
        Insert(0, Source, DataLength)
      else
        Append(Source, DataLength);
      result := DataLength;
    end;
  soDownUp:
    begin
      // Data is appended at the bottom of the buffer
      Append(Source, DataLength);
      result := DataLength;
    end;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Push',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Pull()
    Purpose:  Allows you to extract X number of bytes from the buffer,
              the buffer will then re-scale itself and remove the bytes
              you polled automatically. Very handy "stack" function *)
function TDbLibBuffer.Pull(var Target; DataLength: integer;
  const Orientation: TDbLibStackOrientation = soDownUp):  integer;
var
  mTotal:   Int64;
  mRemains: Int64;
  mOffset:  int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result := 0;

  (* Make sure buffer supports scaling *)
  if mcScale in FCaps then
  begin
    (* check write rights *)
    if mcWrite in FCaps then
    begin
      (* check read rights *)
      if mcRead in FCaps then
      begin
        (* validate length of data to poll *)
        if DataLength > 0 then
        begin
          (* get current size *)
          mTotal := DoGetDataSize();
          if mTotal > 0 then
          begin

            case Orientation of
            soTopDown:
              begin
                // Data is removed from the start of the buffer
                // calc how much data will remain
                mRemains := mTotal - DataLength;

                // anything left afterwards?
                if mRemains>0 then
                begin
                  // return data, keep the stub
                  result := Read(0, DataLength, Target);
                  Remove(0,DataLength);
                end else
                begin
                  // return data, deplete buffer
                  result:=mTotal;
                  DoReadData(0,Target, mTotal);
                  Release;
                end;

              end;
            soDownUp:
              begin
                // Data is removed from the end of the buffer

                // First, clip data-length if it exceeds the total buffer size
                if DataLength > mTotal then
                  DataLength := mTotal;

                // Find offset, clip if it goes beyond start
                mOffset := mTotal - DataLength;
                if mOffset < 0 then
                  mOffset := 0;

                // Read out the data
                result := Read(mOffset, DataLength, Target);

                // Truncate the buffer size
                self.SetSize(mTotal - Result);
              end;
            end;

          end;
        end;
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Pull',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   HashCode()
    Purpose:  Generate an Elf-hashcode [long] from buffer. *)
function TDbLibBuffer.HashCode: cardinal;
var
  i:        integer;
  x:        cardinal;
  mTotal:   Int64;
  mRead:    integer;
  mToRead:  integer;
  mIndex:   Int64;
  mCache:   TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result:=0;

  (* Check that buffer supports reading *)
  if (mcRead in FCaps) then
  begin
    (* Get current datasize *)
    mTotal:=DoGetDataSize;

    (* anything to work with? *)
    if mTotal>0 then
    begin
      (* start at the beginning *)
      mIndex:=0;

      (* keep going while we have data *)
      while mTotal>0 do
      begin
        (* clip prefetch to cache range *)
        mToRead:=SizeOf(mCache);
        if mToRead>mTotal then
        mToRead:=mTotal;

        (* read a chunk of data *)
        mRead:=read(mIndex,mToRead,mCache);

        (* anything to work with? *)
        if mRead>0 then
        begin
          (* go through the cache *)
          for i:=0 to mRead do
          begin
            result := (result shl 4) + mCache[i];
            x := result and $F0000000;
            if (x <> 0) then
            result := result xor (x shr 24);
            result := result and (not x);
          end;

          (* update variables *)
          mTotal:=mTotal - mRead;
          mIndex:=mIndex + mRead;
        end else
        Break;
      end;

    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['HashCode',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   LoadFromFile()
    Purpose:  Loads the content of a file into our buffer *)
procedure TDbLibBuffer.LoadFromFile(Filename: string);
var
  mFile:  TFileStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  mFile:=TFileStream.Create(filename,fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(mFile);
  finally
    mFile.free;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['LoadFromFile',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   SaveToFile()
    Purpose:  Saves the current content of the buffer to a file. *)
procedure TDbLibBuffer.SaveToFile(Filename: string);
var
  mFile:  TFileStream;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  mFile:=TFileStream.Create(filename,fmCreate or fmShareDenyNone);
  try
    SaveToStream(mFile);
  finally
    mFile.free;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['SaveToFile',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   SaveToStream()
    Purpose:  Saves the current content of the buffer to a stream. *)
procedure TDbLibBuffer.SaveToStream(const Stream: TStream);
var
  mWriter:  TWriter;
  mTotal:   Int64;
  mToRead:  integer;
  mRead:    integer;
  mOffset:  Int64;
  mCache:   TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Check that buffer supports reading *)
  if (mcRead in FCaps) then
  begin
    (* make sure targetstream is valid *)
    if (Stream<>NIL) then
    begin
      (* create a TWriter to benefit from cache *)
      mWriter:=TWriter.Create(Stream,1024);
      try
        (* get current buffersize *)
        mTotal:=DoGetDataSize;
        mOffset:=0;

        (* Keep going while there is data *)
        While mTotal>0 do
        begin
          (* Clip prefetch size so not to exceed range *)
          mToRead:=SizeOf(mCache);
          if mToRead>mTotal then
          mToRead:=mTotal;

          (* attempt to read the spec. size *)
          mRead:=Read(mOffset,mToRead,mCache);
          if mRead>0 then
          begin
            (* output data to our writer *)
            mWriter.Write(mCache,mRead);

            (* update variables *)
            mOffset:=mOffset + mRead;
            mTotal:=mTotal - mRead;
          end else
          Break;
        end;
      finally
        (* flush our stream cache to medium *)
        mWriter.FlushBuffer;

        (* release writer object *)
        mWriter.free;
      end;
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATATARGET);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['SaveToStream',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   LoadFromStream()
    Purpose:  Loads the entire content of a stream into the current buffer.
    Comments: This method releases the current buffer and use Append()
              to insert data. Also, it takes height for the current
              position of the source stream - so make sure position is
              set to zero if you want to load the whole content. *)
procedure TDbLibBuffer.LoadFromStream(const Stream: TStream);
var
  mReader:  TReader;
  mTotal:   Int64;
  mToRead:  integer;
  mCache:   TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Check that buffer supports writing *)
  if (mcWrite in FCaps) then
  begin
    (* Check that buffer supports scaling *)
    if (mcScale in FCaps) then
    begin
      (* Validate source PTR *)
      if Stream<>NIL then
      begin
        (* Release current buffer *)
        Release;

        (* create our reader object to benefit from cache *)
        mReader:=TReader.Create(Stream,1024);
        try
          mTotal:=(Stream.Size - Stream.Position);
          While mTotal>0 do
          begin
            (* Clip chunk to read *)
            mToRead:=SizeOf(mCache);
            if mToRead>mTotal then
            mToRead:=mTotal;

            (* Read data *)
            mReader.read(mCache,mToRead);

            (* Append data to current *)
            self.Append(mCache,mToRead);

            (* Update count *)
            mTotal:=mTotal - mToRead;
          end;
        finally
          mReader.free;
        end;
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['LoadFromStream',e.classname,e.message]);
  end;
  {$ENDIF}
end;

{$IFDEF HEX_SUPPORT_ENCRYPTION}
procedure TDbLibBuffer.EncryptTo(const Target: TDbLibBuffer);
var
  LSource, LTarget: TDbLibStreamAdapter;
begin
  if Target <> nil then
  begin
    LSource := TDbLibStreamAdapter.Create(self);
    try
      LTarget := TDbLibStreamAdapter.Create(Target);
      try
        if not FEncryption.EncodeStream(LSource, LTarget) then
        raise EDbLibBufferError.Create('EncryptTo failed, internal encryption error');
      finally
        LTarget.Free;
      end;
    finally
      LSource.Free;
    end;
  end else
  raise EDbLibBufferError.Create('EncryptTo failed, target buffer cannot be NIL error');
end;

procedure TDbLibBuffer.DecryptFrom(const Source: TDbLibBuffer);
var
  LSource, LTarget: TDbLibStreamAdapter;
begin
  if Source <> nil then
  begin
    LSource := TDbLibStreamAdapter.Create(Source);
    try
      LTarget := TDbLibStreamAdapter.Create(self);
      try
        if not Empty then
          Release();

        if not FEncryption.DecodeStream(LSource, LTarget) then
          raise EDbLibBufferError.Create('DecryptFrom failed, internal encryption error');
      finally
        LTarget.Free;
      end;
    finally
      LSource.Free;
    end;
  end else
  raise EDbLibBufferError.Create('DecryptFrom failed, source buffer cannot be NIL error');
end;

function TDbLibBuffer.EncryptTo: TDbLibBuffer;
var
  LSource, LTarget: TDbLibStreamAdapter;
begin
  result := TDbLibBufferMemory.Create(nil);
  if not Empty then
  begin
    LSource := TDbLibStreamAdapter.Create(self);
    try
      LTarget := TDbLibStreamAdapter.Create(result);
      try
        if not FEncryption.EncodeStream(LSource, LTarget) then
          raise EDbLibBufferError.Create('EncryptTo failed, internal encryption error');
      finally
        LTarget.Free;
      end;
    finally
      LSource.Free;
    end;
  end;
end;

function TDbLibBuffer.DecryptTo: TDbLibBuffer;
var
  LSource, LTarget: TDbLibStreamAdapter;
begin
  result := TDbLibBufferMemory.Create(nil);
  if not Empty then
  begin
    LSource := TDbLibStreamAdapter.Create(self);
    try
      LTarget := TDbLibStreamAdapter.Create(result);
      try
        if not FEncryption.DecodeStream(LSource, LTarget) then
          raise EDbLibBufferError.Create('DecryptTo failed, internal encryption error');
      finally
        LTarget.Free;
      end;
    finally
      LSource.Free;
    end;
  end;
end;

procedure TDbLibBuffer.Encrypt;
var
  LTemp:  TDbLibBufferMemory;
begin
  LTemp := TDbLibBufferMemory.Create(nil);
  try
    self.EncryptTo(LTemp);
    self.Release();
    self.Append(LTemp);
  finally
    LTemp.Free;
  end;
end;

procedure TDbLibBuffer.Decrypt;
var
  LTemp:  TDbLibBufferMemory;
begin
  LTemp := TDbLibBufferMemory.Create(nil);
  try
    LTemp.DecryptFrom(self);
    self.Release();
    self.Append(LTemp);
  finally
    LTemp.Free;
  end;
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_ZLIB}
function TDbLibBuffer.CompressTo: TDbLibBuffer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  result := TDbLibBufferMemory.Create(nil);
  self.CompressTo(result);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['CompressTo',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_ZLIB}
procedure TDbLibBuffer.Compress;
var
  lTemp:  TDbLibBuffer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  lTemp := CompressTo;
  try
    Release();
    Append(lTemp);
  finally
    lTemp.Free;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Compress',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_ZLIB}
procedure TDbLibBuffer.Decompress;
var
  mTemp:  TDbLibBuffer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  mTemp:=self.DecompressTo;
  try
    self.Release;
    self.Append(mTemp);
  finally
    mTemp.Free;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Decompress',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_ZLIB}
function TDbLibBuffer.DecompressTo:TDbLibBuffer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  result:=TDbLibBufferMemory.Create(nil);
  result.DeCompressFrom(self);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DecompressTo',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_ZLIB}
procedure TDbLibBuffer.DeCompressFrom(const Source:TDbLibBuffer);
var
  FZRec:      TZStreamRec;
  FInput:     Packed array [word] of Byte;
  FOutput:    Packed array [word] of Byte;
  FReader:    TDbLibReaderBuffer;
  FWriter:    TDbLibWriterBuffer;
  FBytes:     integer;

  procedure CCheck(const Code:integer);
  begin
    if Code<0 then
    begin
      Case Code of
      Z_STREAM_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib stream error #%d',[code]);
      Z_DATA_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib data error #%d',[code]);
      Z_BUF_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib buffer error #%d',[code]);
      Z_VERSION_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib version conflict [#%d]',[code]);
      else
        raise EDbLibBufferError.CreateFmt('Unspecified ZLib error #%d',[Code]);
      end;
    end;
  end;

begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  (* Populate ZLIB header *)
  Fillchar(FZRec,SizeOf(FZRec),0);
  FZRec.zalloc:=zlibAllocMem;
  FZRec.zfree:=zlibFreeMem;

  FzRec.next_in:=Addr(FInput);
  FzRec.next_out:=Addr(FOutput);

  (* release current content if any *)
  If not Empty then
  Release;

  (* initialize ZLIB compression *)
  CCheck(inflateInit_(FZRec,zlib_version,sizeof(FZRec)));
  try
    FReader:=TDbLibReaderBuffer.Create(Source);
    try
      FWriter:=TDbLibWriterBuffer.Create(self);
      try

        (* Signal Uncompress begins *)
        if assigned(OnDeCompressionBegins) then
        OnDeCompressionBegins(self,size);

        Repeat
          (* Get more input *)
          If FzRec.avail_in=0 then
          begin
            FzRec.avail_in:=FReader.Read(FInput,SizeOf(FInput));
            If FzRec.avail_in>0 then
            FzRec.next_in:=Addr(FInput) else
            Break;
          end;

          (* decompress input *)
          Repeat
            FzRec.next_out:=Addr(FOutput);
            FzRec.avail_out:=SizeOf(FOutput);
            CCheck(inflate(FZRec,Z_NO_FLUSH));
            FBytes:=SizeOf(FOutput) - FzRec.avail_out;
            if FBytes>0 then
            begin
              FWriter.Write(FOutput,FBytes);
              FzRec.next_out:=Addr(FOutput);
              FzRec.avail_out:=SizeOf(FOutput);
            end;
          Until FzRec.avail_in=0;

          (* Signal Inflate progress *)
          if assigned(OnDeCompressionUpdate) then
          OnDeCompressionUpdate(self,FReader.Position,Size);
        Until False;

        (* Signal Compression Ends event *)
        if assigned(OnDeCompressionEnds) then
        OnDeCompressionEnds(self,FReader.Position,FWriter.Position);
      finally
        FWriter.free;
      end;
    finally
      FReader.free;
    end;
  finally
    (* end Zlib compression *)
    inflateEnd(FZRec);
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DeCompressFrom',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF HEX_SUPPORT_ZLIB}
procedure TDbLibBuffer.CompressTo(const Target:TDbLibBuffer);
var
  FZRec:      TZStreamRec;
  FInput:     Packed array [Word] of Byte;
  FOutput:    Packed array [Word] of Byte;
  FReader:    TDbLibReaderBuffer;
  FWriter:    TDbLibWriterBuffer;
  FMode:      integer;
  FBytes:     integer;

  procedure CCheck(const Code:integer);
  begin
    if Code<0 then
    begin
      Case Code of
      Z_STREAM_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib stream error #%d',[code]);
      Z_DATA_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib data error #%d',[code]);
      Z_BUF_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib buffer error #%d',[code]);
      Z_VERSION_ERROR:
        raise EDbLibBufferError.CreateFmt('ZLib version conflict [#%d]',[code]);
      else
        raise EDbLibBufferError.CreateFmt('Unspecified ZLib error #%d',[Code]);
      end;
    end;
  end;

begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Populate ZLIB header *)
  Fillchar(FZRec,SizeOf(FZRec),0);
  FZRec.zalloc:=zlibAllocMem;
  FZRec.zfree:=zlibFreeMem;
  FzRec.next_in:=Addr(FInput);
  FZRec.next_out := Addr(FOutput);
  FZRec.avail_out := sizeof(FOutput);

  (* initialize ZLIB compression *)
  CCheck(deflateInit_(FZRec,Z_BEST_COMPRESSION,
  zlib_version,sizeof(FZRec)));

  try
    FReader:=TDbLibReaderBuffer.Create(self);
    try
      FWriter:=TDbLibWriterBuffer.Create(target);
      try

        FMode:=Z_NO_Flush;

        (* Signal Compression begins *)
        if assigned(OnCompressionBegins) then
        OnCompressionBegins(self,Size);

        Repeat
          (* more data required? If not, finish *)
          If FzRec.avail_in=0 then
          begin
            If FReader.Position<Size then
            begin
              FzRec.avail_in:=FReader.Read(FInput,SizeOf(FInput));
              FzRec.next_in:=@FInput
            end else
            FMode:=Z_Finish;
          end;

          (* Continue compression operation *)
          CCheck(deflate(FZRec,FMode));

          (* Write compressed data if any.. *)
          FBytes:=SizeOf(FOutput) - FzRec.avail_out;
          if FBytes>0 then
          begin
            FWriter.Write(FOutput,FBytes);
            FzRec.next_out:=@FOutput;
            FzRec.avail_out:=SizeOf(FOutput);
          end;

          (* Signal Compression Progress Event *)
          if assigned(OnCompressionUpdate) then
          OnCompressionUpdate(self,FReader.Position,Size);

        Until (FBytes=0) and (FMode=Z_Finish);

        (* Signal Compression Ends event *)
        if assigned(OnCompressionEnds) then
        OnCompressionEnds(self,FReader.Position,Size);

      finally
        FWriter.free;
      end;
    finally
      FReader.free;
    end;
  finally
    (* end Zlib compression *)
    deflateEnd(FZRec);
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['CompressTo',e.classname,e.message]);
  end;
  {$ENDIF}
end;
{$ENDIF}


(*  Method:   ExportTo()
    Purpose:  The ExportTo method allows you to read from the buffer,
              but output the data to an alternative target. In this case
              a TWriter class, which comes in handy when working
              with persistence.
    Comments: This method calls Write() to do the actual reading *)
function  TDbLibBuffer.ExportTo(ByteIndex: Int64;
          DataLength: integer;const Writer:TWriter):  integer;
var
  mToRead:  integer;
  mRead:    integer;
  mCache:   TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result:=0;

  (* Check that buffer supports reading *)
  if (mcRead in FCaps) then
  begin
    (* Check length of export *)
    if DataLength>0 then
    begin
      (* Validate writer PTR *)
      if Writer<>NIL then
      begin
        (* Keep going while there is data *)
        While DataLength>0 do
        begin
          (* Clip prefetch to actual length *)
          mToRead:=EnsureRange(SizeOf(mCache),0,DataLength);

          (* read from our buffer *)
          mRead:=Read(ByteIndex,mToRead,mCache);

          (* Anything read? *)
          if mRead>0 then
          begin
            (* output data to writer *)
            Writer.Write(mCache,mRead);

            (* update variables *)
            ByteIndex:=ByteIndex + mRead;
            DataLength:=DataLength - mRead;
            result:=result + mRead;
          end else
          Break;
        end;

        (* flush writer cache to medium, this is important *)
        Writer.FlushBuffer;
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATATARGET);
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['ExportTo',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Read()
    Purpose:  The read method allows you to read from the buffer into
              an untyped targetbuffer. *)
function  TDbLibBuffer.Read(const ByteIndex:Int64;
          DataLength: integer;var Data):  integer;
var
  mTotal:   Int64;
  mRemains: Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result:=0;

  (* Check that our buffer supports reading *)
  if (mcRead in FCaps) then
  begin
    (* Check length of read *)
    if DataLength>0 then
    begin
      (* Get current size of buffer *)
      mTotal:=DoGetDataSize;

      (* anything to read from? *)
      if mTotal>0 then
      begin
        (* make sure entry is within range *)
        if (ByteIndex>=0) and (ByteIndex<mTotal) then
        begin
          (* Check that copy results in data move *)
          mRemains:=mTotal - ByteIndex;
          if mRemains>0 then
          begin
            (* clip copylength to edge of buffer if we need to *)
            if DataLength>mRemains then
            DataLength:=mRemains;

            (* Read data into buffer *)
            DoReadData(ByteIndex,Data,DataLength);

            (* return bytes moved *)
            result:=DataLength;
          end;
        end;
      end else
      raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);
    end;
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_READNOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Read',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   ImportFrom()
    Purpose:  The ImportFrom method allows you to write to the buffer,
              but using an alternative datasource. In this case a TReader
              class, which comes in handy when working with persistence.
    Comments: This method calls Write() to do the actual writing  *)
function  TDbLibBuffer.ImportFrom(ByteIndex: Int64;DataLength: integer;const Reader: TReader):  integer;
var
  mToRead:  integer;
  mCache:   TDbLibIOCache;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize *)
  result:=0;

  (* Check that buffer supports writing *)
  if (mcWrite in FCaps) then
  begin
    (* Check that Reader PTR is valid *)
    if (Reader<>NIL) then
    begin
      (* keep going until no more data *)
      While DataLength>0 do
      begin
        (* Clip prefetch to make sure we dont read to much *)
        mToRead:=EnsureRange(SizeOf(mCache),0,DataLength);

        (* Anything to read after clipping? *)
        if mToRead>0 then
        begin
          (* read from source *)
          Reader.Read(mCache,mToRead);

          (* write to target *)
          Write(ByteIndex,mToRead,mCache);

          (* update variables *)
          result:=result + mToRead;
          ByteIndex:=ByteIndex + mToRead;
          DataLength:=DataLength - mToRead;
        end else
        Break;
      end;
    end else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_INVALIDDATASOURCE);
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['ImportFrom',e.classname,e.message]);
  end;
  {$ENDIF}
end;

(*  Method:   Write()
    Purpose:  The write method allows you to write data into the buffer.
              It uses the internal mechanisms to do the actual writing,
              which means that how the data is written depends on the
              buffer implementation and medium.
    Comments: This method supports scaling and will automatically
              resize the buffer to fit the new data. if the byteindex is
              within range of the current buffer - the trailing data will
              be overwritten (same as a normal MOVE operation in memory). *)
function  TDbLibBuffer.Write(const ByteIndex: Int64; DataLength: integer; const Data):  integer;
var
  mTotal:   Int64;
  mending:  Int64;
  mExtra:   Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  // Initialize
  result := 0;

  (* Check that buffer supports writing *)
  if (mcWrite in FCaps) then
  begin
    (* Make sure there is data to write *)
    if DataLength > 0 then
    begin
      (* Get current data size. if it's empty we will go for
         a straight append instead, see further down *)
      mTotal := DoGetDataSize();
      if mTotal>0 then
      begin
        (* offset within range of allocation? *)
        if (ByteIndex >= 0) and (ByteIndex < mTotal) then
        begin
          (* does this write exceed the current buffer size? *)
          mending:=ByteIndex + DataLength;
          if mending>mTotal then
          begin
            (* by how much? *)
            mExtra:=mending - mTotal;

            (* Check that we support scaling, grow if we can.
               Otherwise just clip the data to the current buffer size *)
            if (mcScale in FCaps) then
            DoGrowDataBy(mExtra) else
            DataLength:=EnsureRange(DataLength - mExtra,0,MAXINT);
          end;

          (* Anything to work with? *)
          if DataLength>0 then
          DoWriteData(ByteIndex,Data,DataLength);

          (* retun bytes written *)
          result:=DataLength;
        end else
        raise EDbLibBufferError.CreateFmt
        (CNT_ERR_BTRG_BYTEINDEXVIOLATION,[0,mTotal-1,ByteIndex]);
      end else
      begin
        (* Check that buffer supports scaling *)
        if (mcScale in FCaps) then
        begin
          (* Grow the current buffer to new size *)
          DoGrowDataBy(DataLength);

          (* write data *)
          DoWriteData(0,Data,DataLength);

          (* return bytes written *)
          result:=DataLength;
        end else
        raise EDbLibBufferError.Create(CNT_ERR_BTRG_SCALENOTSUPPORTED);
      end;
    end;
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_WRITENOTSUPPORTED);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Write',e.classname,e.message]);
  end;
  {$ENDIF}
end;

//##########################################################################
// TDbLibStreamAdapter
//##########################################################################

constructor TDbLibStreamAdapter.Create(const SourceBuffer:TDbLibBuffer);
begin
  inherited Create;
  if (SourceBuffer <> nil) then
  FBufObj := SourceBuffer else
  raise EDbLibStreamAdapter.Create(CNT_ERR_BTRGSTREAM_INVALIDBUFFER);
end;

function TDbLibStreamAdapter.GetSize:Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  result:=FBufObj.Size;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['GetSize',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibStreamAdapter.SetSize(const NewSize: Int64);
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  FBufObj.Size := EnsureRange(NewSize, 0, MaxLongInt);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['SetSize',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibStreamAdapter.Read(var Buffer;Count: longint):  longint;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  result:=FBufObj.Read(FOffset,Count,Buffer);
  inc(FOffset,result);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Read',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibStreamAdapter.Write(const Buffer;Count:longint):  longint;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  if FOffset=FBufObj.Size then
  begin
    FBufObj.Append(Buffer,Count);
    result:=Count;
  end else
  result:=FBufObj.Write(FOffset,Count,Buffer);
  inc(FOffset,result);
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Write',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibStreamAdapter.Seek(const Offset:Int64;
         Origin:TSeekOrigin): Int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  Case Origin of
  sobeginning:
    begin
      if Offset>=0 then
      FOffset:=EnsureRange(Offset,0,FBufObj.Size);
    end;
  soCurrent:
    begin
      FOffset:=EnsureRange(FOffset + Offset,0,FBufObj.Size);
    end;
  soEnd:
    begin
      if Offset>0 then
      FOffset:=FBufObj.Size-1 else
      FOffset:=EnsureRange(FOffset-(abs(Offset)),0,FBufObj.Size);
    end;
  end;
  result:=FOffset;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['Seek',e.classname,e.message]);
  end;
  {$ENDIF}
end;




end.
