unit dblib.btree;

interface

{$I 'dblib.inc'}

uses
  system.sysutils,
  system.classes;

type

  TBinTreeItem  = class;
  TBinTree      = class;

  [weak]
  TBinTreeTraverseCb = reference to procedure (const Item: TBinTreeItem);

  TBinTreeItem = class(TObject)
  private
    left: TBinTreeItem;
    right: TBinTreeItem;
    bal: -1 .. 1;
    count: integer;
  public
    constructor Create;
    function    Compare(const WithItem: TBinTreeItem): integer; virtual; abstract;
    procedure   Copy(const Target: TBinTreeItem); virtual; abstract;
    procedure   List(const CB: TBinTreeTraverseCb); virtual;
  end;

  TBinTree = class(TPersistent)
  private
    FRoot:      TBinTreeItem;
  protected
    function    Delete(const item: TBinTreeItem; var p: TBinTreeItem; var h: boolean): boolean;
    function    SearchAndInsert(const item: TBinTreeItem; var p: TBinTreeItem; var h: boolean): boolean; inline;
    function    SearchItem(const item: TBinTreeItem; Var p: TBinTreeItem): boolean; inline;
    procedure   BalanceLeft(var p: TBinTreeItem; var h: boolean; const dl: boolean); inline;
    procedure   BalanceRight(var p: TBinTreeItem; var h: boolean; const dl: boolean); inline;
    procedure   ListItems(const p: TBinTreeItem; const CB: TBinTreeTraverseCb); inline;
  public
    function    Add(const Item: TBinTreeItem): boolean; inline;
    function    Remove(const Item: TBinTreeItem): boolean; inline;
    function    Search(const Item: TBinTreeItem): boolean; inline;
    procedure   Traverse(const CB: TBinTreeTraverseCb);
    procedure   Clear; inline;

    constructor Create; virtual;
    destructor  Destroy; override;

    property    Root: TBinTreeItem read FRoot;
  end;

implementation

//#############################################################################
// TBinTreeItem
//#############################################################################

constructor TBinTreeItem.create;
begin
  inherited create;
  count := 0;
end;

procedure TBinTreeItem.List(const CB: TBinTreeTraverseCb);
begin
  if assigned(CB) then
    CB(self);
end;

//#############################################################################
// TBinTree
//#############################################################################

constructor TBinTree.create;
begin
  inherited create;
end;

destructor TBinTree.destroy;
begin
  Clear();
  inherited destroy;
end;

procedure TBinTree.Clear;
begin
  while FRoot <> nil do
  begin
    Remove(FRoot);
  end;
end;

function TBinTree.SearchAndInsert(const Item: TBinTreeItem; Var P: TBinTreeItem; var H: boolean): boolean;
begin
  result := false;
  if p = nil then
  begin // word not in tree, insert it
    p := item;
    h := true;
    with p do
    begin
      if FRoot = nil then
        FRoot := p;
      count := 1;
      left := nil;
      right := nil;
      bal := 0;
    end;
  end else
  if (item.compare(p) > 0) then // new < current
  begin
    result := SearchAndInsert(item, p.left, h);
    if h and not result then
      balanceLeft(p, h, false);
  end else
  if (item.compare(p) < 0) then // new > current
  begin
    result := SearchAndInsert(item, p.right, h);
    if h and not result then
      balanceRight(p, h, false);
  end else
  begin
    p.count := p.count + 1;
    h := false;
    result := true;
  end;
end;

// returns true and a pointer to the equal item if found, false otherwise
function TBinTree.SearchItem(const item: TBinTreeItem; Var p: TBinTreeItem): boolean;
begin
  result := false;
  if p <> nil then
  begin
    if (item.compare(p) = 0) then
      result := true
    else
    begin
      if (item.compare(p) > 0) then
        result := SearchItem(item, p.left)
      else
      begin
        if (item.compare(p) < 0) then
          result := SearchItem(item, p.right)
      end;
    end;
  end;
end;

procedure TBinTree.balanceRight(var p: TBinTreeItem; var h: boolean; const dl: boolean);
var
  p1, p2: TBinTreeItem;
Begin
  case p.bal of
    -1:
      begin
        p.bal := 0;
        if not dl then
          h := false;
      end;
    0:
      begin
        p.bal := +1;
        if dl then
          h := false;
      end;
    +1:
      begin // new balancing
        p1 := p.right;
        if (p1.bal = +1) or ((p1.bal = 0) and dl) then
        begin // single rr rotation
          p.right := p1.left;
          p1.left := p;
          if not dl then
            p.bal := 0
          else
          begin
            if p1.bal = 0 then
            begin
              p.bal := +1;
              p1.bal := -1;
              h := false;
            end
            else
            begin
              p.bal := 0;
              p1.bal := 0;
            end;
          end;
          p := p1;
        end
        else
        begin // double rl rotation
          p2 := p1.left;
          p1.left := p2.right;
          p2.right := p1;
          p.right := p2.left;
          p2.left := p;
          if p2.bal = +1 then
            p.bal := -1
          else
            p.bal := 0;
          if p2.bal = -1 then
            p1.bal := +1
          else
            p1.bal := 0;
          p := p2;
          if dl then
            p2.bal := 0;
        end;
        if not dl then
        begin
          p.bal := 0;
          h := false;
        end;
      end;
  end;
End;

procedure TBinTree.BalanceLeft(var p: TBinTreeItem; var h: boolean; const dl: boolean);
var
  p1, p2: TBinTreeItem;
Begin
  case p.bal of
    1:
      begin
        p.bal := 0;
        if not dl then
          h := false;
      end;
    0:
      begin
        p.bal := -1;
        if dl then
          h := false;
      end;
    -1:
      begin // new balancing
        p1 := p.left;
        if (p1.bal = -1) or ((p1.bal = 0) and dl) then
        begin // single ll rotation
          p.left := p1.right;
          p1.right := p;
          if not dl then
            p.bal := 0
          else
          begin
            if p1.bal = 0 then
            begin
              p.bal := -1;
              p1.bal := +1;
              h := false;
            end
            else
            begin
              p.bal := 0;
              p1.bal := 0;
              (* h:=false; *)
            end;
          end;
          p := p1;
        end
        else
        begin // double lr rotation
          p2 := p1.right;
          p1.right := p2.left;
          p2.left := p1;
          p.left := p2.right;
          p2.right := p;
          if p2.bal = -1 then
            p.bal := +1
          else
            p.bal := 0;
          if p2.bal = +1 then
            p1.bal := -1
          else
            p1.bal := 0;
          p := p2;
          if dl then
            p2.bal := 0;
        end;
        if not dl then
        begin
          p.bal := 0;
          h := false;
        end;
      end; { -1 }
  end; { case }
End;

function TBinTree.Delete(const item: TBinTreeItem; var p: TBinTreeItem; var h: boolean): boolean;
var
  q: TBinTreeItem; // h=false;

  procedure del(var r: TBinTreeItem; var h: boolean);
  begin // h=false
    if r.right <> nil then
    begin
      del(r.right, h);
      if h then
        balanceLeft(r, h, true);
    end
    else
    begin
      r.copy(q); { q.key:=r.key; }
      q.count := r.count;
      q := r;
      r := r.left;
      h := true;
    end;
  end;

begin { main of delete }
  if P = nil then
  begin
    result := false;
    h := false;
    exit;
  end;

  result := true;

  if (item.compare(p) > 0) { (x < p^.key) } then
  begin
    result := Delete(item, p.left, h);
    if h then
      balanceRight(p, h, true);
  end else
  if (item.compare(p) < 0) { (x > p^.key) } then
  begin
    result := Delete(item, p.right, h);
    if h then
      balanceLeft(p, h, true);
  end else
  begin // remove q
    q := p;
    if q.right = nil then
    begin
      p := q.left;
      h := true;
    end else
    if (q.left = nil) then
    begin
      p := q.right;
      h := true;
    end else
    begin
      del(q.left, h);
      if h then
        balanceRight(p, h, true);
    end;
    q.free; { dispose(q) };
  end;
end;

function TBinTree.Add(const item: TBinTreeItem): boolean;
var
  h: boolean;
begin
  result := SearchAndInsert(item, FRoot, h);
end;

function TBinTree.Remove(const item: TBinTreeItem): boolean;
var
  h: boolean;
begin
  result := Delete(item, FRoot, h);
end;

function TBinTree.Search(const item: TBinTreeItem): boolean;
begin
  result := SearchItem(item, FRoot);
end;

procedure TBinTree.ListItems(const p: TBinTreeItem; const CB: TBinTreeTraverseCb);
begin
  if p <> nil then
  begin
    if p.left <> nil then
      ListItems(p.left, CB);

    p.List(CB);

    if p.right <> nil then
      ListItems(p.right, CB);
  end;
end;

procedure TBinTree.Traverse(const CB: TBinTreeTraverseCb); // uses item.list recursively
begin
  Listitems(FRoot, CB);
end;

end.
