unit dblib.database;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  system.ioutils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,

  dblib.common,
  dblib.buffer,
  dblib.buffer.memory,
  dblib.buffer.disk,
  dblib.readwrite,
  dblib.encoder,
  dblib.encoder.buffer,
  dblib.bitbuffer,
  dblib.records,
  dblib.partaccess;

type

  // Forward declarations
  TDbLibDatabase    = class;
  TDbLibTable       = class;
  TDbLibMetaData    = class;
  TDbLibRecordList  = class;

  // Exception types
  EDbLibDatabase = class(EDbLibError);

  TDbLibAccessMode = (amCreate, // Create new database
    amReadWrite // Open in read-write mode [default]
    );

  IDbLibTables = interface
    ['{81DAF0F4-2352-46C2-A8DB-E1CF7E74D972}']
    function GetTable(index: integer): TDbLibTable;
    function GetTableCount: integer;
    function GetTableByName(TableName: string; var Table: TDbLibTable): boolean;
    function AddTable(TableName: string): TDbLibTable;

    property Tables[index: integer]: TDbLibTable read GetTable;
    property TableCount: integer read GetTableCount;
  end;

  TDbLibRecordList = class(TDbLibSerializableObject)
  strict private
    FDbRecList: TList<cardinal>;
    function    GetCount: cardinal; inline;
    function    GetItem(const index: integer): cardinal; inline;
    procedure   SetItem(const index: integer; const Value: cardinal); inline;
  strict protected
    procedure   BeforeReadObject; override;
    procedure   WriteObject(const Writer: TDbLibWriter); override;
    procedure   ReadObject(const Reader: TDbLibReader); override;
  public
    property    RecordList: TList<cardinal> read FDbRecList;
    property    Items[const index: integer]: cardinal read GetItem write SetItem; default;
    property    Count: cardinal read GetCount;

    procedure   Clear;

    constructor Create; override;
    destructor  Destroy; override;
  end;

  TDBLibCursorMode = (cmRead, cmEdit, cmInsert);

  TDBLibCursorLockChangedEvent = procedure (Sender: TObject; LockMode: TDBLibCursorMode) of object;
  TDBLibCursorRecNoChangedEvent = procedure (Sender: TObject; NewRecNo: integer) of object;

  TDBLibCursor = class(TObject)
  strict private
    FRecNo:       integer;
    FMode:        TDBLibCursorMode;
    FFields:      TDbLibRecord;
    FTable:       TDbLibTable;
    FOnLock:      TDBLibCursorLockChangedEvent;
    FOnNavigate:  TDBLibCursorRecNoChangedEvent;
  strict protected
    function    GetDatabase: TDbLibDatabase;
    procedure   SetRecNo(const NewRecNo: integer);
    function    GetBOF: boolean;
    function    GetEOF: boolean;
  public
    property    Table: TDbLibTable read FTable;
    property    Mode: TDBLibCursorMode read FMode;
    property    RecNo: integer read FRecNo write SetRecNo;
    property    Fields: TDbLibRecord read FFields;
    property    BOF: boolean read GetBOF;
    property    EOF: boolean read GetEOF;

    procedure   First;
    procedure   Last;
    procedure   Next;
    procedure   Previous;
    function    Lock(const LockMode: TDBLibCursorMode): boolean;

    procedure   Post;
    procedure   Cancel;

    property    OnLock: TDBLibCursorLockChangedEvent read FOnLock write FOnLock;
    property    OnNavigate: TDBLibCursorRecNoChangedEvent read FOnNavigate write FOnNavigate;

    constructor Create(const Table: TDbLibTable); virtual;
    destructor  Destroy; override;

  end;

  TDbLibTable = class(TDbLibSerializableObject)
  strict private
    FName:      string;
    FParent:    TDbLibMetaData;
    FPrototype: TDbLibRecord;
    FFieldDefs: IDbLibFields;
    FRecords:   TDbLibRecordList;
    FRecAccess: IExtendedPersistence;
    FCursor:    TDBLibCursor;

    function    GetCursor: TDBLibCursor;
    function    GetCount: cardinal;

  strict protected
    procedure   BeforeReadObject; override;
    procedure   WriteObject(const Writer: TDbLibWriter); override;
    procedure   ReadObject(const Reader: TDbLibReader); override;
  protected
    function    GetRecordList: TDbLibRecordList; inline;
    procedure   InitRecord(const Rec: TDbLibRecord);
  public
    property    Cursor: TDBLibCursor read GetCursor;
    property    RecordCount: cardinal read GetCount;
    constructor Create(const MetaData: TDbLibMetaData); reintroduce; virtual;
    destructor  Destroy; override;
  published
    property    Parent: TDbLibMetaData read FParent;
    property    FieldDefs: IDbLibFields read FFieldDefs;
    property    TableName: string read FName write FName;
  end;

  TDbLibMetaData = class(TDbLibSerializableObject, IDbLibTables)
  strict private[Weak]
    FParent: TDbLibDatabase;
    FDbName: string;
    FTables: TObjectList<TDbLibTable>;
    function GetTable(index: integer): TDbLibTable;
    function GetTableCount: integer;

    function AddTable(TableName: string): TDbLibTable;
  strict protected
    procedure SetDatabaseName(NewName: string);
    procedure BeforeReadObject; override;
    procedure WriteObject(const Writer: TDbLibWriter); override;
    procedure ReadObject(const Reader: TDbLibReader); override;
  public
    property Database: TDbLibDatabase read FParent;
    property DatabaseName: string read FDbName write SetDatabaseName;

    property Tables[index: integer]: TDbLibTable read GetTable; default;
    property TableCount: integer read GetTableCount;

    function GetTableByName(TableName: string; var Table: TDbLibTable): boolean;

    constructor Create(const database: TDbLibDatabase); reintroduce; virtual;
    destructor Destroy; override;
  end;

  IDBLibSequenceAccess = interface
    ['{558E6EF3-F6F4-4D99-A2A3-74257F702BB8}']
    function    GetFailed: boolean;
    function    GetLastError: string;
    procedure   ClearLastError;
    procedure   SetLastError(const Text: string);
    procedure   SetLastErrorF(const Text: string; const Values: array of const);

    function    GetRecordCount: cardinal;
    function    GetRecordSequence(const RecNo: cardinal): TDbLibSequence;

    function    GetSequenceForNewData(DataSize: int64; var Sequence: TDbLibSequence): boolean;

    function    GetSequenceBits(NumberOfBits: cardinal; var Bits: TDbLibSequence): boolean;
    function    WriteSequence(const Sequence: TDbLibSequence; const Data: TStream): boolean;
    function    WriteDataAsSequence(const Data: TStream; var Sequence: TDbLibSequence): boolean;

    function    MapSequence(StartPageIndex: cardinal; var Sequence: TDbLibSequence): boolean; overload;
    function    MapSequence(const StartPageIndex: cardinal): TDbLibSequence; overload;
    function    ReadDataFromSequence(SeqStart: cardinal; var Stream: TStream): boolean;
  end;

  TDbLibGrowthScheme = (gsModest, gsModerate, gsAggressive);

  TDbLibDatabase = class(TDbLibPersistentObject, IDBLibSequenceAccess)
  strict private
    FHeader:        TDbLibFileHeader;
    FBitbuffer:     TDBLibBitBuffer;
    FDbFile:        TDbLibBuffer;
    FDbFileAccess:  TDbLibPartAccess;
    FDbRecList:     TDbLibRecordList;
    FMetaData:      TDbLibMetaData;
    FTableAccess:   IDbLibTables;
    FActive:        boolean;
    FFilename:      string;
    FAccessMode:    TDbLibAccessMode;
    FGrowthScheme:  TDbLibGrowthScheme;

    function        GetActive: boolean;
    procedure       SetActive(const NewActive: boolean);

    function        GetFileName: string;
    procedure       SetFileName(NewFileName: string);

    function        GetAccessMode: TDbLibAccessMode;
    procedure       SetAccessMode(const NewMode: TDbLibAccessMode);

    function        GetDbName: string;
    procedure       SetDbName(NewDbName: string);

  strict protected

    procedure   ResetHeader;
    procedure   ReadHeader;
    procedure   WriteHeader;

    procedure   GrowBy(Parts: cardinal);
    procedure   ShrinkBy(Parts: cardinal);

    function    GetRecordCount: cardinal;
    function    GetRecordSequence(const RecNo: cardinal): TDbLibSequence;

    procedure   FlashDiskSequence(const SequenceArray: TDbLibSequence);

    // adds sequence to recordlist, returns global-scope record ID (!)
    //function    AddToGlobalRecordList(SequenceStart: cardinal): Integer;

    function    GetSequenceForNewData(DataSize: int64; var Sequence: TDbLibSequence ): boolean;

    function    GetSequenceBits(NumberOfBits: cardinal; var Bits: TDbLibSequence): boolean;
    function    WriteSequence(const Sequence: TDbLibSequence; const Data: TStream): boolean; inline;
    function    WriteDataAsSequence(const Data: TStream; var Sequence: TDbLibSequence): boolean;

    function    MapSequence(StartPageIndex: cardinal; var Sequence: TDbLibSequence): boolean; overload;
    function    MapSequence(const StartPageIndex: cardinal): TDbLibSequence; overload;
    function    ReadDataFromSequence(SeqStart: cardinal; var Stream: TStream): boolean;

  public
    procedure   Open;
    procedure   Close;

    procedure   BeforeDestruction; override;
    constructor Create; override;
    destructor  Destroy; override;

    property    Active: boolean read GetActive write SetActive;
    property    DatabaseName: string read GetDbName write SetDbName;
    property    Filename: string read GetFileName write SetFileName;
    property    AccessMode: TDbLibAccessMode read GetAccessMode write SetAccessMode;
    property    GrowthScheme: TDbLibGrowthScheme read FGrowthScheme write FGrowthScheme;
    property    MetaData: TDbLibMetaData read FMetaData;
    property    Tables: IDbLibTables read FTableAccess;
  end;

implementation

const
  CNT_BOF = -1;

// #############################################################################
// TDBLibCursor
// #############################################################################

constructor TDBLibCursor.Create(const Table: TDbLibTable);
begin
  inherited Create;
  FTable := Table;
  FFields := TDbLibRecord.Create(NIL);
  FMode := TDBLibCursorMode.cmRead;
  FRecNo := CNT_BOF;
end;

destructor TDBLibCursor.Destroy;
begin
  FFields.Free;
  inherited;
end;

procedure TDBLibCursor.Cancel;
begin
  // Check lock-mode
  if FMode in [cmEdit, cmInsert] then
  begin
    // flush whatever changes
    FFields.Clear();

    // reset mode
    FMode := TDBLibCursorMode.cmRead;
  end;
end;

function TDBLibCursor.GetBOF: boolean;
var
  LRawData: TDbLibRecordList;
begin
  LRawData := FTable.GetRecordList();
  if LRawData.Count > 0 then
    result := FRecNo < 0
  else
    result := true;
end;

{$WARNINGS OFF}
function TDBLibCursor.GetEOF: boolean;
begin
  result := FRecNo >= FTable.GetRecordList().Count
end;
{$WARNINGS ON}

{$WARNINGS OFF}
procedure TDBLibCursor.SetRecNo(const NewRecNo: integer);
var
  lRecords: TDbLibRecordList;
  lSequenceStart: cardinal;
  lAccess: IDBLibSequenceAccess;
  lData: TStream;
begin
  if FMode <> cmRead then
  begin
    raise Exception.Create('Navigation not allowed while a lock is active');
    exit;
  end;

  LRecords := FTable.GetRecordList();

  // Allow BOF
  if NewRecNo >= CNT_BOF then
  begin
    // Allow EOF
    if NewRecNo <= LRecords.Count then
    begin
      FRecNo := NewRecNo;

      if (FRecNo >= 0) and (FRecNo < LRecords.Count) then
      begin
        if FMode = TDBLibCursorMode.cmRead then
        begin
          LSequenceStart := LRecords[FRecNo];
          if GetDatabase().GetInterface(IDBLibSequenceAccess, LAccess) then
          begin
            if LAccess.ReadDataFromSequence(LSequenceStart, LData) then
            begin
              try
                FFields.LoadFromStream(LData);
              finally
                lData.Free;
              end;
            end;
          end;
        end;
      end;

      if assigned(FOnNavigate) then
        FOnNavigate(self, NewRecNo);
    end else
    raise Exception.Create('Failed to change RecNo, invalid value error');
  end else
  raise Exception.Create('Failed to change RecNo, invalid value error');

end;
{$WARNINGS ON}

procedure TDBLibCursor.First;
begin
  if FMode = cmRead then
  begin
    if FTable.GetRecordList().Count > 0 then
      SetRecNo(0);
  end else
  raise Exception.Create('Navigation not allowed while a lock is active');
end;

procedure TDBLibCursor.Last;
var
  lData: TDbLibRecordList;
  lTotal: integer;
begin
  if FMode = cmRead then
  begin
    lData := FTable.GetRecordList();
    lTotal := lData.Count;
    if lTotal > 0 then
      SetRecNo( lTotal-1 )
    else
      SetRecNo( CNT_BOF );
  end else
  raise Exception.Create('Navigation not allowed while a lock is active');
end;

{$WARNINGS OFF}
procedure TDBLibCursor.Next;
var
  lData: TDbLibRecordList;
  lTotal: cardinal;
begin
  if FMode = cmRead then
  begin
    LData := FTable.GetRecordList();
    lTotal := LData.Count;
    if lTotal > 0 then
    begin
      if FRecNo < lTotal then
        SetRecNo(FRecNo + 1);
    end;
  end else
  raise Exception.Create('Navigation not allowed while a lock is active');
end;
{$WARNINGS ON}

procedure TDBLibCursor.Previous;
var
  LData: TDbLibRecordList;
begin
  if FMode = cmRead then
  begin
    LData := FTable.GetRecordList();
    if LData.Count > 0 then
    begin
      if FRecNo > CNT_BOF then
        SetRecNo(FRecNo - 1);
    end;
  end else
  raise Exception.Create('Navigation not allowed while a lock is active');
end;

function TDBLibCursor.Lock(const LockMode: TDBLibCursorMode): boolean;
var
  LData: TDbLibRecordList;
begin
  result := false;
  if FMode = cmRead then
  begin
    LData := FTable.GetRecordList();
    case LockMode of
    cmEdit:
      begin
        // Edit is only allowed if there are record
        // and if the RecNo index >BOF and <EOF
        if LData.Count = 0 then
          raise Exception.Create('Lock failed, cannot edit when no records exist error');

        FMode := LockMode;
      end;
    cmInsert:
      begin
        // Insert is allowed even if the table is
        // empty, so we just enter the mode
        FMode := LockMode;

        // Initialize our record buffer so that it has the
        // fields and layout of the table structure
        FTable.InitRecord(FFields);
      end;
    end;

    // Fire event
    if assigned(FOnLock) then
      FOnLock(self, FMode);
  end else
  raise Exception.Create('Lock failed, please cancel or post existing lock');
end;

function TDBLibCursor.GetDatabase: TDbLibDatabase;
begin
  // use complete boolean eval
  {$B+}
  if ( (FTable <> nil) and (FTable.Parent <> nil) ) then
    result := FTable.Parent.Database
  else
    result := nil;
  {$B-}
end;

procedure TDBLibCursor.Post;
var
  lRecNo: integer;
  lDatabase: TDbLibDatabase;
  lList: TDbLibRecordList;
  lBuffer: TStream;
  lAccess: IDBLibSequenceAccess;
  lSequence:  TDbLibSequence;
begin
  if FMode in [cmEdit, cmInsert] then
  begin
    LList := FTable.GetRecordList();
    LDatabase := GetDatabase();

    case FMode of
    cmEdit:
      begin
        // Get target sequence id
        //LSequenceId := LList[FRecNo];
      end;
    cmInsert:
      begin

        if LDatabase.GetInterface(IDBLibSequenceAccess, LAccess) then
        begin
          // Get the record data as a buffer
          lBuffer := FFields.ToStream();
          try
            // Get a bit-sequence for the data
            if LAccess.GetSequenceForNewData(LBuffer.Size, lSequence) then
            begin
              // Write out data spread over the blocks
              LAccess.WriteDataAsSequence(LBuffer, LSequence);

              // Add start of sequence to our record list
              LRecNo := LList.RecordList.Add(LSequence[0]);

              // Return to read mode
              FMode := cmRead;

              // Avoid reading back the buffers!
              // We directly set the Recno so we dont
              // trigger reading stuff on navigation
              FRecNo := LRecNo;

              if assigned(FOnNavigate) then
                FOnNavigate(self, LRecNo);
            end else
            begin
              raise Exception.Create('An error occured while writing to disk error');
            end;

            // Move cursor to latest record
            //self.SetRecNo( LRecNo );
          finally
            lBuffer.Free;
          end;

        end else
        raise Exception.Create('Could not access sequence methods error');
      end;
    end;
  end else
  raise Exception.Create('Post not allowed in read-mode, please lock with edit or insert');
end;

// #############################################################################
// TDbLibRecordList
// #############################################################################

constructor TDbLibRecordList.Create;
begin
  inherited;
  FDbRecList := TList<cardinal>.Create;
end;

destructor TDbLibRecordList.Destroy;
begin
  FDbRecList.Free;
  inherited;
end;

procedure TDbLibRecordList.Clear;
begin
  FDbRecList.Clear();
end;

function TDbLibRecordList.GetCount: cardinal;
begin
  result := FDbRecList.Count;
end;

function TDbLibRecordList.GetItem(const index: integer): cardinal;
begin
  result := FDbRecList[index];
end;

procedure TDbLibRecordList.SetItem(const index: integer; const Value: cardinal);
begin
  FDbRecList[index] := Value;
end;

procedure TDbLibRecordList.BeforeReadObject;
begin
  inherited;
  FDbRecList.Clear();
end;

procedure TDbLibRecordList.WriteObject(const Writer: TDbLibWriter);
var
  x: integer;
  lTotal: integer;
  lLongs, lSingles: integer;
begin
  inherited WriteObject(Writer);

  lTotal := FDbRecList.Count;
  lLongs := lTotal div 8;
  lSingles := lTotal mod 8;
  Writer.WriteInt(lTotal);

  x := 0;
  while lLongs > 0 do
  begin
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    dec(lLongs);
  end;

  while lSingles > 0 do
  begin
    Writer.WriteLong( FDbRecList[x] ); inc(x);
    dec(lSingles);
  end;
end;

procedure TDbLibRecordList.ReadObject(const Reader: TDbLibReader);
var
  lTotal: integer;
  lLongs, lSingles: integer;
begin
  inherited ReadObject(Reader);

  lTotal := Reader.ReadInt();
  lLongs := lTotal div 8;

  while lLongs > 0 do
  begin
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    FDbRecList.Add(Reader.ReadLong());
    dec(lLongs);
  end;

  lSingles := lTotal mod 8;
  while lSingles > 0 do
  begin
    FDbRecList.Add(Reader.ReadLong());
    dec(lSingles);
  end;
end;

// #############################################################################
// TDbLibMetaData
// #############################################################################

constructor TDbLibMetaData.Create(const database: TDbLibDatabase);
begin
  inherited Create;
  FTables := TObjectList<TDbLibTable>.Create(true);
  FParent := database;
end;

destructor TDbLibMetaData.Destroy;
begin
  FTables.Free;
  inherited;
end;

procedure TDbLibMetaData.SetDatabaseName(NewName: string);
begin
  NewName := trim(NewName);
  if NewName.Length > 0 then
  begin
    if NewName <> FDbName then
    begin
      if not FParent.Active then
        FDbName := NewName
      else
        raise Exception.Create('Database name cannot be altered while active');
    end;
  end else
  raise Exception.Create('Invalid database name [empty] error');
end;

function TDbLibMetaData.GetTableByName(TableName: string;
  var Table: TDbLibTable): boolean;
var
  x: integer;
begin
  result := false;
  TableName := trim(TableName);
  TableName := Lowercase(TableName);

  if length(TableName) > 0 then
  begin
    for x := 0 to FTables.Count - 1 do
    begin
      result := LowerCase( trim(FTables[x].TableName) ) = TableName;
      if result then
      begin
        Table := FTables[x];
        break;
      end;
    end;
  end;
end;

function TDbLibMetaData.GetTable(index: integer): TDbLibTable;
begin
  result := FTables[index];
end;

function TDbLibMetaData.GetTableCount: integer;
begin
  result := FTables.Count;
end;

function TDbLibMetaData.AddTable(TableName: string): TDbLibTable;
begin
  if not GetTableByName(TableName, result) then
  begin
    result := TDbLibTable.Create(self);
    result.TableName := TableName;
    FTables.Add(result);
  end;
end;

procedure TDbLibMetaData.WriteObject(const Writer: TDbLibWriter);
var
  x: integer;
  LAccess: IExtendedPersistence;
begin
  inherited WriteObject(Writer);
  Writer.WriteString(FDbName);
  Writer.WriteInt(FTables.Count);
  if FTables.Count > 0 then
  begin
    for x := 0 to FTables.Count - 1 do
    begin
      lAccess := IExtendedPersistence(FTables[x]);
      //FTables[x].GetInterface(IExtendedPersistence, LAccess);
      Writer.WriteStream(LAccess.ObjectToStream(), true);
    end;
  end;
end;

procedure TDbLibMetaData.BeforeReadObject;
begin
  inherited;
  FDbName := '';
  FTables.Clear();
end;

procedure TDbLibMetaData.ReadObject(const Reader: TDbLibReader);
var
  LTabCount: integer;
  LTable: TDbLibTable;
  LAccess: IExtendedPersistence;
begin
  inherited ReadObject(Reader);

  FDbName := Reader.ReadString();
  LTabCount := Reader.ReadInt();
  if LTabCount > 0 then
  begin
    While LTabCount > 0 do
    begin
      LTable := TDbLibTable.Create(self);
      LTable.GetInterface(IExtendedPersistence, LAccess);
      LAccess.ObjectFromStream(Reader.ReadStream(), true);
      FTables.Add(LTable);
      dec(LTabCount);
    end;
  end;
end;

// #############################################################################
// TDbLibTable
// #############################################################################

constructor TDbLibTable.Create(const MetaData: TDbLibMetaData);
begin
  inherited Create;
  FParent := MetaData;
  FPrototype := TDbLibRecord.Create(nil);
  FFieldDefs := FPrototype as IDbLibFields;
  FRecords := TDbLibRecordList.Create;
  FRecords.GetInterface(IExtendedPersistence, FRecAccess);
  FCursor := nil;
end;

destructor TDbLibTable.Destroy;
begin
  FFieldDefs := nil;
  FPrototype.Free;
  FRecords.Free;
  inherited;
end;

function TDbLibTable.GetCount: cardinal;
begin
  result := FRecords.Count;
end;

function TDbLibTable.GetCursor: TDBLibCursor;
begin
  if FCursor = nil then
    FCursor := TDBLibCursor.Create(self);
  result := FCursor;
end;

procedure TDbLibTable.InitRecord(const Rec: TDbLibRecord);
begin
  rec.Assign(FPrototype);
end;

function TDbLibTable.GetRecordList: TDbLibRecordList;
begin
  result := FRecords;
end;

procedure TDbLibTable.WriteObject(const Writer: TDbLibWriter);
begin
  inherited WriteObject(Writer);
  Writer.WriteString(FName);
  Writer.WriteStream(FPrototype.ToStream(), true);
  Writer.WriteStream(FRecAccess.ObjectToStream(), true)
end;

procedure TDbLibTable.BeforeReadObject;
begin
  FName := '';
  FPrototype.Clear();
  FRecords.Clear();
end;

procedure TDbLibTable.ReadObject(const Reader: TDbLibReader);
begin
  inherited ReadObject(Reader);
  FName := Reader.ReadString();
  FPrototype.FromStream(Reader.ReadStream(), true);
  FRecAccess.ObjectFromStream(Reader.ReadStream(), true);
end;

// #############################################################################
// TDbLibDatabase
// #############################################################################

constructor TDbLibDatabase.Create;
begin
  inherited Create();
  FBitbuffer := TDBLibBitBuffer.Create;
  FDbRecList := TDbLibRecordList.Create;

  FMetaData := TDbLibMetaData.Create(self);
  FMetaData.GetInterface(IDbLibTables, FTableAccess);

  FGrowthScheme := TDbLibGrowthScheme.gsModerate;

  ResetHeader();
end;

destructor TDbLibDatabase.Destroy;
begin
  FBitbuffer.Free;
  FDbRecList.Free;

  FTableAccess := nil;
  FMetaData.Free;
  inherited;
end;

procedure TDbLibDatabase.ResetHeader;
begin
  fillchar(FHeader, SizeOf(FHeader), 0);
  FHeader.dhSignature := CNT_DATABASEFILE_SIGNATURE;
  FHeader.dhVersion.bvMajor := 0;
  FHeader.dhVersion.bvMinor := 1;
  FHeader.dhVersion.bvRevision := 0;
  FHeader.dhMetadata := CNT_EOS;
  FHeader.dhRecList := CNT_EOS;
  FHeader.dhBitBuffer := CNT_EOS;
end;

procedure TDbLibDatabase.BeforeDestruction;
begin
  if Active then
    Close();
  inherited;
end;

procedure TDbLibDatabase.Open;
var
  lCreated: boolean;
  lSizeEstimate: int64;
  lPartSize: int64;
  x:  integer;
begin
  if GetActive() then
    raise EDbLibDatabase.Create('Database already active error');

  // Reset internal header record
  ResetHeader();

  // This is set to true if the file is created
  // which means that metadata must be written [the first write]
  lCreated := false;

  // We support both memory and disk database files
  if FFilename.ToLower().Equals('::memory::') then
  begin
    FDbFile := TDbLibBufferMemory.Create();
    lCreated := true;
  end
  else
  begin
    case FAccessMode of
      amCreate:
      begin
        // Delete file if already there, just for brewity
        if FileExists(FFilename) then
          raise Exception.Create('Failed to create database, file already exists error');
          //DeleteFile(FFilename);

        try
          FDbFile := TDbLibBufferFile.CreateEx(FFilename, fmCreate or fmShareDenyWrite);
        except
          on e: exception do
          begin
            raise;
            exit;
          end;
        end;

        lCreated := true;
      end;
      amReadWrite:
      begin
        FDbFile := TDbLibBufferFile.CreateEx(FFilename, fmOpenReadWrite or fmShareDenyWrite);
      end;
    end;
  end;

  // Setup our part-file access interface
  FDbFileAccess := TDbLibPartAccess.Create(FDbFile, SizeOf(TDbLibFileHeader),
    SizeOf(TDbPartData), CNT_DATABASEFILE_PAGESIZE);

  // First time? Initialize the fileheader
  if LCreated then
  begin
    // Allocate bits for parts
    FBitbuffer.ReAllocate(16);

    // Calculate the size of the database so it matches the
    // bit-buffer. The bit-buffer allocates bytes and will always
    // scale up to the nearest byte (ex: 9 bits will scale to 16, since 9 bits require 2 bytes)
    LPartSize := SizeOf(TDbPartData);
    LSizeEstimate := SizeOf(TDbLibFileHeader);
    LSizeEstimate := lSizeEstimate + (LPartSize * FBitbuffer.Count);

    // Set initial size
    FDbFile.Size := LSizeEstimate;

    // Format the pages
    for x := 0 to FBitBuffer.Count-1 do
    begin
      FDbFileAccess.ZeroPart(x);
    end;

    // Write the header
    WriteHeader();
  end else
  ReadHeader();

  FActive := true;
end;

procedure TDbLibDatabase.Close;
begin
  if GetActive() then
  begin
    try
      WriteHeader();
    finally
      FreeAndNil(FDbFileAccess);
      FreeAndNil(FDbFile);
      FBitbuffer.Release();
      ResetHeader();
      FActive := false;
    end;
  end;
end;

function TDbLibDatabase.GetRecordCount: cardinal;
begin
  result := FDbRecList.Count;
end;

function TDbLibDatabase.GetRecordSequence(const RecNo: cardinal): TDbLibSequence;
begin
  result := MapSequence(FDbRecList[RecNo]);
end;

procedure TDbLibDatabase.GrowBy(Parts: cardinal);
var
  lNew: cardinal;
  lPart: TDbPartData;
begin
  if GetFailed() then
    ClearLastError();

  if Parts > 0 then
  begin
    // Align growth factor to nearest X depending
    // on the growth scheme selected
    case FGrowthScheme of
    gsModest:     Parts := TDbLibUtils.ToNearest(Parts, 32);
    gsModerate:   Parts := TDbLibUtils.ToNearest(Parts, 128);
    gsAggressive: Parts := TDbLibUtils.ToNearest(Parts, 1024);
    end;

    // Find new size in bits
    lNew := FBitbuffer.Count + Parts;

    // Re-allocate the bitbuffer
    try
      FBitbuffer.ReAllocate(lNew);
    except
      on e: exception do
      begin
        SetLastError('Failed to allocate new page-index: ' + e.Message);
        exit;
      end;
    end;

    // Scale the file according to the growth
    while Parts > 0 do
    begin
      LPart := TDbPartData.Create();
      FDbFileAccess.AppendPart(LPart);
      dec(Parts);
    end;

  end;
end;

procedure TDbLibDatabase.ShrinkBy(Parts: cardinal);
var
  lNew: cardinal;
begin
  if Parts > 0 then
  begin
    // Find new size
    LNew := FBitbuffer.Count - Parts;

    // Re-allocate the bitbuffer
    FBitbuffer.ReAllocate(LNew);

    // Since the bitbuffer will align stuff based on
    // the growth factor, we have to use whatever the bitbuffer arrived at
    // to keep track of the new size
    lNew := FBitbuffer.Count * SizeOf(TDbPartData);
    lNew := lNew + SizeOf(FHeader);

    // Scale the file so it matches the bitbuffer
    FDbFile.Size := lNew;
  end;
end;

function TDbLibDatabase.ReadDataFromSequence(SeqStart: cardinal; var Stream: TStream): boolean;
var
  x: integer;
  LSequence: TDbLibSequence;
  LPart: TDbPartData;
  lCount: cardinal;
begin
  if GetFailed() then
    ClearLastError();

  if not MapSequence(SeqStart, LSequence) then
  begin
    SetLastErrorF('Failed to read sequence: %s', [GetLastError()]);
    exit(false);
  end;

  lCount := length(lSequence);
  if lCount < 1 then
  begin
    SetLastError('Failed to read sequence: sequence is empty error');
    exit(false);
  end;

  try
    if lCount > 2000 then
      Stream := TFileStream.Create(TPath.GetTempFileName(), fmCreate)
    else
      Stream := TMemoryStream.Create();
  except
    on e: exception do
    begin
      SetLastErrorF('Failed to read sequence: %s', [e.Message]);
      exit(false);
    end;
  end;

  for x := low(LSequence) to high(LSequence) do
  begin
    FDbFileAccess.ReadPart(LSequence[x], LPart);
    if LPart.ddBytes > 0 then
      lPart.DataToStream(Stream);
  end;

  if Stream = nil then
  begin
    SetLastErrorF('Failed to read sequence: could not read sequence from block %d', [SeqStart]);
    exit(false);
  end;

  Stream.Seek32(0, TSeekOrigin.soBeginning);

  result := true;
end;

function TDbLibDatabase.WriteDataAsSequence(const Data: TStream;
  var Sequence: TDbLibSequence): boolean;
var
  LPagesRequired: cardinal;
begin
  if GetFailed() then
    ClearLastError();

  LPagesRequired := FDbFileAccess.CalcPartsForData(Data.Size);
  result := GetSequenceBits(LPagesRequired, Sequence);
  if result then
  begin
    result := WriteSequence(Sequence, Data);
    if not result then
    begin
      try
        FBitbuffer.SetBits(sequence, false);
      except
        on e: exception do
        begin
          // Write to log
        end;
      end;

    end;
  end;
end;

function TDbLibDatabase.MapSequence(const StartPageIndex: cardinal): TDbLibSequence;
begin
  if StartPageIndex <> CNT_EOS then
    MapSequence(StartPageIndex, result)
  else
    Setlength(result, 0);
end;

function TDbLibDatabase.MapSequence(StartPageIndex: cardinal; var Sequence: TDbLibSequence): boolean;
var
  LPart: TDbPartData;
  xlen: cardinal;
begin
  if GetFailed() then
    ClearLastError();

  Setlength(Sequence, 0);

  if StartPageIndex = CNT_EOS then
  begin
    SetLastError('Failed to map sequence: Invalid page offset error');
    exit(false);
  end;

  if StartPageIndex > FDbFileAccess.PartCount-1 then
  begin
    SetLastError('Failed to map sequence: Invalid index, sequence mapping failed error');
    exit(false);
  end;

  repeat

    try
      FDbFileAccess.ReadPart(StartPageIndex, LPart);
    except
      on e: exception do
      begin
        SetLastErrorF('Failed to map sequence: %s', [e.Message]);
        exit(false);
      end;
    end;

    if lPart.ddSignature <> CNT_DATABASEPART_SIGNATURE then
      break;

    xlen := Length(Sequence);
    Setlength(Sequence, xlen + 1);
    Sequence[xlen] := StartPageIndex;

    if LPart.ddNext = CNT_EOS then
      break;

    StartPageIndex := LPart.ddNext;

  until StartPageIndex = CNT_EOS;

  result := true;
end;

function TDbLibDatabase.WriteSequence(const Sequence: TDbLibSequence; const Data: TStream): boolean;
var
  x: integer;
  lPart: TDbPartData;
  lWritten: cardinal;
begin
  if GetFailed() then
    ClearLastError();

  if Data = nil then
  begin
    SetLastError('Failed to write sequence: source stream was nil error');
    exit(false);
  end;

  if Data.position > 0 then
  begin
    try
      Data.Seek32(0, TSeekOrigin.soBeginning);
    except
      on e: exception do
      begin
        SetLastErrorF('Failed to write sequence: %s', [e.Message]);
        exit(false);
      end;
    end;
  end;

  for x := low(Sequence) to high(Sequence) do
  begin
    // Initialize the part
    LPart.Reset();
    LPart.ddRoot := Sequence[low(Sequence)];

    // Set Next-> unless its the last part
    if x < high(Sequence) then
      LPart.ddNext := Sequence[x + 1]
    else
      LPart.ddNext := CNT_EOS;

    if x > low(Sequence) then
      LPart.ddPrevious := Sequence[x - 1]
    else
      LPart.ddPrevious := CNT_EOS;

    try
      lWritten := lPart.StreamToData(Data);
    except
      on e: exception do
      begin
        SetLastErrorF('Failed to write sequence: %s', [e.Message]);
        exit(false);
      end;
    end;

    if lWritten < 1 then
    begin
      SetLastError('Sequence broken or stream misaligned, no bytes written to page error');
      exit(false);
    end;

    // Write finished page to disk
    try
      FDbFileAccess.WritePart(Sequence[x], LPart);
    except
      on e: exception do
      begin
        SetLastErrorF('Failed to write sequence: %s', [e.Message]);
        exit(false);
      end;
    end;
  end;

  result := true;
end;

function TDbLibDatabase.GetSequenceForNewData(DataSize: int64; var Sequence: TDbLibSequence): boolean;
var
  lParts: integer;
begin
  if GetFailed() then
    ClearLastError();

  SetLength(Sequence, 0);

  if DataSize < 1 then
  begin
    SetLastError('Failed to build sequence for data: size was 0 or less error');
    exit(false);
  end;

  // Calc number of parts needed for data
  lParts := FDbFileAccess.CalcPartsForData(DataSize);

  // Get free pages and build up the sequence
  result := GetSequenceBits(LParts, Sequence);
end;

function TDbLibDatabase.GetSequenceBits(NumberOfBits: cardinal;
  var Bits: TDbLibSequence): boolean;
var
  x: cardinal;
  LFound: cardinal;
  lWorked: boolean;
  llen: cardinal;
  lBitsToGrow: cardinal;

begin
  if GetFailed() then
    ClearLastError();

  Setlength(Bits, 0);

  if NumberOfBits < 1 then
    exit(false);

  lWorked := false;
  lBitsToGrow := NumberOfBits;

  for x := 1 to NumberOfBits do
  begin
    LFound := CNT_EOS;
    lWorked := FBitbuffer.FindIdleBit(LFound, false); //
    if lWorked then
    begin
      if lFound = CNT_EOS then
      begin
        SetLastError('Found page for data, but offset is invalid error');
        Exit(false);
      end;

      FBitbuffer.Bits[LFound] := true;

      llen := length(Bits);
      SetLength(Bits, lLen + 1);
      Bits[lLen] := LFound;
    end else
      break;
  end;

  // Did not find all?
  if not lWorked then
  begin
    // Reset any bits we might have found above.
    // The sequence is incomplete
    llen := length(Bits);
    if lLen > 0 then
    begin
      // Adjust bits actually needed to store the data
      lBitsToGrow := lBitsToGrow - lLen;

      // zap bits we did manage to allocate
      FBitbuffer.SetBits(Bits, false);
      Setlength(Bits, 0);
    end;

    // Grow the database to fit the new data.
    GrowBy(lBitsToGrow);

    // Right, having scaled the file we try again
    result := GetSequenceBits(NumberOfBits, Bits);
    if not result then
    begin
      SetLastError('Failed to scale page-index, insufficient space error');
      exit(false);
    end;

  end else
  result := true;
end;

// Physically reset the pages of a sequence on disk
procedure TDbLibDatabase.FlashDiskSequence(const SequenceArray: TDbLibSequence);
begin
  if Length(SequenceArray) > 0 then
  begin
    // Mark pages as available in our page-map
    FBitbuffer.SetBits(SequenceArray, false);

    // Reset pages on disk
    FDbFileAccess.ZeroSequence(SequenceArray);
  end;
end;

procedure TDbLibDatabase.ReadHeader;
var
  LReadBytes: integer;
  LSequence: TDbLibSequence;
  lStream: TStream;
begin
  if GetFailed() then
    ClearlastError();

  // Begin by reading the header
  // If this is not the same size, then we know something is wrong
  LReadBytes := FDbFile.Read(0, SizeOf(TDbLibFileHeader), FHeader);
  if LReadBytes <> SizeOf(TDbLibFileHeader) then
    raise EDbLibDatabase.CreateFmt
      ('Failed to read database header, expected %d bytes not %d bytes',
      [SizeOf(TDbLibFileHeader), LReadBytes]);

  // Check the header signature, we dont want to corrupt a file
  // by accident
  if FHeader.dhSignature <> CNT_DATABASEFILE_SIGNATURE then
    raise EDbLibDatabase.CreateFmt
      ('Invalid database file, expected signature [$%s] not [$%s] error',
      [IntToHex(CNT_DATABASEFILE_SIGNATURE), IntToHex(FHeader.dhSignature)]);

  // Load the database metadata
  if FHeader.dhMetadata <> CNT_EOS then
  begin
    if ReadDataFromSequence(FHeader.dhMetadata, lStream) then
    begin
      try
        IExtendedPersistence(FMetaData).ObjectFromStream(lStream, false);
      finally
        FreeAndNil(lStream);
      end;
    end else
    raise EDbLibDatabase.CreateFmt('Failed to read metadata: %s', [GetLastError()]);
  end;

  // Load the database record list
  if FHeader.dhRecList <> CNT_EOS then
  begin
    if ReadDataFromSequence(FHeader.dhRecList, lStream) then
    begin
      try
        IExtendedPersistence(FDbRecList).ObjectFromStream(lStream, false);
      finally
        FreeAndNil(lStream);
      end;
    end else
    raise EDbLibDatabase.CreateFmt('Failed to read record-list: %s', [GetLastError()]);
  end;

  // Load the database bitbuffer that maps available pages
  if FHeader.dhBitBuffer <> CNT_EOS then
  begin
    if ReadDataFromSequence(FHeader.dhBitBuffer, lStream) then
    begin
      try
        FBitbuffer.FromStream(lStream, false);
      finally
        lStream.Free;
      end;
    end else
    raise EDbLibDatabase.CreateFmt('Failed to read page-index: %s', [GetLastError()]);
  end;

  // flash bitbuffer blocks
  lSequence := MapSequence(FHeader.dhBitBuffer);
  FlashDiskSequence(lSequence);
  FHeader.dhBitBuffer := CNT_EOS;
  Setlength(lSequence, 0);

  // flash record list blocks
  lSequence := MapSequence(FHeader.dhRecList);
  FlashDiskSequence(lSequence);
  FHeader.dhRecList := CNT_EOS;
  Setlength(lSequence, 0);

  // flash metadata blocks
  lSequence := MapSequence(FHeader.dhMetadata);
  FlashDiskSequence(lSequence);
  FHeader.dhMetadata := CNT_EOS;
end;

procedure TDbLibDatabase.WriteHeader;

  procedure _WriteMetaData;
  var
    lData: TStream;
    lSequence: TDbLibSequence;
  begin
    Setlength(LSequence, 0);

    // Delete older metadata from file
    if FHeader.dhMetadata <> CNT_EOS then
    begin
      lSequence := MapSequence(FHeader.dhMetadata);
      FlashDiskSequence(lSequence);
      FHeader.dhMetadata := CNT_EOS;
      Setlength(LSequence, 0);
    end;

    lData := IExtendedPersistence(FMetaData).ObjectToStream();
    try
      if WriteDataAsSequence(LData, LSequence) then
        FHeader.dhMetadata := LSequence[0]
      else
        raise Exception.Create('Internal error, failed to write metadata')
    finally
      LData.Free;
    end;
  end;

  procedure _WriteRecordList;
  var
    lSequence: TDbLibSequence;
    lData: TStream;
  begin
    Setlength(LSequence, 0);

    if FHeader.dhRecList <> CNT_EOS then
    begin
      try
        try
          lSequence := MapSequence(FHeader.dhRecList);
          FlashDiskSequence(LSequence);
          FHeader.dhRecList := CNT_EOS;
        finally
          Setlength(LSequence, 0);
        end;
      except
        on e: exception do;
      end;
    end;

    LData := IExtendedPersistence(FDbRecList).ObjectToStream();
    try
      if WriteDataAsSequence(LData, LSequence) then
        FHeader.dhRecList := LSequence[0]
      else
        raise Exception.Create('Internal error, failed to write record list')
    finally
      FreeAndNil(LData);
    end;
  end;

  procedure _WriteBitBuffer;
  var
    lSequence: TDbLibSequence;
    lData: TStream;
    lPartsNeeded: cardinal;
    lPartsFree: cardinal;
    lDataSize: cardinal;
  begin
    Setlength(LSequence, 0);

    if FHeader.dhBitBuffer <> CNT_EOS then
    begin
      lSequence := MapSequence(FHeader.dhBitBuffer);
      FlashDiskSequence( lSequence );
      FHeader.dhBitBuffer := CNT_EOS;
      Setlength(LSequence, 0);
    end;

    // We have to do this twice to make sure the file
    // has enough space to store the final bit-buffer.
    // if we just store it and the file scales, the bitbuffer
    // we have serialized will be out-of-date (the new bits wont be in it).
    lDataSize := FBitBuffer.CalcSizeOfData();
    lPartsNeeded := FDbFileAccess.CalcPartsForData(lDataSize);
    lPartsFree := FBitBuffer.CalcFreeBits();
    if lPartsNeeded > lPartsFree then
      self.GrowBy(lPartsNeeded - lPartsFree);

    lDataSize := FBitBuffer.CalcSizeOfData();
    lPartsNeeded := FDbFileAccess.CalcPartsForData(lDataSize);
    lPartsFree := FBitBuffer.CalcFreeBits();
    if lPartsNeeded >= lPartsFree then
      GrowBy(lPartsNeeded - lPartsFree);

    LData := FBitbuffer.ToStream();
    try

      // We should now have enough pages to store this
      if WriteDataAsSequence(LData, LSequence) then
        FHeader.dhBitBuffer := LSequence[0]
      else
        raise Exception.Create('Internal error, failed to write pagemap error');
    finally
      FreeAndNil(LData);
    end;
  end;

begin
  //fillchar(FHeader, SizeOf(FHeader), 0);
  FHeader.dhSignature := CNT_DATABASEFILE_SIGNATURE;
  FHeader.dhVersion.bvMajor := 0;
  FHeader.dhVersion.bvMinor := 1;
  FHeader.dhVersion.bvRevision := 0;
  FHeader.dhName := shortstring(FMetaData.DatabaseName);

  _WriteMetaData();
  _WriteRecordList();
  _WriteBitBuffer();

  // Write header to file
  FDbFile.Write(0, SizeOf(FHeader), FHeader);
end;

function TDbLibDatabase.GetAccessMode: TDbLibAccessMode;
begin
  result := FAccessMode;
end;

procedure TDbLibDatabase.SetAccessMode(const NewMode: TDbLibAccessMode);
begin
  if NewMode <> FAccessMode then
  begin
    if not GetActive() then
      FAccessMode := NewMode
    else
      raise EDbLibDatabase.Create
        ('Accessmode cannot be altered while active error');
  end;
end;

function TDbLibDatabase.GetActive: boolean;
begin
  result := FActive;
end;

procedure TDbLibDatabase.SetActive(const NewActive: boolean);
begin
  if NewActive <> FActive then
  begin
    case NewActive of
    true:   Open();
    false:  Close();
    end;
  end;
end;

function TDbLibDatabase.GetDbName: string;
begin
  result := FMetaData.DatabaseName;
end;

procedure TDbLibDatabase.SetDbName(NewDbName: string);
begin
  NewDbName := trim(NewDbName);
  if NewDbName <> FMetaData.DatabaseName then
  begin
    if not GetActive() then
      FMetaData.DatabaseName := NewDbName
    else
      raise EDbLibDatabase.Create('Database name cannot be altered while active error');
  end;
end;

function TDbLibDatabase.GetFileName: string;
begin
  result := FFilename;
end;

procedure TDbLibDatabase.SetFileName(NewFileName: string);
begin
  NewFileName := trim(NewFileName);
  if NewFileName <> FFilename then
  begin
    if not GetActive() then
      FFilename := NewFileName
    else
      raise EDbLibDatabase.Create('Filename cannot be altered while active error');
  end;
end;


end.
