unit dblib.encoder;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants
  {$IFDEF HEX_SUPPORT_ZLIB},System.Zlib{$ENDIF}
  {$IFDEF HEX_SUPPORT_INTERNET}
    , IdZLibCompressorBase
    , IdCompressorZLib
    , IdBaseComponent
    , IdComponent
    , IdTCPConnection
    , IdTCPClient
    , IdHTTP
    , idMultipartFormData
  {$ENDIF}
  , dblib.common
  ;

type

  // Forward declarations
  TDbLibEncoder     = class;
  TDbLibEncodingKey = class;

  // Custom exceptions
  EDbLibEncoder = class(EDbLibError);

  // Event declarations
  TDbLibEncoderBeforeEncodeEvent    = procedure (Sender: TObject; Total: int64) of object;
  TDbLibEncoderEncodeProgressEvent  = procedure (Sender: TObject; Offset, Total: int64) of object;
  TDbLibEncoderAfterEncodeEvent     = procedure (Sender: TObject; Total: int64) of object;
  TDbLibEncoderBeforeDecodeEvent    = procedure (Sender: TObject; Total: int64) of object;
  TDbLibEncoderDecodeProgressEvent  = procedure (Sender: TObject; Offset, Total: int64) of object;
  TDbLibEncoderAfterDecodeEvent     = procedure (Sender: TObject; Total: int64) of object;

  TDbLibEncodingKey = class(TObject)
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  private
    FEncoder:   TDbLibEncoder;
    FOnBefore:  TNotifyEvent;
    FOnAfter:   TNotifyEvent;
    FOnReset:   TNotifyEvent;
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  protected
    function    GetReady: boolean; virtual; abstract;
    procedure   DoReset; virtual; abstract;
    procedure   DoBuild(const Data; const ByteSize: integer); overload; virtual; abstract;
    procedure   DoBuild(const Data: TStream); overload; virtual; abstract;
    procedure   SetEncoder(const NewEncoder: TDbLibEncoder); virtual;
  public
    property    Encoder: TDbLibEncoder read FEncoder write SetEncoder;
    property    Ready: boolean read GetReady;
    procedure   Build(const Data; const ByteSize: integer); overload;
    procedure   Build(const Data: TStream); overload;
    procedure   Reset;

    constructor Create; virtual;
    destructor  Destroy; override;

    property  OnKeyReset: TNotifyEvent read FOnReset write FOnReset;
    property  OnBeforeKeyBuildt: TNotifyEvent read FOnBefore write FOnBefore;
    property  OnAfterKeyBuilt: TNotifyEvent read FOnAfter write FOnAfter;
  end;

  TDbLibEncoder = class(TObject)
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  private
    FCipherKey:     TDbLibEncodingKey;
    FOnEncBegins:   TDbLibEncoderBeforeEncodeEvent;
    FOnEncProgress: TDbLibEncoderEncodeProgressEvent;
    FOnEncEnds:     TDbLibEncoderAfterEncodeEvent;
    FOnDecBegins:   TDbLibEncoderBeforeDecodeEvent;
    FOnDecProgress: TDbLibEncoderDecodeProgressEvent;
    FOnDecEnds:     TDbLibEncoderAfterDecodeEvent;

  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  protected
    function  GetReady: boolean; virtual;
    procedure SetKey(const NewKey: TDbLibEncodingKey); virtual;

  public
    property  Ready: boolean read GetReady;

    function  EncodePtr(const Source; const Target; ByteLen: int64): boolean; virtual; abstract;
    function  EncodeStream(Source, Target: TStream): boolean; virtual; abstract;

    function  DecodePtr(const Source; const Target; ByteLen: int64): boolean; virtual; abstract;
    function  DecodeStream(Source, Target: TStream): boolean; virtual; abstract;

    procedure Reset; virtual;

    property  Key: TDbLibEncodingKey read FCipherKey write SetKey;
    property  OnEncodingBegins: TDbLibEncoderBeforeEncodeEvent read FOnEncBegins write FOnEncBegins;
    property  OnEncodingProgress: TDbLibEncoderEncodeProgressEvent read FOnEncProgress write FOnEncProgress;
    property  OnEncodingEnds: TDbLibEncoderAfterEncodeEvent read FOnEncEnds write FOnEncEnds;

    property  OnDecodingBegins: TDbLibEncoderBeforeDecodeEvent read FOnDecBegins write FOnDecBegins;
    property  OnDecodingProgress: TDbLibEncoderDecodeProgressEvent read FOnDecProgress write FOnDecProgress;
    property  OnDecodingEnds: TDbLibEncoderAfterDecodeEvent read FOnDecEnds write FOnDecEnds;
  end;

implementation


//##########################################################################
// TDbLibEncodingKey
//##########################################################################

constructor TDbLibEncodingKey.Create;
begin
  inherited Create();
end;

destructor TDbLibEncodingKey.Destroy;
begin
  if GetReady() then
    DoReset();
  inherited;
end;

procedure TDbLibEncodingKey.SetEncoder(const NewEncoder: TDbLibEncoder);
begin
  FEncoder := NewEncoder;
end;

procedure TDbLibEncodingKey.Build(const Data: TStream);
begin
  if GetReady() then
    DoReset();

  if assigned(FOnBefore) then
    FOnBefore(self);

  try
    DoBuild(Data);
  finally
    if assigned(FOnAfter) then
      FOnAfter(Self);
  end;
end;

procedure TDbLibEncodingKey.Build(const Data; const ByteSize: integer);
begin
  if GetReady() then
    DoReset();

  if assigned(FOnBefore) then
    FOnBefore(self);
  try
    DoBuild(Data, ByteSize);
  finally
    if assigned(FOnAfter) then
    FOnAfter(Self);
  end;
end;

procedure TDbLibEncodingKey.Reset;
begin
  if GetReady then
    DoReset();
end;


//##########################################################################
// TDbLibEncoder
//##########################################################################

procedure TDbLibEncoder.Reset;
begin
  if assigned(FCipherKey) then
  begin
    FCipherKey.Reset();
  end else
  raise EDbLibEncoder.Create('Reset failed, no encryption-key has been assigned error');
end;

procedure TDbLibEncoder.SetKey(const NewKey: TDbLibEncodingKey);
begin
  FCipherKey := newKey;
end;

function TDbLibEncoder.GetReady: boolean;
begin
  result := assigned(FCipherKey) and FCipherKey.Ready;
end;

end.
