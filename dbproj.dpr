program dbproj;

uses
  Vcl.Forms,
  mainform in 'mainform.pas' {frmMain},
  dblib.bitbuffer in 'dblib.bitbuffer.pas',
  dblib.buffer in 'dblib.buffer.pas',
  dblib.common in 'dblib.common.pas',
  dblib.encoder in 'dblib.encoder.pas',
  dblib.buffer.memory in 'dblib.buffer.memory.pas',
  dblib.readwrite in 'dblib.readwrite.pas',
  dblib.buffer.disk in 'dblib.buffer.disk.pas',
  dblib.encoder.buffer in 'dblib.encoder.buffer.pas',
  dblib.encoder.rc4 in 'dblib.encoder.rc4.pas',
  dblib.partaccess in 'dblib.partaccess.pas',
  dblib.database in 'dblib.database.pas',
  dblib.records in 'dblib.records.pas',
  dblib.btree in 'dblib.btree.pas',
  dblib.filearray in 'dblib.filearray.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
