unit dblib.encoder.buffer;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.encoder;

type

  TDbLibEncoderBuffer = class(TDbLibEncoder)
  public
    // These access inherited methods: EncodeStream, DecodeStream
    function  EncodeBuffer(const Source, Target: TDbLibBuffer): boolean; virtual;
    function  DecodeBuffer(const Source, Target: TDbLibBuffer): boolean; virtual;
  end;

implementation


//##########################################################################
// TDbLibEncoderBuffer
//##########################################################################

function TDbLibEncoderBuffer.EncodeBuffer(const Source, Target: TDbLibBuffer): boolean;
var
  LSource: TDbLibStreamAdapter;
  LTarget: TDbLibStreamAdapter;
begin
  result := false;

  if GetReady() then
  begin
    if (source <> nil) then
    begin
      if (target <> nil) then
      begin
        // Flush target if not empty
        if not Target.Empty then
        Target.Release();

        LSource := TDbLibStreamAdapter.Create(source);
        try
          LTarget := TDbLibStreamAdapter.Create(Target);
          try
            EncodeStream(LSource, LTarget);
          finally
            LTarget.Free;
          end;
        finally
          LSource.Free;
        end;

        result := true;
      end;
    end;
  end;
end;

function TDbLibEncoderBuffer.DecodeBuffer(const Source, Target: TDbLibBuffer): boolean;
var
  LSource: TDbLibStreamAdapter;
  LTarget: TDbLibStreamAdapter;
begin
  result := false;

  if GetReady() then
  begin
    if (source <> nil) then
    begin
      if (target <> nil) then
      begin
        if not Target.Empty then
        Target.Release();

        LSource := TDbLibStreamAdapter.Create(source);
        try
          LTarget := TDbLibStreamAdapter.Create(Target);
          try
            DecodeStream(LSource, LTarget);
          finally
            LTarget.Free;
          end;
        finally
          LSource.Free;
        end;

        result := true;
      end;
    end;
  end;
end;


end.
