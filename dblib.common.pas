unit dblib.common;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants;

type

  // Exception types
  EDbLibError = class(Exception);

  // Forward declarations
  TDbLibPersistentObjectClass = class of TDbLibPersistentObject;

  // Object state for TDbLibObject
  TObjectState = set of
    (
      osCreating,
      osDestroying,
      osUpdating,
      osReadWrite,
      osSilent
    );

  TDbLibSequence = array of cardinal;

  IObjectOwner = Interface
  ['{286A5B65-D5F6-4008-8095-7978CE74C666}']
    function  GetParent: TObject;
    procedure SetParent(const ParentObj: TObject);
  End;

  IObjectLifeCycle = Interface
    ['{FFF8E7BF-ADC9-4245-8717-E9A6C567DED9}']
    procedure SetObjectState(const Value: TObjectState);
    procedure AddObjectState(const Value: TObjectState);
    procedure RemoveObjectState(const Value: TObjectState);
    function  GetObjectState: TObjectState;
    function  QueryObjectState(const Value: TObjectState):  boolean;
    function  GetObjectClass: TDbLibPersistentObjectClass;
  End;

  // Simple object that supports interfaces, no reference counting
  TDbLibInterfacedObject = class(TObject)
  {$IFDEF HEX_USE_STRICT}
  strict
  {$ENDIF}
  protected
    // Implements:: IUnknown
    function    QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function    _AddRef: integer; virtual; stdcall;
    function    _Release: integer; virtual; stdcall;
  end;

  TDbLibErrorObject = class(TPersistent)
  {$IFDEF HEX_USE_STRICT}
  strict
  {$ENDIF}
  private
    FLastError: string;

  {$IFDEF HEX_USE_STRICT}
  strict
  {$ENDIF}
  protected
    procedure   SetLastError(const Text: string); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    procedure   SetLastErrorF(const Text: string; const Values: array of const);
    function    GetLastError: string; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
  public
    function    GetFailed: boolean; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    procedure   ClearLastError; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}

    property    LastError: string read GetLastError;
  end;

  // Simple persistent object with interface support, no reference counting
  TDbLibPersistentObject = class(TDbLibErrorObject, IUnknown, IObjectOwner, IObjectLifeCycle)
  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  private
    FState:     TObjectState;
    FParent:    TObject;

  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  protected
    // Implements:: IDbLibOwnedObject
    function    GetParent: TObject; virtual;
    procedure   SetParent(const ParentObj: TObject);
    property    Parent: TObject read GetParent;

    // Implements:: IObjectLifeCycle
    procedure   SetObjectState(const Value: TObjectState);
    procedure   AddObjectState(const Value: TObjectState);
    procedure   RemoveObjectState(const Value: TObjectState);
    function    QueryObjectState(const Value: TObjectState):  boolean;
    function    GetObjectState: TObjectState;
    function    GetObjectclass: TDbLibPersistentObjectClass;

  {$IFDEF HEX_USE_STRICT}
    strict
  {$ENDIF}
  protected
    // Implements:: IUnknown
    function    QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function    _AddRef: integer; virtual; stdcall;
    function    _Release: integer; virtual; stdcall;
  public
    class function ObjectPath: string;
    procedure   AfterConstruction; override;
    procedure   BeforeDestruction; override;
    constructor Create; virtual;
  end;


  TDbLibUtils = class
  public
    class function  ToNearest(const Value, Factor: cardinal): cardinal; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    class procedure Swap(var Primary, Secondary: cardinal); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    class function  Diff(const Primary, Secondary: cardinal; const Exclusive: boolean = false): cardinal; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}

    class function  BitsOf(Const FromByteCount: cardinal): cardinal; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    class function  BytesOf(FromBitCount: cardinal): cardinal; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    class function  BitsSetInByte(const Value: byte): cardinal;

    class Function  BitGet(const Index: cardinal; const Buffer): boolean; {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
    class procedure BitSet(const Index: cardinal; var Buffer; const Value: boolean); {$IFDEF HEX_USE_INLINE} inline; {$ENDIF}
  end;


var
  CNT_BitBuffer_ByteTable:  array [0..255] of integer =
  (0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8);

implementation

//##########################################################################
//TDbLibUtils
//##########################################################################

// rounds a number to nearest factor of X
class function TDbLibUtils.ToNearest(const Value, Factor: cardinal): cardinal;
var
  lTemp: cardinal;
Begin
  result := Value;
  lTemp := Value mod Factor;
  if lTemp > 0 then
    inc(result, factor - lTemp);
end;

// Swaps the content of two variables
class procedure TDbLibUtils.Swap(var Primary, Secondary: cardinal);
var
  LTemp: cardinal;
Begin
  LTemp := Primary;
  Primary := Secondary;
  Secondary := LTemp;
end;

// Calculates difference between two numbers, counting zero
class function TDbLibUtils.Diff(const Primary, Secondary: cardinal;
  const Exclusive: boolean = false): cardinal;
Begin
  If Primary <> Secondary then
  begin
    If Primary > Secondary then
      result := (Primary - Secondary)
    else
      result := (Secondary - Primary);

    if Exclusive then
    begin
      If (Primary < 1) or (Secondary < 1) then
        inc(result);
    end;

  end else
    result := 0;
end;

// Returns the number of bits in X number of bytes
class function TDbLibUtils.BitsOf(const FromByteCount: cardinal): cardinal;
Begin
  result := cardinal(FromByteCount * 8);
end;

// Returns the number of bytes from X number of bits
class function TDbLibUtils.BytesOf(FromBitCount: cardinal): cardinal;
Begin
  if FromBitCount > 0 then
  begin
    if fromBitCount > 8 then
    begin
      FromBitCount := TDbLibUtils.ToNearest(FromBitCount, 8);
      result := FromBitCount div 8;
      if (result * 8) < FromBitCount then
        inc(result);
    end else
    result := 8;
  end else
  result := 0;
end;

class function TDbLibUtils.BitsSetInByte(const Value: byte): cardinal;
begin
  Result := CNT_BitBuffer_ByteTable[Value];
end;

class Function TDbLibUtils.BitGet(const Index: cardinal; const Buffer): boolean;
var
  LValue: byte;
  BitOfs: 0 .. 255;
  {$IFNDEF USE_ARRAYS}
  lAddr: PByte;
  lByteIndex: cardinal;
  {$ENDIF}
begin
  BitOfs := Index mod 8;
  {$IFDEF USE_ARRAYS}
  lValue := PByteAccessArray(@Buffer)^[index shr 3];
  Result := (LValue and (1 shl (BitOfs mod 8))) <> 0;
  {$ELSE}
  lAddr := @Buffer;
  lByteIndex := cardinal(Index div 8);
  inc(lAddr, lByteIndex);

  lValue := lAddr^;
  Result := (LValue and (1 shl (BitOfs mod 8))) <> 0;
  {$ENDIF}
end;

class procedure TDbLibUtils.BitSet(const Index: cardinal; var Buffer; const Value: boolean);
var
  lByte: byte;
  lByteIndex: cardinal;
  BitOfs: 0 .. 255;
  lCurrent: boolean;
  {$IFNDEF USE_ARRAYS}
  lAddr: PByte;
  {$ENDIF}
begin
  lByteIndex := Index div 8;
  {$IFDEF USE_ARRAYS}
  lByte := PByteAccessArray(@Buffer)^[lByteIndex];
  {$ELSE}
  lAddr := @Buffer;
  inc(lAddr, lByteIndex);
  lByte := lAddr^;
  {$ENDIF}

  BitOfs := Index mod 8;
  lCurrent := (lByte and (1 shl (BitOfs mod 8))) <> 0;

  case Value of
  true:
    begin
      //set bit if not already set *)
      if not LCurrent then
      {$IFDEF USE_ARRAYS}
      PByteAccessArray(@Buffer)^[lByteIndex] := (lByte or (1 shl (BitOfs mod 8)));
      {$ELSE}
      lAddr^ := (LByte or (1 shl (BitOfs mod 8)));
      {$ENDIF}
    end;
  false:
    begin
      // clear bit if already set
      if lCurrent then
      begin
      {$IFDEF USE_ARRAYS}
      //lByte := (PByteAccessArray(@Buffer)^[LByteIndex] and not (1 shl (BitOfs mod 8)));
      PByteAccessArray(@Buffer)^[LByteIndex] := LByte and not (1 shl (BitOfs mod 8));
      //PByteAccessArray(@Buffer)^[LByteIndex] := LByte;
      {$ELSE}
      //lByte := lAddr^ and not (1 shl (BitOfs mod 8));
      lAddr^ := lByte and not (1 shl (BitOfs mod 8));
      {$ENDIF}
      end;
    end;
  end;
end;

//##########################################################################
// TDbLibPersistentObject
//##########################################################################

constructor TDbLibPersistentObject.Create;
begin
  inherited;
  FState := [osCreating];
end;

procedure TDbLibPersistentObject.Afterconstruction;
begin
  inherited;
  Exclude(FState, osCreating);
end;

procedure TDbLibPersistentObject.BeforeDestruction;
begin
  include(FState, osDestroying);
  inherited;
end;

function TDbLibPersistentObject.GetObjectClass: TDbLibPersistentObjectClass;
begin
  result := TDbLibPersistentObjectclass(ClassType);
end;

class function TDbLibPersistentObject.ObjectPath: string;
var
  LAncestor:  TClass;
begin
  SetLength(result, 0);
  LAncestor := ClassParent;

  while LAncestor <> NIL do
  begin
    case result.length > 0 of
    true:   result := (LAncestor.ClassName + '.' + result);
    false:  result := LAncestor.ClassName;
    end;
    LAncestor := LAncestor.ClassParent;
  end;

  case result.length > 0 of
  true:   result := result + '.' + ClassName;
  false:  result := ClassName;
  end;
end;

function TDbLibPersistentObject.GetParent:TObject;
begin
  result := FParent;
end;

procedure TDbLibPersistentObject.SetParent(const ParentObj: TObject);
begin
  FParent := ParentObj;
end;

function TDbLibPersistentObject.GetObjectState: TObjectState;
begin
  result := FState;
end;

procedure TDbLibPersistentObject.AddObjectState(const Value: TObjectState);
begin
  FState := FState + Value;
end;

procedure TDbLibPersistentObject.RemoveObjectState(const Value: TObjectState);
begin
  FState := FState - Value;
end;

procedure TDbLibPersistentObject.SetObjectState(const Value: TObjectState);
begin
  FState := Value;
end;

function TDbLibPersistentObject.QueryObjectState(const Value: TObjectState):  boolean;
begin
  case (osCreating in Value) of
  true:   result := osCreating in FState;
  false:  result := false;
  end;

  if not result and (osDestroying in Value) then result := (osDestroying in FState);
  if not result and (osUpdating in Value) then result := (osUpdating in FState);
  if not result and (osReadWrite in Value) then result := (osReadWrite in FState);
  if not result and (osSilent in Value) then result := (osSilent in FState);
end;

function TDbLibPersistentObject.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
begin
  case GetInterface(IID, obj) of
  true:   result := S_OK;
  false:  result := E_NOINTERFACE;
  end;
end;

function TDbLibPersistentObject._AddRef: integer; stdcall;
begin
  // Inform COM that no reference counting is required
  result := -1;
end;

function TDbLibPersistentObject._Release: integer; stdcall;
begin
  // Inform COM that no reference counting is required
  result := -1;
end;

//##########################################################################
// TDbLibBase
//##########################################################################

function TDbLibInterfacedObject.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  case GetInterface(IID, obj) of
  true:   result := S_OK;
  false:  result := E_NOINTERFACE;
  end;
end;

function TDbLibInterfacedObject._AddRef: integer;
begin
  // Inform COM that no reference counting is required
  result := -1;
end;

function TDbLibInterfacedObject._Release: integer;
begin
  // Inform COM that no reference counting is required
  result := -1;
end;

//##########################################################################
//TDbLibErrorObject
//##########################################################################

procedure TDbLibErrorObject.ClearLastError;
begin
  FLastError := '';
end;

function TDbLibErrorObject.GetFailed: boolean;
begin
  result := length(FLastError) > 0;
end;

function TDbLibErrorObject.GetLastError: string;
begin
  result := FLastError;
end;

procedure TDbLibErrorObject.SetLastError(const Text: string);
begin
  FLastError := Text.Trim();
end;

procedure TDbLibErrorObject.SetLastErrorF(const Text: string;
  const Values: array of const);
begin
  FLastError := Format(Text, Values).Trim();
end;

end.
