unit dblib.buffer.memory;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.encoder;

type

  //  This class implements an in-memory-buffer.
  //  It allows you to allocate and manipulate a chunk of memory
  //  with the same simplicity as you would a TMemoryStream

  TDbLibBufferMemory = class(TDbLibBuffer)
  strict private
    FDataPTR:   Pbyte;
    FDataLen:   int64;
  strict protected
    function    BasePTR: Pbyte;
    function    AddrOf(const byteIndex: Int64): Pbyte;

    function    DoGetCapabilities: TDbLibBufferCapabilities; override;
    function    DoGetDataSize: Int64; override;
    procedure   DoReleaseData; override;
    procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); override;
    procedure   DoWriteData(Start: Int64;const Buffer; BufLen: integer); override;
    procedure   DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer); override;

    procedure   DoGrowDataBy(const Value: integer); override;
    procedure   DoShrinkDataBy(const Value: integer); override;
    procedure   DoZeroData; override;
  public
    property    Data: Pbyte read FDataPTR;
  end;


implementation

//##########################################################################
// TDbLibStreamAdapter
//##########################################################################

function TDbLibBufferMemory.BasePTR:Pbyte;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  (* Any pointer to return? *)
  if FDataPTR<>NIL then
    result:=FDataPTR
  else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['BasePTR',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBufferMemory.AddrOf(const byteIndex:Int64): Pbyte;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  (* Are we within range of the buffer? *)
  if (byteIndex>=0) and (byteIndex<FDataLen) then
  begin
    (* Note: if you have modified this class to working with
    memory-mapped files and go beyond MAXINT in the byteindex,
    Delphi can raise an EIntOverFlow exception *)
    result:=FDataPTR;
    inc(result,byteIndex);
  end else
  raise EDbLibBufferError.CreateFmt
  (CNT_ERR_BTRG_BYTEINDEXVIOLATION,[0,FDataLEN,byteIndex]);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['AddrOf',e.classname,e.message]);
  end;
  {$ENDIF}
end;

function TDbLibBufferMemory.DoGetCapabilities: TDbLibBufferCapabilities;
begin
  result := [mcScale, mcOwned, mcRead, mcWrite];
end;

function TDbLibBufferMemory.DoGetDataSize:Int64;
begin
  if FDataPTR <> nil then
    result := FDataLen
  else
    result := 0;
end;

procedure TDbLibBufferMemory.DoReleaseData;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  try
    if FDataPTR <> NIL then
      Freemem(FDataPTR);
  finally
    FDataPTR := NIL;
    FDataLEN := 0;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoReleaseData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferMemory.DoReadData(Start:Int64;
          var Buffer;BufLen:integer);
var
  mSource:    Pbyte;
  mTarget:    Pbyte;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  (* Calculate PTR's *)
  mSource := AddrOf(Start);
  mTarget := Addr(Buffer);
  move(mSource^, mTarget^, BufLen);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoReadData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferMemory.DoWriteData(Start:Int64;
          const Buffer;BufLen:integer);
var
  mSource:    Pbyte;
  mTarget:    Pbyte;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  (* Calculate PTR's *)
  mSource := Addr(Buffer);
  mTarget := AddrOf(start);
  move(mSource^, mTarget^, BufLen);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoWriteData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferMemory.DoGrowDataBy(const Value: integer);
var
  LNewSize: int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  try
    if FDataPTR <> nil then
    begin
      (* Re-scale current memory *)
      LNewSize := FDataLEN + Value;
      ReAllocMem(FDataPTR, LNewSize);
      FDataLen := LNewSize;
    end else
    begin
      (* Allocate new memory *)
      FDataPTR := AllocMem(Value);
      FDataLen := Value;
    end;
  except
    on e: exception do
    begin
      FDataLen := 0;
      FDataPTR := nil;
      raise EDbLibBufferError.CreateFmt
      (CNT_ERR_BTRG_SCALEFAILED,[e.message]);
    end;
  end;

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoGrowDataBy',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferMemory.DoShrinkDataBy(const Value: integer);
var
  LNewSize: int64;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FDataPTR <> nil then
  begin
    LNewSize := EnsureRange(FDataLEN - Value, 0, FDataLen);
    if LNewSize > 0 then
    begin
      if LNewSize <> FDataLen then
      begin
        try
          ReAllocMem(FDataPTR, LNewSize);
          FDataLen := LNewSize;
        except
          on e: exception do
          begin
            raise EDbLibBufferError.CreateFmt
            (CNT_ERR_BTRG_SCALEFAILED,[e.message]);
          end;
        end;
      end;
    end else
      DoReleaseData;
  end else
  raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoShrinkDataBy', e.classname, e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferMemory.DoZeroData;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}

  if FDataPTR <> nil then
    TDbLibBuffer.Fillbyte(FDataPTR, FDataLen, $00)
  else
    raise EDbLibBufferError.Create(CNT_ERR_BTRG_EMPTY);

  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoZeroData',e.classname,e.message]);
  end;
  {$ENDIF}
end;

procedure TDbLibBufferMemory.DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer);
var
  FSource:    Pbyte;
  FTarget:    Pbyte;
  FLongs:     integer;
  FSingles:   integer;
begin
  {$IFDEF HEX_DEBUG}
  try
  {$ENDIF}
  (* Initialize pointers *)
  FSource := Addr(Data);
  FTarget := self.AddrOf(Start);

  (* EVEN copy source into destination *)
  FLongs := FillLength div DataLen;
  While FLongs > 0 do
  begin
    Move(FSource^, FTarget^, DataLen);
    inc(FTarget, DataLen);
    dec(FLongs);
  end;

  (* ODD copy of source into destination *)
  FSingles := FillLength mod DataLen;
  if FSingles > 0 then
  begin
    Case FSingles of
    1: FTarget^ := FSource^;
    2: PWord(FTarget)^ := PWord(FSource)^;
    3: PDbTriplebyte(FTarget)^ := PDbTriplebyte(FSource)^;
    4: PCardinal(FTarget)^ := PCardinal(FSource)^;
    else
      Move(FSource^, FTarget^, FSingles);
    end;
  end;
  {$IFDEF HEX_DEBUG}
  except
    on e: exception do
    raise EDbLibBufferError.CreateFmt
    (CNT_ERR_BTRG_BASE,['DoFillData',e.classname,e.message]);
  end;
  {$ENDIF}
end;


end.
