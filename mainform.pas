unit mainform;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils,
  System.Variants, System.Classes, System.Dateutils, System.StrUtils,
  System.Generics.Collections,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,

  dblib.filearray,
  dblib.common,
  dblib.buffer,
  dblib.partaccess,
  dblib.buffer.memory,
  dblib.buffer.disk,
  dblib.encoder,
  dblib.bitbuffer,
  dblib.records,
  dblib.database;

type

  TfrmMain = class(TForm)
    memoOut: TMemo;
    Panel1: TPanel;
    btnWriteReadTest: TButton;
    procedure btnWriteReadTestClick(Sender: TObject);
  private
    { Private declarations }
    FDatabase: TDbLibDatabase;

    FFileArray: TDbLibLongFileList;

  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

{ TfrmMain }

constructor TfrmMain.Create(AOwner: TComponent);
begin
  inherited;
  // Create our database file, in memory
  FDatabase := TDbLibDatabase.Create();
  DoubleBuffered := true;
end;

destructor TfrmMain.Destroy;
begin
  FDatabase.Free;
  inherited;
end;


procedure TfrmMain.btnWriteReadTestClick(Sender: TObject);
var
  LTable: TDbLibTable;
  x: integer;
  lTime: TDateTime;
  lSecs: integer;
  offx: string;
  lRawData: TStringStream;
  lDbFile: string;

const
  db_acc: array[boolean] of TDbLibAccessMode = (
    TDbLibAccessMode.amCreate,
    TDbLibAccessMode.amReadWrite
    );

begin

  if FDatabase.Active then
  begin
    Showmessage('Database is active');
    exit;
  end;

  memoOut.Lines.Clear();
  memoOut.Lines.BeginUpdate();
  screen.Cursor := crHourGlass;
  btnWriteReadTest.Enabled := false;

  try
    try
      lDBFile := '.\test.data';
      FDatabase.Filename := lDBFile; //'::memory::';
      FDatabase.AccessMode := db_acc[FileExists(lDBFile)]; //TDbLibAccessMode.amCreate; //
      FDatabase.DatabaseName := 'temp';
      FDatabase.GrowthScheme := gsAggressive;

      if FDatabase.AccessMode = TDbLibAccessMode.amCreate then
      begin
        LTable := FDatabase.Tables.AddTable('main');
        LTable.FieldDefs.Addinteger('id');
        LTable.FieldDefs.AddStr('firstname');
        LTable.FieldDefs.AddStr('lastname');
        LTable.FieldDefs.AddStr('address1');
        LTable.FieldDefs.AddStr('address2');
        LTable.FieldDefs.AddInteger('zipcode');
        LTable.FieldDefs.AddStr('state');
        LTable.FieldDefs.AddStr('nation');
        LTable.FieldDefs.AddStr('phone');
        LTable.FieldDefs.AddStr('email');
        LTable.FieldDefs.Add('data', TDbLibFieldData);
      end;

      lTime := now;
      FDatabase.Open();

      if FDatabase.AccessMode = TDbLibAccessMode.amReadWrite then
      begin
        if not FDataBase.Tables.GetTableByName('main', LTable) then
          raise Exception.Create('Failed to locate table [main]');
      end;

      if FDatabase.AccessMode = TDbLibAccessMode.amCreate then
      begin
        lRawData := TStringStream.Create('UTF8 encoded binary data', TEncoding.UTF8);
        try
          lTime := now;
          for x := 1 to 1000000 do
          begin
            offx := x.ToString();
            LTable.Cursor.Lock(TDbLibCursorMode.cmInsert);
            LTable.Cursor.Fields.WriteStr('firstname', 'John' + offx);
            LTable.Cursor.Fields.WriteStr('lastname', 'Doe');
            LTable.Cursor.Fields.WriteStr('address1', 'address' + offx);
            LTable.Cursor.Fields.WriteStr('address2', 'address' + offx);
            LTable.Cursor.Fields.WriteInt('zipcode', x);
            LTable.Cursor.Fields.WriteStr('state', 'state' + offx);
            LTable.Cursor.Fields.WriteStr('nation', 'Norway');
            LTable.Cursor.Fields.WriteStr('phone', '+4745818777');
            LTable.Cursor.Fields.WriteStr('email', 'someone@overthemoon,com' + offx );
            LTable.Cursor.Fields.WriteData('data', lRawData, false);
            LTable.Cursor.Post();
            if x mod 10000 = 0 then
            begin
              Caption := format('%d records written ', [x]);
              application.ProcessMessages();
            end;
          end;
          lSecs := SecondsBetween(now, lTime);
        finally
          lRawData.Free;
        end;

        if application.Terminated then
          exit;

        Caption := format('%d records written in %d seconds', [lTable.RecordCount, lSecs]);
        application.ProcessMessages();
      end else
      begin
        lSecs := SecondsBetween(now, lTime);
        Caption := format('%d records available in table @ %d seconds', [lTable.RecordCount, lSecs]);
        application.ProcessMessages();
      end;

      // Now read back and emit to the form's memo
      LTable.Cursor.First();
      LTable.Cursor.RecNo := LTable.RecordCount - 1000;
      while not LTable.Cursor.EOF do
      begin
        memoOut.Lines.Add( LTable.Cursor.Fields.Fields['firstname'].AsString );
        LTable.Cursor.Next();
      end;

    finally
      if FDatabase.Active then
        FDatabase.Close();
    end;
  finally
    memoOut.lines.EndUpdate();
    screen.Cursor := crDefault;
    btnWriteReadTest.Enabled := true;
    application.ProcessMessages();
  end;
end;

end.
