unit dblib.filearray;

{$I 'dblib.inc'}

interface

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.records,
  dblib.buffer,
  dblib.readwrite,
  dblib.buffer.memory,
  dblib.buffer.disk,
  dblib.encoder,
  dblib.encoder.buffer;

type

  TDbLibCustomFileArray = Class(TDbLibSerializableObject)
  Private
    FFile:        TDbLibBufferFile;
    FFilename:    string;
    FSize:        integer;
    FTempFile:    boolean;
  Protected
    Procedure     BeforeReadObject; override;
    procedure     ReadObject(const Reader: TDbLibReader); override;
    procedure     WriteObject(const Writer: TDbLibWriter); override;
  Protected
    Function      ObjectHasData: boolean; override;
    Function      DoGetCount: integer;
    Function      DoGetItemSize: integer; virtual; abstract;
    Procedure     DoReadItem(Const ItemIndex: integer; var Data);
    Procedure     DoWriteItem(Const ItemIndex: integer; const Data);
    Procedure     DoDeleteItem(Const ItemIndex: integer);
    Function      DoAppendItem(Const Data): integer;
    Function      DoInsertItem(Const ItemIndex: integer;const Data): integer;
  Public
    Property      Empty: boolean read ObjectHasData;
    Property      Filename: string read FFilename;
    Property      Count: integer read DoGetCount;
    Procedure     Clear;
    Procedure     Delete(const Index: integer);

    Procedure     AfterConstruction; override;
    Constructor   Create(ThisFileName: string; Flags: word); reintroduce; virtual;
    Destructor    Destroy; override;
  End;

  TDbLibLongFileList = Class(TDbLibCustomFileArray)
  private
    function  GetItem(const Index: integer): cardinal;
    procedure SetItem(const Index: integer; const Value: cardinal);
  protected
    function  DoGetItemSize: integer; override;
  public
    property  Items[Const Index: integer]: cardinal read GetItem write SetItem; default;
    function  Add(const Value: cardinal): integer;
    function  Insert(const Index: integer; const Value: cardinal): integer;
  End;


implementation

uses
  System.IOUtils;

resourcestring

  ERR_SRL_LIST_FailedGrowList
  = 'Failed to grow array memory error';

  ERR_SRL_LIST_FailedAddData
  = 'Failed to allocate list item memory for adding error';

  ERR_SRL_LIST_FailedInsertData
  = 'Failed to allocate list item memory for list insert error';

  ERR_SRL_LIST_InvalidItemIndex
  = 'Invalid item index error (%d)';

  ERR_SRL_LIST_InvalidInputBuffer
  = 'Invalid input buffer size error';

  ERR_SRL_LIST_InvalidOutputBuffer
  = 'Invalid output buffer size error';

//###########################################################################
// TDbLibCustomFileArray
//###########################################################################

constructor TDbLibCustomFileArray.Create(ThisFileName: string; Flags: word);
begin
  inherited Create;

  ThisFileName := ThisFileName.Trim();
  if ThisFileName.Length < 1 then
  begin
    ThisFileName := TPath.GetTempFileName();
    FTempFile := true;
  end else
  FTempFile := false;

  if Flags = 0 then flags := fmCreate or fmShareDenyWrite;

  FFile := TDbLibBufferFile.CreateEx(ThisFileName, Flags);
end;

destructor TDbLibCustomFileArray.Destroy;
begin
  if FFile <> nil then
  begin

    // Close the file
    try
      if FFile.Active then
        FFile.Close();
    except
      on e: exception do;
    end;

    // Dispose of file-buffer instance
    try
      FreeAndNIL(FFile);
    except
      on e: exception do;
    end;
  end;

  // Delete file if its temporary
  if FTempFile then
  begin
    if FileExists(FFileName, true) then
    begin
      try
        DeleteFile(FFilename);
      except
        on e: exception do;
      end;
    end;
  end;

  inherited;
end;

Procedure TDbLibCustomFileArray.AfterConstruction;
Begin
  inherited;
  FSize := DoGetItemSize();
end;

Procedure TDbLibCustomFileArray.BeforeReadObject;
Begin
  inherited;
  If FFile.Size > 0 then
    FFile.Size := 0;
end;

Procedure TDbLibCustomFileArray.ReadObject(const Reader: TDbLibReader);
var
  ltemp:  int64;
Begin
  inherited ReadObject(Reader);
  lTemp := Reader.ReadInt64();
  If lTemp > 0 then
    Reader.CopyTo(FFile, lTemp);
end;

Procedure TDbLibCustomFileArray.WriteObject(const Writer: TDbLibWriter);
Begin
  inherited WriteObject(Writer);
  Writer.WriteInt64(FFile.Size);
  If FFile.Size > 0 then
    Writer.CopyFrom(FFile, FFile.Size);
end;

Procedure TDbLibCustomFileArray.Delete(Const Index:integer);
Begin
  DoDeleteItem(Index);
end;

Procedure TDbLibCustomFileArray.Clear;
Begin
  If FFile.Size > 0 then
    FFile.Release();
end;

Function TDbLibCustomFileArray.DoGetCount:integer;
var
  lTemp:  integer;
Begin
  lTemp := FFile.Size;
  If lTemp > 0 then
    result := lTemp div FSize
  else
    result := 0;
end;

Function TDbLibCustomFileArray.ObjectHasData: boolean;
Begin
  result := FFile.Size > 0;
end;

Procedure TDbLibCustomFileArray.DoReadItem(Const ItemIndex: integer; var Data);
var
  lOffset: int64;
begin
  If (ItemIndex >= 0) and (ItemIndex < DoGetCount() ) then
  Begin
    lOffset := ItemIndex * FSize;
    FFile.Read(lOffset, FSize, Data);
  end else
  Raise Exception.CreateFmt(ERR_SRL_LIST_InvalidItemIndex,[ItemIndex]);
end;

function TDbLibCustomFileArray.DoInsertItem(Const ItemIndex: integer; const Data): integer;
var
  lOffset: Int64;
begin
  If ItemIndex >= 0 then
  Begin
    If ItemIndex < DoGetCount() then
    Begin
      lOffset := ItemIndex * FSize;
      FFile.Insert(lOffset, Data, FSize);
      result := ItemIndex;
    end else
    Result := DoAppendItem(Data);
  end else
  Raise Exception.CreateFmt(ERR_SRL_LIST_InvalidItemIndex,[ItemIndex]);
end;

Procedure TDbLibCustomFileArray.DoWriteItem(Const ItemIndex:integer;Const Data);
var
  lOffset: int64;
Begin
  If (ItemIndex >= 0) and (ItemIndex < Count) then
  Begin
    lOffset := ItemIndex * FSize;
    FFile.Write(lOffset, FSize, Data);
  end else
  Raise Exception.CreateFmt(ERR_SRL_LIST_InvalidItemIndex,[ItemIndex]);
end;

Procedure TDbLibCustomFileArray.DoDeleteItem(Const ItemIndex:integer);
var
  lOffset: int64;
Begin
  If (ItemIndex >= 0) and (ItemIndex < Count) then
  begin
    lOffset := ItemIndex * FSize;
    FFile.Remove(lOffset, FSize);
  end else
  raise Exception.CreateFmt(ERR_SRL_LIST_InvalidItemIndex,[ItemIndex]);
end;

Function TDbLibCustomFileArray.DoAppendItem(const Data): integer;
begin
  FFile.Append(Data, FSize);
  result := (FFile.Size div FSize);
  dec(result);
end;

//###########################################################################
// TDbLibLongFileList
//###########################################################################

function TDbLibLongFileList.DoGetItemSize: integer;
begin
  result := SizeOf(cardinal);
end;

function TDbLibLongFileList.GetItem(Const Index:integer): cardinal;
begin
  DoReadItem(Index, result);
end;

procedure TDbLibLongFileList.SetItem(Const Index: integer;Const Value: cardinal);
begin
  DoWriteItem(Index,Value);
end;

function TDbLibLongFileList.Add(Const Value: cardinal): integer;
begin
  result := DoAppendItem(Value);
end;

function TDbLibLongFileList.Insert(const Index: integer; const Value: cardinal): integer;
Begin
  result := DoInsertItem(Index, Value);
end;



end.
