unit dblib.records;

interface

{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.readwrite,
  dblib.buffer.memory,
  dblib.buffer.disk,
  dblib.encoder,
  dblib.encoder.buffer;

type
  // Exception classes
  EDbLibPersistent        = class(EDbLibError);
  EDbLibRecordError       = class(EDbLibError);
  EDbLibRecordFieldError  = class(EDbLibError);

  // class types
  TDbLibRecordFieldclass  = class of TDbLibRecordField;
  TDbLibRecordFieldArray  = array of TDbLibRecordFieldClass;

  // Forward declarations
  TDbLibRecordField   = class;
  TDbLibFieldInteger  = class;
  TDbLibFieldString   = class;
  TDbLibFieldByte     = class;
  TDbLibFieldBoolean  = class;
  TDbLibFieldCurrency = class;
  TDbLibFieldData     = class;
  TDbLibFieldDateTime = class;
  TDbLibFieldDouble   = class;
  TDbLibFieldGUID     = class;
  TDbLibFieldInt64    = class;
  TDbLibFieldLong     = class;

  IExtendedPersistence = interface
    ['{282CC310-CD3B-47BF-8EB0-017C1EDF0BFC}']
    procedure   ObjectFrom(const Reader: TDbLibReader);
    procedure   ObjectFromStream(const Stream: TStream; const Disposable: boolean);
    procedure   ObjectFromData(const Data: TDbLibBuffer; const Disposable: boolean);
    procedure   ObjectFromFile(const Filename: string);

    procedure   ObjectTo(const Writer: TDbLibWriter);
    function    ObjectToStream: TStream; overload;
    procedure   ObjectToStream(const Stream: TStream); overload;
    function    ObjectToData: TDbLibBuffer; overload;
    procedure   ObjectToData(const Data: TDbLibBuffer); overload;
    procedure   ObjectToFile(const Filename: string);
  end;

  IDbLibFields = interface
    ['{0D6A9FE2-24D2-42AE-A343-E65F18409FA2}']
    function    IndexOf(FieldName: string):  integer;
    function    ObjectOf(FieldName: string): TDbLibRecordField;

    function    Add(const FieldName: string; const FieldClass: TClass): TDbLibRecordField;
    function    Addinteger(const FieldName: string): TDbLibFieldInteger;
    function    AddStr(const FieldName: string): TDbLibFieldString;
    function    Addbyte(const FieldName: string): TDbLibFieldbyte;
    function    AddBool(const FieldName: string): TDbLibFieldboolean;
    function    AddCurrency(const FieldName: string): TDbLibFieldCurrency;
    function    AddData(const FieldName: string): TDbLibFieldData;
    function    AddDateTime(const FieldName: string): TDbLibFieldDateTime;
    function    AddDouble(const FieldName: string): TDbLibFieldDouble;
    function    AddGUID(const FieldName: string):  TDbLibFieldGUID;
    function    AddInt64(const FieldName: string): TDbLibFieldInt64;
    function    AddLong(const FieldName: string): TDbLibFieldLong;
  end;

  TDbLibSerializableObject = class(TDbLibPersistentObject, IExtendedPersistence)
  private
    FObjId:     cardinal;
    FUpdCount:  integer;
  strict protected
    // Implements:: IExtendedPersistence
    procedure   ObjectTo(const Writer: TDbLibWriter);
    procedure   ObjectFrom(const Reader: TDbLibReader);
    procedure   ObjectFromStream(const Stream: TStream; const Disposable: boolean);
    function    ObjectToStream: TStream; overload;
    procedure   ObjectToStream(const Stream: TStream); overload;
    procedure   ObjectFromData(const Binary: TDbLibBuffer; const Disposable: boolean);
    function    ObjectToData: TDbLibBuffer; overload;
    procedure   ObjectToData(const Binary: TDbLibBuffer); overload;
    procedure   ObjectFromFile(const Filename: string);
    procedure   ObjectToFile(const Filename: string);

  protected
    procedure   BeforeUpdate; virtual;
    procedure   AfterUpdate; virtual;

  strict protected
    // Persistency Read/Write methods
    procedure   BeforeReadObject; virtual;
    procedure   AfterReadObject; virtual;
    procedure   BeforeWriteObject; virtual;
    procedure   AfterWriteObject; virtual;
    procedure   ReadObject(const Reader: TDbLibReader); virtual;
    procedure   WriteObject(const Writer: TDbLibWriter); virtual;

  strict protected
    // Standard persistence
    function    ObjectHasData: boolean; virtual;
    procedure   ReadObjBin(Stream: TStream); virtual;
    procedure   WriteObjBin(Stream: TStream); virtual;
    procedure   DefineProperties(Filer: TFiler); override;

  public
    property    UpdateCount: integer read FUpdCount;

    procedure   Assign(Source: TPersistent); override;
    function    ObjectIdentifier: cardinal; inline;

    function    BeginUpdate: boolean; inline;
    procedure   EndUpdate; inline;

    class function ClassIdentifier: cardinal; inline;

    constructor Create; override;
  end;

  TDbLibRecordField = class(TDbLibBufferMemory)
  private
    FName:      string;
    FNameHash:  Int64;
    FOnRead:    TNotifyEvent;
    FOnWrite:   TNotifyEvent;
    FOnRelease: TNotifyEvent;
    procedure   SetRecordName(NewName: string);
  protected
    function    GetDisplayName: string; virtual;
    procedure   BeforeReadObject; override;
    procedure   ReadObject(const Reader: TReader); override;
    procedure   WriteObject(const Writer: TWriter); override;
    procedure   DoReleaseData; override;
  protected
    procedure   SignalWrite;
    procedure   SignalRead;
    procedure   SignalRelease;
  public
    function    AsString: string; virtual;abstract;
    property    DisplayName: string read GetDisplayName;
    property    FieldSignature:Int64 read FNameHash;
  published
    property    OnValueRead: TNotifyEvent read FOnRead write FOnRead;
    property    OnValueWrite: TNotifyEvent read FOnWrite write FOnWrite;
    property    OnValueRelease: TNotifyEvent read FOnRelease write FOnRelease;
    property    FieldName: string read FName write SetRecordName;
  end;

  TDbLibFieldboolean = class(TDbLibRecordField)
  private
    function    GetValue: boolean;
    procedure   SetValue(const NewValue: boolean);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value: boolean read GetValue write SetValue;
  end;

  TDbLibFieldbyte = class(TDbLibRecordField)
  private
    function    GetValue: byte;
    procedure   SetValue(const NewValue:byte);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:byte read GetValue write SetValue;
  end;

  TDbLibFieldCurrency = class(TDbLibRecordField)
  private
    function    GetValue: Currency;
    procedure   SetValue(const NewValue: Currency);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:Currency read GetValue write SetValue;
  end;

  TDbLibFieldData = class(TDbLibRecordField)
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  end;

  TDbLibFieldDateTime = class(TDbLibRecordField)
  private
    function    GetValue:TDateTime;
    procedure   SetValue(const NewValue:TDateTime);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:TDateTime read GetValue write SetValue;
  end;

  TDbLibFieldDouble = class(TDbLibRecordField)
  private
    function    GetValue:Double;
    procedure   SetValue(const NewValue:Double);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:Double read GetValue write SetValue;
  end;

  TDbLibFieldGUID = class(TDbLibRecordField)
  private
    function    GetValue:TGUID;
    procedure   SetValue(const NewValue:TGUID);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:TGUID read GetValue write SetValue;
  end;

  TDbLibFieldInteger = class(TDbLibRecordField)
  private
    function    GetValue: integer;
    procedure   SetValue(const NewValue:integer);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:integer read GetValue write SetValue;
  end;

  TDbLibFieldInt64 = class(TDbLibRecordField)
  private
    function    GetValue:Int64;
    procedure   SetValue(const NewValue:Int64);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value:Int64 read GetValue write SetValue;
  end;

  TDbLibFieldString = class(TDbLibRecordField)
  private
    FLength:    integer;
    FExplicit:  boolean;
    FEncoding:  TEncoding;
    function    GetValue: string;
    procedure   SetValue(NewValue: string);
    procedure   SetFieldLength(Value: integer);
    procedure   SetEncoding(const NewEncoding: TEncoding);
  protected
    function    GetDisplayName: string; override;
  public
    procedure   Assign(Source: TPersistent); override;

    function    AsString: string; override;
    constructor Create; override;
  published
    property    Encoding: TEncoding read FEncoding write SetEncoding;
    property    Value: string read GetValue write SetValue;
    property    Length: integer read FLength write SetFieldLength;
    property    Explicit: boolean read FExplicit write FExplicit;
  end;

  TDbLibFieldLong = class(TDbLibRecordField)
  private
    function    GetValue: cardinal;
    procedure   SetValue(const NewValue:cardinal);
  protected
    function    GetDisplayName: string; override;
  public
    function    AsString: string; override;
  published
    property    Value: cardinal read GetValue write SetValue;
  end;

  TDbLibCustomRecord = class(TComponent, IDbLibFields, IStreamPersist)
  strict private
    FObjects:   TObjectList<TDbLibRecordField>;
    FEncoding:  TEncoding;

  strict protected
    procedure   SetEncoding(const NewEncoding: TEncoding);

    function    GetItem(const Index:integer): TDbLibRecordField;
    procedure   SetItem(const Index: integer; const Value: TDbLibRecordField);

    function    GetField(const FieldName: string): TDbLibRecordField;
    procedure   SetField(const FieldName: string; const Value: TDbLibRecordField);

    function    GetCount: integer;

    property    Fields[const FieldName: string]: TDbLibRecordField read GetField write SetField;
    property    Items[const Index: integer]: TDbLibRecordField read GetItem write SetItem;
    property    Count: integer read GetCount;
  public
    function    Add(const FieldName: string; const FieldClass: TClass): TDbLibRecordField;
    function    Addinteger(const FieldName: string): TDbLibFieldInteger;
    function    AddStr(const FieldName: string): TDbLibFieldString;
    function    Addbyte(const FieldName: string): TDbLibFieldbyte;
    function    AddBool(const FieldName: string): TDbLibFieldboolean;
    function    AddCurrency(const FieldName: string): TDbLibFieldCurrency;
    function    AddData(const FieldName: string): TDbLibFieldData;
    function    AddDateTime(const FieldName: string): TDbLibFieldDateTime;
    function    AddDouble(const FieldName: string): TDbLibFieldDouble;
    function    AddGUID(const FieldName: string):  TDbLibFieldGUID;
    function    AddInt64(const FieldName: string): TDbLibFieldInt64;
    function    AddLong(const FieldName: string): TDbLibFieldLong;

    procedure   WriteInt(const FieldName: string;const Value: integer);
    procedure   WriteStr(const FieldName: string;const Value: string);
    procedure   Writebyte(const FieldName: string;const Value: byte);
    procedure   WriteBool(const FieldName: string;const Value: boolean);
    procedure   WriteCurrency(const FieldName: string;const Value: currency);

    procedure   WriteData(const FieldName: string;
                const Value: TStream; const Disposable: boolean = false);

    procedure   WriteDateTime(const FieldName: string; const Value: TDateTime);
    procedure   WriteDouble(const FieldName: string; const Value: double);
    procedure   WriteGUID(const FieldName: string; const Value: TGUID);
    procedure   WriteInt64(const FieldName: string; const Value: int64);
    procedure   WriteLong(const FieldName: string; const Value: cardinal);

    procedure   Clear; virtual;

    procedure   Assign(Source: TPersistent); override;

    function    ToStream: TStream; virtual;
    function    ToBuffer: TDbLibBuffer; virtual;

    procedure   FromStream(const Stream: TStream; const Disposable: boolean = true);

    // Implements:: IStreamPersist
    procedure   LoadFromStream(Stream: TStream); virtual;
    procedure   SaveToStream(Stream: TStream); virtual;

    function    IndexOf(FieldName: string):  integer;
    function    ObjectOf(FieldName: string): TDbLibRecordField;

    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    property    Encoding: TEncoding read FEncoding write SetEncoding;
  end;

  TDbLibRecord = class(TDbLibCustomRecord)
  public
    property  Fields;
    property  Items;
    property  Count;
  end;

  procedure DbLibRegisterRecordField(AClass: TDbLibRecordFieldclass);

  function  DbLibRecordFieldKnown(AClass: TDbLibRecordFieldclass):  boolean;

  function  DbLibRecordFieldClassFromName(aName: string;
            var Aclass: TDbLibRecordFieldClass):  boolean;

  function  DbLibRecordInstanceFromName(aName: string;
            out Value:TDbLibRecordField):  boolean;

  function DbLibStrToGUID(const Value: Ansistring):  TGUID;
  function DbLibGUIDToStr(const GUID: TGUID):  Ansistring;

implementation

const
  CNT_RECORD_HEADER = $BAADBABE;

//###########################################################################
// Persistancy errors
//###########################################################################
const
  ERR_HEX_PERSISTENCY_ASSIGNCONFLICT
  = '%s can not be assigned to %s ';

  ERR_HEX_PERSISTENCY_INVALIDSIGNATURE
  = 'Invalid signature, found %s, expected %s';

  ERR_HEX_PERSISTENCY_INVALIDREADER
  = 'Invalid reader object error';

  ERR_HEX_PERSISTENCY_INVALIDWRITER
  = 'Invalid writer object error';

//###########################################################################
// Record management errors
//###########################################################################

  ERR_RECORDFIELD_INVALIDNAME =
  'Invalid field name [%s] error';

  ERR_RECORDFIELD_FailedSet =
  'Writing to field buffer [%s] failed error';

  ERR_RECORDFIELD_FailedGet =
  'Reading from field buffer [%s] failed error';

  ERR_RECORDFIELD_FieldIsEmpty
  = 'Record field is empty [%s] error';

var
  _Fieldclasses:  TDbLibRecordFieldArray;


function DbLibGUIDToStr(const GUID: TGUID):  Ansistring;
begin
  SetLength(result, 38);
  StrLFmt(@result[1],38,'{%.8x-%.4x-%.4x-%.2x%.2x-%.2x%.2x%.2x%.2x%.2x%.2x}',
  [GUID.D1, GUID.D2, GUID.D3, GUID.D4[0], GUID.D4[1], GUID.D4[2], GUID.D4[3],
  GUID.D4[4], GUID.D4[5], GUID.D4[6], GUID.D4[7]]);
end;

function DbLibStrToGUID(const Value:Ansistring): TGUID;
const
  ERR_InvalidGUID = '[%s] is not a valid GUID value';
var
  i:  integer;
  src, dest: PAnsiChar;

  function _HexChar(const C: AnsiChar):  byte;
  begin
    case C of
      '0'..'9': result := byte(c) - byte('0');
      'a'..'f': result := (byte(c) - byte('a')) + 10;
      'A'..'F': result := (byte(c) - byte('A')) + 10;
    else        raise Exception.CreateFmt(ERR_InvalidGUID,[Value]);
    end;
  end;

  function _Hexbyte(const P:PAnsiChar):  AnsiChar;
  begin
    result := AnsiChar((_HexChar(p[0]) shl 4)+_HexChar(p[1]));
  end;

begin
  if Length(Value)=38 then
  begin
    dest := @result;
    src := PAnsiChar(Value);
    Inc(src);

    for i := 0 to 3 do
    dest[i] := _Hexbyte(src+(3-i)*2);

    Inc(src, 8);
    Inc(dest, 4);
    if src[0] <> '-' then
    raise Exception.CreateFmt(ERR_InvalidGUID,[Value]);

    Inc(src);
    for i := 0 to 1 do
    begin
      dest^ := _Hexbyte(src+2);
      Inc(dest);
      dest^ := _Hexbyte(src);
      Inc(dest);
      Inc(src, 4);
      if src[0] <> '-' then
      raise Exception.CreateFmt(ERR_InvalidGUID,[Value]);
      inc(src);
    end;

    dest^ := _Hexbyte(src);
    Inc(dest);
    Inc(src, 2);
    dest^ := _Hexbyte(src);
    Inc(dest);
    Inc(src, 2);
    if src[0] <> '-' then
    raise Exception.CreateFmt(ERR_InvalidGUID,[Value]);

    Inc(src);
    for i := 0 to 5 do
    begin
      dest^:=_Hexbyte(src);
      Inc(dest);
      Inc(src, 2);
    end;
  end else
  raise Exception.CreateFmt(ERR_InvalidGUID,[Value]);
end;

procedure DbLibRegisterRecordField(AClass: TDbLibRecordFieldclass);
var
  FLen: integer;
begin
  if AClass <> NIL then
  begin
    if DbLibRecordFieldKnown(Aclass) = false then
    begin
      FLen := Length(_Fieldclasses);
      Setlength(_Fieldclasses, FLen + 1);
      _Fieldclasses[FLen] := AClass;
    end;
  end;
end;

function DbLibRecordFieldKnown(AClass: TDbLibRecordFieldclass): boolean;
var
  x:  integer;
begin
  result:=Aclass<>NIl;
  if result then
  begin
    result:=Length(_Fieldclasses)>0;
    if result then
    begin
      result:=False;
      for x:=low(_Fieldclasses) to high(_Fieldclasses) do
      begin
        result:=_Fieldclasses[x]=Aclass;
        if result then
        break;
      end;
    end;
  end;
end;

function DbLibRecordFieldclassFromName(AName: string;
          var AClass: TDbLibRecordFieldclass): boolean;
var
  x:  integer;
begin
  Aclass := nil;
  result := false;
  if Length(_Fieldclasses) > 0 then
  begin
    for x:=low(_Fieldclasses) to high(_Fieldclasses) do
    begin
      result:=_Fieldclasses[x].className=aName;
      if result then
      begin
        Aclass := _Fieldclasses[x];
        break;
      end;
    end;
  end;
end;

function DbLibRecordInstanceFromName(AName: string;
    out Value: TDbLibRecordField): boolean;
var
  LClass: TDbLibRecordFieldclass;
begin
  result := DbLibRecordFieldclassFromName(aName, LClass);
  if result then
    Value := LClass.Create();
end;

//##########################################################################
// TDbLibSerializableObject
//##########################################################################

constructor TDbLibSerializableObject.Create;
begin
  inherited;
  FObjId := ClassIdentifier;
end;

procedure TDbLibSerializableObject.Assign(Source: TPersistent);
begin
  If Source <> nil then
  begin
    if source is TDbLibSerializableObject then
    begin
      (* Always supports object of same class *)
      if IObjectLifeCycle(TDbLibSerializableObject(Source)).GetObjectclass() = GetObjectclass then
      begin
        If Source <> Self then
          ObjectFromData(TDbLibSerializableObject(Source).ObjectToData,True);
      end else
      // no support, raise exception
      raise EDbLibPersistent.CreateFmt(ERR_HEX_PERSISTENCY_ASSIGNCONFLICT,
        [TDbLibSerializableObject(Source).ObjectPath,ObjectPath]);
    end;
  end;
end;

function TDbLibSerializableObject.ObjectHasData: boolean;
begin
  result:=False;
end;

procedure TDbLibSerializableObject.DefineProperties(Filer: TFiler);
begin
  inherited;
  filer.DefineBinaryproperty('$RES',ReadObjBin,WriteObjBin,ObjectHasData);
end;

procedure TDbLibSerializableObject.ReadObjBin(Stream: TStream);
var
  mReader:  TDbLibReaderStream;
begin
  mReader:=TDbLibReaderStream.Create(Stream);
  try
    ObjectFrom(mReader);
  finally
    mReader.Free;
  end;
end;

procedure TDbLibSerializableObject.WriteObjBin(Stream: TStream);
var
  mWriter:  TDbLibWriterStream;
begin
  mWriter:=TDbLibWriterStream.Create(Stream);
  try
    ObjectTo(mWriter);
  finally
    mWriter.Free;
  end;
end;

function TDbLibSerializableObject.ObjectIdentifier: cardinal;
begin
  result := FObjId;
end;

class function TDbLibSerializableObject.ClassIdentifier: cardinal;
begin
  result := TDbLibBuffer.ElfHash(ObjectPath);
end;

// IExtendedPersistence: ObjectToStream
procedure TDbLibSerializableObject.ObjectToStream(const Stream: TStream);
var
  FWriter: TDbLibWriterStream;
begin
  FWriter:=TDbLibWriterStream.Create(Stream);
  try
    ObjectTo(FWriter);
  finally
    FWriter.free;
  end;
end;

// IExtendedPersistence: ObjectToBinary
procedure TDbLibSerializableObject.ObjectToData(const Binary:TDbLibBuffer);
var
  FWriter: TDbLibWriterBuffer;
begin
  FWriter:=TDbLibWriterBuffer.Create(Binary);
  try
    ObjectTo(FWriter);
  finally
    FWriter.free;
  end;
end;

// IExtendedPersistence: ObjectToBinary
function TDbLibSerializableObject.ObjectToData: TDbLibBuffer;
begin
  Result := TDbLibBufferMemory.Create();
  try
    ObjectToData(Result);
  except
    on e: exception do
    begin
      FreeAndNil(Result);
      raise EDbLibPersistent.Create(e.Message);
    end;
  end;
end;

// IExtendedPersistence: ObjectToStream
function TDbLibSerializableObject.ObjectToStream: TStream;
begin
  result := TMemoryStream.Create;
  try
    ObjectToStream(Result);
    result.Position := 0;
  except
    on e: exception do
    begin
      FreeAndNil(Result);
      raise EDbLibPersistent.Create(e.message);
    end;
  end;
end;

// IExtendedPersistence: ObjectFrom
procedure TDbLibSerializableObject.ObjectFrom(const Reader:TDbLibReader);
begin
  if Reader = nil then
    raise EDbLibPersistent.Create(ERR_HEX_PERSISTENCY_INVALIDREADER);

  If BeginUpdate() then
  begin
    BeforeReadObject;
    ReadObject(Reader);
    AfterReadObject;
    EndUpdate;
  end;
end;

// IExtendedPersistence: ObjectTo
procedure TDbLibSerializableObject.ObjectTo(const Writer:TDbLibWriter);
begin
  if Writer = nil then
    raise EDbLibPersistent.Create(ERR_HEX_PERSISTENCY_INVALIDWRITER);

  BeforeWriteObject;
  WriteObject(Writer);
  AfterWriteObject;
end;

// IExtendedPersistence: ObjectFromBinary
procedure TDbLibSerializableObject.ObjectFromData(const Binary: TDbLibBuffer; const Disposable: boolean);
var
  FReader: TDbLibReaderBuffer;
begin
  FReader:=TDbLibReaderBuffer.Create(Binary);
  try
    ObjectFrom(FReader);
  finally
    FReader.free;
    If Disposable then
    Binary.free;
  end;
end;

procedure TDbLibSerializableObject.BeforeWriteObject;
begin
  AddObjectState([osReadWrite]);
end;

procedure TDbLibSerializableObject.BeforeReadObject;
begin
  AddObjectState([osReadWrite]);
end;

procedure TDbLibSerializableObject.AfterReadObject;
begin
  RemoveObjectState([osReadWrite]);
end;

procedure TDbLibSerializableObject.AfterWriteObject;
begin
  RemoveObjectState([osReadWrite]);
end;

procedure TDbLibSerializableObject.WriteObject(const Writer: TDbLibWriter);
begin
  // write identifier to stream
  Writer.WriteLong(FObjId);
end;

procedure TDbLibSerializableObject.ReadObject(const Reader: TDbLibReader);
var
  lReadId:  cardinal;
begin
  // read identifier from stream
  lReadId := Reader.ReadLong();

  If lReadId <> FObjId then
    raise EDbLibPersistent.CreateFmt
    (ERR_HEX_PERSISTENCY_INVALIDSIGNATURE,
    [IntToHex(lReadId, 8),IntToHex(FObjId,8)]);
end;

(* ISRLPersistent: ObjectFromStream *)
procedure TDbLibSerializableObject.ObjectFromStream
          (const Stream: TStream; const Disposable: boolean);
var
  lReader:  TDbLibReaderStream;
begin
  lReader := TDbLibReaderStream.Create(Stream);
  try
    ObjectFrom(lReader);
  finally
    lReader.free;

    if Disposable then
      Stream.free;
  end;
end;

(* ISRLPersistent: ObjectfromFile *)
procedure TDbLibSerializableObject.ObjectfromFile(const Filename: string);
begin
  ObjectFromStream(TFileStream.Create(filename, fmOpenRead or fmShareDenyWrite), true);
end;

(* ISRLPersistent: ObjectToFile *)
procedure TDbLibSerializableObject.ObjectToFile(const Filename: string);
var
  lFile:  TFileStream;
begin
  lFile := TFileStream.Create(filename, fmCreate);
  try
    ObjectToStream(lFile);
  finally
    lFile.free;
  end;
end;

procedure TDbLibSerializableObject.BeforeUpdate;
begin
end;

procedure TDbLibSerializableObject.AfterUpdate;
begin
end;

function TDbLibSerializableObject.beginUpdate: boolean;
begin
  result := QueryObjectState([osDestroying]) = false;
  if result then
  begin
    inc(FUpdCount);
    If FUpdCount = 1 then
    begin
      AddObjectState([osUpdating]);
      BeforeUpdate;
    end;
  end;
end;

procedure TDbLibSerializableObject.EndUpdate;
begin
  If QueryObjectState([osUpdating]) then
  begin
    dec(FUpdCount);
    If FUpdCount < 1 then
    begin
      RemoveObjectState([osUpdating]);
      AfterUpdate;
    end;
  end;
end;

//##########################################################################
// TDbLibCustomRecord
//##########################################################################

constructor TDbLibCustomRecord.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FObjects := TObjectList<TDbLibRecordField>.Create(true);
  FEncoding := TEncoding.UTF8;
end;

destructor TDbLibCustomRecord.Destroy;
begin
  FObjects.free;
  inherited;
end;

procedure TDbLibCustomRecord.SetEncoding(const NewEncoding: TEncoding);
begin
  // Encoding cannot be blank
  if NewEncoding <> nil then
    FEncoding := NewEncoding;
end;

procedure TDbLibCustomRecord.Assign(Source: TPersistent);
var
  lSourceRecord: TDbLibCustomRecord;
  lSourceField: TDbLibRecordField;
  lTargetField: TDbLibRecordField;
  x: integer;
begin
  if Source <> nil then
  begin
    if Source is TDbLibCustomRecord then
    begin
      lSourceRecord := TDbLibCustomRecord(Source);

      Clear();
      FEncoding := lSourceRecord.Encoding;

      for x := 0 to lSourceRecord.Count-1 do
      begin
        lSourceField := lSourceRecord.Items[x];
        lTargetField := Add(lSourceField.FieldName, lSourceField.ClassType);
        lTargetField.Assign(lSourceField);
      end;

    end else
    inherited;
  end else
  inherited;
end;

procedure TDbLibCustomRecord.Clear;
begin
  FObjects.Clear();
  FEncoding := TEncoding.UTF8;
end;

procedure TDbLibCustomRecord.FromStream(const Stream: TStream; const Disposable: boolean = true);
begin
  if Stream <> nil then
  begin
    LoadFromStream(Stream);
    if Disposable then
      Stream.Free();
  end else
    raise Exception.Create('Failed to load from stream, stream was nil error');
end;

function TDbLibCustomRecord.ToStream: TStream;
begin
  result:=TMemoryStream.Create;
  try
    SaveToStream(result);
    result.Position:=0;
  except
    on exception do
    begin
      FreeAndNIL(result);
      raise;
    end;
  end;
end;

function TDbLibCustomRecord.ToBuffer: TDbLibBuffer;
var
  mAdapter: TDbLibStreamAdapter;
begin
  result := TDbLibBufferMemory.Create();
  try
    mAdapter:=TDbLibStreamAdapter.Create(result);
    try
      SaveToStream(mAdapter);
    finally
      mAdapter.Free;
    end;
  except
    on exception do
    begin
      FreeAndNIL(result);
      raise;
    end;
  end;
end;

procedure TDbLibCustomRecord.SaveToStream(Stream: TStream);
var
  x:  integer;
  LWriter:  TWriter;
  LHead:  cardinal;
begin
  LWriter := TWriter.Create(stream,1024);
  try
    LHead := CNT_RECORD_HEADER;
    LWriter.Write(LHead, SizeOf(LHead) );
    LWriter.Writeinteger(FObjects.Count);

    for x := 0 to FObjects.Count-1 do
    begin
      LWriter.Writestring(items[x].className);
      items[x].WriteObject(LWriter);
    end;

  finally
    LWriter.FlushBuffer();
    LWriter.Free;
  end;
end;

procedure TDbLibCustomRecord.LoadFromStream(Stream: TStream);
var
  x:  integer;
  mReader:  TReader;
  mHead:  cardinal;
  mCount: integer;
  mName:  string;
  mField: TDbLibRecordField;
begin
  Clear();
  mReader := TReader.Create(stream, 1024 * 1024);
  try
    mReader.Read(mHead,SizeOf(mHead));
    if mHead=CNT_RECORD_HEADER then
    begin
      mCount:=mReader.Readinteger;
      for x := 0 to mCount-1 do
      begin
        mName := mReader.Readstring;
        if DbLibRecordInstanceFromName(mName, mField) then
        begin
          self.FObjects.Add(mField);
          mField.ReadObject(mReader);
        end else
        raise EDbLibRecordError.CreateFmt('Unknown field class [%s] error',[mName]);
      end;
    end else
    raise EDbLibRecordError.Create('Invalid record header error');
  finally
    mReader.Free;
  end;
end;

procedure TDbLibCustomRecord.WriteInt(const FieldName: string;const Value:integer);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldInteger);
  TDbLibFieldInteger(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteStr(const FieldName: string;const Value: string);
var
  LRef: TDbLibRecordField;
begin
  LRef := ObjectOf(FieldName);
  if LRef = nil then
    LRef := Add(FieldName, TDbLibFieldString);
  TDbLibFieldString(LRef).Value := Value;
end;

procedure TDbLibCustomRecord.Writebyte(const FieldName: string;const Value:byte);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldbyte);
  TDbLibFieldbyte(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteBool(const FieldName: string;const Value: boolean);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldboolean);
  TDbLibFieldboolean(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteCurrency(const FieldName: string;
          const Value:Currency);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldCurrency);
  TDbLibFieldCurrency(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteData(const FieldName: string; const Value: TStream; const Disposable: boolean = false);
var
  LRef: TDbLibRecordField;
begin
  LRef := ObjectOf(FieldName);
  if LRef = nil then
    LRef := Add(FieldName,TDbLibFieldData);

  if value <> nil then
  begin
    Value.Position := 0;
    TDbLibFieldData(LRef).LoadFromStream(value);

    // Release input stream?
    if Disposable then
      Value.Free();

  end;
end;

procedure TDbLibCustomRecord.WriteDateTime(const FieldName: string;
          const Value:TDateTime);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldDateTime);
  TDbLibFieldDateTime(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteDouble(const FieldName: string;
          const Value:Double);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldDouble);
  TDbLibFieldDouble(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteGUID(const FieldName: string; const Value: TGUID);
var
  mRef: TDbLibRecordField;
begin
  mRef := ObjectOf(FieldName);
  if mRef = NIL then
    mRef:=Add(FieldName, TDbLibFieldGUID);
  TDbLibFieldGUID(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteInt64(const FieldName: string;
          const Value:Int64);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldInt64);
  TDbLibFieldInt64(mRef).Value:=Value;
end;

procedure TDbLibCustomRecord.WriteLong(const FieldName: string;
          const Value:cardinal);
var
  mRef: TDbLibRecordField;
begin
  mRef:=ObjectOf(FieldName);
  if mRef=NIL then
  mRef:=Add(FieldName,TDbLibFieldLong);
  TDbLibFieldLong(mRef).Value:=Value;
end;

function TDbLibCustomRecord.Addinteger(const FieldName: string): TDbLibFieldInteger;
begin
  result := TDbLibFieldInteger( Add(FieldName, TDbLibFieldInteger) );
end;

function TDbLibCustomRecord.AddStr(const FieldName: string): TDbLibFieldString;
begin
  result := TDbLibFieldString(Add(FieldName, TDbLibFieldString));
end;

function TDbLibCustomRecord.Addbyte(const FieldName: string): TDbLibFieldbyte;
begin
  result:=TDbLibFieldbyte(Add(FieldName,TDbLibFieldbyte));
end;

function TDbLibCustomRecord.AddBool(const FieldName: string): TDbLibFieldboolean;
begin
  result:=TDbLibFieldboolean(Add(FieldName,TDbLibFieldboolean));
end;

function TDbLibCustomRecord.AddCurrency(const FieldName: string): TDbLibFieldCurrency;
begin
  result:=TDbLibFieldCurrency(Add(FieldName,TDbLibFieldCurrency));
end;

function TDbLibCustomRecord.AddData(const FieldName: string): TDbLibFieldData;
begin
  result:=TDbLibFieldData(Add(FieldName,TDbLibFieldData));
end;

function TDbLibCustomRecord.AddDateTime(const FieldName: string): TDbLibFieldDateTime;
begin
  result:=TDbLibFieldDateTime(Add(FieldName,TDbLibFieldDateTime));
end;

function TDbLibCustomRecord.AddDouble(const FieldName: string): TDbLibFieldDouble;
begin
  result:=TDbLibFieldDouble(Add(FieldName,TDbLibFieldDouble));
end;

function TDbLibCustomRecord.AddGUID(const FieldName: string): TDbLibFieldGUID;
begin
  result := TDbLibFieldGUID(Add(FieldName,TDbLibFieldGUID));
end;

function TDbLibCustomRecord.AddInt64(const FieldName: string): TDbLibFieldInt64;
begin
  result:=TDbLibFieldInt64(Add(FieldName,TDbLibFieldInt64));
end;

function TDbLibCustomRecord.AddLong(const FieldName: string): TDbLibFieldLong;
begin
  result:=TDbLibFieldLong(Add(FieldName,TDbLibFieldLong));
end;

function TDbLibCustomRecord.Add(const FieldName: string;
         const FieldClass: TClass): TDbLibRecordField;
begin
  result := ObjectOf(FieldName);
  if result = nil then
  begin
    if Fieldclass <> nil then
    begin
      if FieldClass.InheritsFrom(TDbLibRecordField) then
      begin
        result := TDbLibRecordFieldclass(Fieldclass).Create();
        result.FieldName := FieldName;
        FObjects.Add(result);

        // If the instance is a string-field, copy over the
        // encoding settings from here
        if (result is TDbLibFieldString) then
          TDbLibFieldString(result).Encoding := FEncoding;
      end else
      raise EDbLibRecordError.Create('Failed to add field, unknown or unsupported field class error');
    end else
    result:=NIL;
  end;
end;

function TDbLibCustomRecord.GetCount: integer;
begin
  result:=FObjects.Count;
end;

function TDbLibCustomRecord.GetItem(const Index:integer): TDbLibRecordField;
begin
  result:=TDbLibRecordField(FObjects[index]);
end;

procedure TDbLibCustomRecord.SetItem(const Index: integer;
          const value:TDbLibRecordField);
begin
  TDbLibRecordField(FObjects[index]).Assign(Value);
end;

function TDbLibCustomRecord.GetField(const FieldName: string): TDbLibRecordField;
begin
  result:=ObjectOf(FieldName);
end;

procedure TDbLibCustomRecord.SetField(const FieldName: string;
          const Value:TDbLibRecordField);
var
  FItem: TDbLibRecordField;
begin
  FItem:=ObjectOf(FieldName);
  if FItem<>NIL then
  FItem.assign(Value);
end;

function TDbLibCustomRecord.IndexOf(FieldName: string):  integer;
var
  x:  integer;
begin
  result:=-1;
  FieldName := trim(FieldName);
  FieldName := lowercase(FieldName);
  if length(FieldName)>0 then
  begin
    for x:=0 to FObjects.Count-1 do
    begin
      if lowercase(GetItem(x).FieldName) = FieldName then
      begin
        result:=x;
        Break;
      end;
    end;
  end;
end;

function TDbLibCustomRecord.ObjectOf(FieldName: string): TDbLibRecordField;
var
  x:      integer;
  FItem:  TDbLibRecordField;
begin
  result := NIL;
  FieldName := trim(FieldName);
  FieldName := lowercase(FieldName);
  if length(FieldName)>0 then
  begin
    for x:=0 to FObjects.Count-1 do
    begin
      FItem:=GetItem(x);
      if lowercase(FItem.FieldName) = FieldName then
      begin
        result:=FItem;
        Break;
      end;
    end;
  end;
end;

//##########################################################################
// TDbLibFieldLong
//##########################################################################

function TDbLibFieldLong.AsString: string;
begin
  result := IntToStr(Value);
end;

function TDbLibFieldLong.GetDisplayName: string;
begin
  result := 'cardinal';
end;

function TDbLibFieldLong.GetValue: cardinal;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result),result) < SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet,[FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty,[FieldName]);
end;

procedure TDbLibFieldLong.SetValue(const NewValue:cardinal);
begin
  if Write(0,SizeOf(NewValue),NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet,[FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TSRLFieldInt64
//##########################################################################

function TDbLibFieldInt64.AsString: string;
begin
  result := IntToStr(Value);
end;

function TDbLibFieldInt64.GetDisplayName: string;
begin
  result := 'Int64';
end;

function TDbLibFieldInt64.GetValue:Int64;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result),result)<SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet, [FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty, [FieldName]);
end;

procedure TDbLibFieldInt64.SetValue(const NewValue:Int64);
begin
  if Write(0,SizeOf(NewValue),NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet, [FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldInteger
//##########################################################################

function TDbLibFieldInteger.AsString: string;
begin
  result := IntToStr(Value);
end;

function TDbLibFieldInteger.GetDisplayName: string;
begin
  result := 'integer';
end;

function TDbLibFieldInteger.GetValue: integer;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result), result)<SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet,[FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty,[FieldName]);
end;

procedure TDbLibFieldInteger.SetValue(const NewValue:integer);
begin
  if Write(0,SizeOf(NewValue), NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet,[FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldGUID
//##########################################################################

function TDbLibFieldGUID.AsString: string;
begin
  result := string( DbLibGUIDToStr(Value) );
end;

function TDbLibFieldGUID.GetDisplayName: string;
begin
  result := 'GUID';
end;

function TDbLibFieldGUID.GetValue:TGUID;
begin
  if not Empty then
  begin
    if Read(0, SizeOf(result),result) < SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet,[FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty,[FieldName]);
end;

procedure TDbLibFieldGUID.SetValue(const NewValue:TGUID);
begin
  if Write(0, SizeOf(NewValue), NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet,[FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldDateTime
//##########################################################################

function TDbLibFieldDateTime.AsString: string;
begin
  result := DateTimeToStr(Value);
end;

function TDbLibFieldDateTime.GetDisplayName: string;
begin
  result := 'DateTime';
end;

function TDbLibFieldDateTime.GetValue:TDateTime;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result),result) < SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet,[FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty, [FieldName]);
end;

procedure TDbLibFieldDateTime.SetValue(const NewValue:TDateTime);
begin
  if Write(0, SizeOf(NewValue), NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet,[FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldDouble
//##########################################################################

function TDbLibFieldDouble.AsString: string;
begin
  result := FloatToStr(Value);
end;

function TDbLibFieldDouble.GetDisplayName: string;
begin
  result := 'Double';
end;

function TDbLibFieldDouble.GetValue:Double;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result),result) < SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RecordField_FailedGet, [FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty, [FieldName]);
end;

procedure TDbLibFieldDouble.SetValue(const NewValue:Double);
begin
  if Write(0, SizeOf(NewValue), NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet,[FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldData
//##########################################################################

function TDbLibFieldData.AsString: string;
begin
  result := '[Binary]';
end;

function TDbLibFieldData.GetDisplayName: string;
begin
  result := 'Binary';
end;

//##########################################################################
// TDbLibFieldCurrency
//##########################################################################

function TDbLibFieldCurrency.AsString: string;
begin
  result := CurrToStr(Value);
end;

function TDbLibFieldCurrency.GetDisplayName: string;
begin
  result := 'Currency';
end;

function TDbLibFieldCurrency.GetValue:Currency;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result), result) <  SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet,[FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty, [FieldName]);
end;

procedure TDbLibFieldCurrency.SetValue(const NewValue:Currency);
var
  lSize: integer;
begin
  lSize := SizeOf(NewValue);
  if Write(0, lSize, NewValue) < lSize then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet, [FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldbyte
//##########################################################################

function TDbLibFieldbyte.AsString: string;
begin
  result := IntToStr(Value);
end;

function TDbLibFieldbyte.GetDisplayName: string;
begin
  result := 'byte';
end;

function TDbLibFieldbyte.GetValue: byte;
begin
  if not Empty then
  begin
    if Read(0, SizeOf(result), result) < SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet, [FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty, [FieldName]);
end;

procedure TDbLibFieldbyte.SetValue(const NewValue:byte);
begin
  if Write(0, SizeOf(NewValue), NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet, [FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldboolean
//##########################################################################

function TDbLibFieldboolean.AsString: string;
begin
  result := BoolToStr(Value, True);
end;

function TDbLibFieldboolean.GetDisplayName: string;
begin
  result := 'boolean';
end;

function TDbLibFieldboolean.GetValue: boolean;
begin
  if not Empty then
  begin
    if Read(0,SizeOf(result), result) < SizeOf(result) then
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet, [FieldName])
    else
      SignalRead();
  end else
  raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FieldIsEmpty, [FieldName]);
end;

procedure TDbLibFieldboolean.SetValue(const NewValue: boolean);
begin
  if Write(0,SizeOf(NewValue), NewValue) < SizeOf(NewValue) then
    raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet, [FieldName])
  else
    SignalWrite();
end;

//##########################################################################
// TDbLibFieldString
//##########################################################################

constructor TDbLibFieldString.Create;
begin
  inherited Create();
  FEncoding := TEncoding.UTF8;
  FLength := 0;
  FExplicit := false;
end;

procedure TDbLibFieldString.Assign(Source: TPersistent);
begin
  if Source <> nil then
  begin
    if Source is TDbLibFieldString then
    begin
      FLength := TDbLibFieldString(Source).Length;
      FExplicit := TDbLibFieldString(Source).Explicit;
      FEncoding := TDbLibFieldString(Source).Encoding;
      inherited;
    end else
    inherited;
  end else
  inherited;
end;

function TDbLibFieldString.AsString: string;
begin
  result := Value;
end;

function TDbLibFieldString.GetDisplayName: string;
begin
  result := 'string';
end;

procedure TDbLibFieldString.SetFieldLength(Value: integer);
begin
  if FExplicit then
  begin
    if Value <> FLength then
    begin
      if Value < 0 then
        value := 0;

      if Value > 0 then
      begin
        FLength := Value;
        if FLength <> Size then
          Size := FLength;
      end else
      begin
        FLength := 0;
        Release();
      end;
    end;
  end;
end;

procedure TDbLibFieldString.SetEncoding(const NewEncoding: TEncoding);
begin
  if NewEncoding <> nil then
    FEncoding := NewEncoding;
end;

function TDbLibFieldString.GetValue: string;
var
  LBytes: TArray<byte>;
begin
  setlength(result, 0);
  if not Empty then
  begin
    if Size > 0 then
    begin

      SetLength(LBytes, Size);
      if Read(0, Size, LBytes[0]) < Size then
        raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedGet,[FieldName])
      else
      begin
        result := FEncoding.GetString(LBytes);
        SignalRead();
      end;

    end;
  end;
end;

procedure TDbLibFieldString.SetValue(NewValue: string);
var
  lLen: integer;
  LBytes: TArray<byte>;
begin
  lLen := System.Length(NewValue);
  if lLen > 0 then
  begin
    (* cut string to length if explicit *)
    if FExplicit then
    begin
      if lLen > FLength then
        lLen := FLength;
      Setlength(NewValue, lLen);
    end;

    LBytes := FEncoding.GetBytes(NewValue);
    Size := System.Length(LBytes);

    if Write(0, Size, LBytes[0]) < Size then
        raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_FailedSet,[FieldName])
      else
        SignalWrite();
  end else
  Release();
end;

//##########################################################################
// TDbLibRecordField
//##########################################################################

function TDbLibRecordField.GetDisplayName: string;
begin
  result := 'Unknown';
end;

procedure TDbLibRecordField.SignalWrite;
begin
  if assigned(FOnWrite) then
    FOnWrite(self);
end;

procedure TDbLibRecordField.SignalRead;
begin
  if assigned(FOnRead) then
    FOnRead(self);
end;

procedure TDbLibRecordField.SignalRelease;
begin
  if assigned(FOnRelease) then
    FOnRelease(self);
end;

procedure TDbLibRecordField.SetRecordName(NewName: string);
begin
  NewName := trim(NewName);
  if NewName <> FName then
  begin
    if system.Length(NewName) > 0 then
    begin
      FName := NewName;
      FNameHash := ElfHash( LowerCase(NewName) )
    end else
      raise EDbLibRecordFieldError.CreateFmt(ERR_RECORDFIELD_INVALIDNAME,[NewName]);
  end;
end;

procedure TDbLibRecordField.BeforeReadObject;
begin
  inherited;
  FName := '';
  FNameHash := 0;
end;

procedure TDbLibRecordField.ReadObject(const Reader: TReader);
begin
  inherited;
  FNameHash := Reader.ReadInt64();
  FName := Reader.ReadString();
end;

procedure TDbLibRecordField.WriteObject(const Writer: TWriter);
begin
  inherited;
  Writer.Writeinteger(FNameHash);
  Writer.Writestring(FName);
end;

procedure TDbLibRecordField.DoReleaseData;
begin
  inherited;
  SignalRelease;
end;

procedure InitializeFramework;
begin
  DbLibRegisterRecordField(TDbLibFieldboolean);
  DbLibRegisterRecordField(TDbLibFieldbyte);
  DbLibRegisterRecordField(TDbLibFieldCurrency);
  DbLibRegisterRecordField(TDbLibFieldData);
  DbLibRegisterRecordField(TDbLibFieldDateTime);
  DbLibRegisterRecordField(TDbLibFieldDouble);
  DbLibRegisterRecordField(TDbLibFieldGUID);
  DbLibRegisterRecordField(TDbLibFieldInt64);
  DbLibRegisterRecordField(TDbLibFieldInteger);
  DbLibRegisterRecordField(TDbLibFieldLong);
  DbLibRegisterRecordField(TDbLibFieldString);
end;

procedure FinalizeFramwork;
begin
  (* We only store class-types in this array, no instances,
     so we can just clear it for brewity - nothing to release *)
  SetLength(_Fieldclasses,0);
end;

initialization
begin
  InitializeFramework();
end;

finalization
begin
  FinalizeFramwork();
end;

end.
