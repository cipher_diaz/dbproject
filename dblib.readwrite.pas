unit dblib.readwrite;

interface


{$I 'dblib.inc'}

uses
  System.SysUtils,
  System.classes,
  System.Math,
  System.Generics.Collections,
  System.Variants,
  dblib.common,
  dblib.buffer,
  dblib.buffer.memory;

const
  CNT_HEX_ANSI_STRING_HEADER  = $DB07;  // Ansi string encoding
  CNT_HEX_ASCI_STRING_HEADER  = $DB08;  // Ascii string encoding
  CNT_HEX_UNIC_STRING_HEADER  = $DB09;  // unicode string encoding
  CNT_HEX_UTF7_STRING_HEADER  = $DB0A;  // UTF7
  CNT_HEX_UTF8_STRING_HEADER  = $DB0B;  // UTF8
  CNT_HEX_DEFA_STRING_HEADER  = $DB0C;  // Default encoding (Do not localize!)
  CNT_HEX_VARIANT_HEADER      = $DB0E;  // variant header value

type
  // Forward declarations
  TDbLibReader  = class;
  TDbLibWriter  = class;

  // Exception objects
  EDbLibReader  = class(EDbLibError);
  EDbLibWriter  = class(EDbLibError);

  // Abstract reader class
  TDbLibReader = class
  private
    FOffset:    Int64;
    FEncoding:  TEncoding;
  protected
    procedure   Advance(const Value: integer); inline;
  public
    property    Position: Int64 read FOffset;
    property    TextEncoding: TEncoding read FEncoding write FEncoding;
    function    ReadNone(Length: integer):  integer;

    // Noobs: Only use this when working with memory-mapping.
    // The moment the data is released, a pointer is invalid
    function    ReadPointer: pointer;

    function    Readbyte: byte;
    function    ReadBool: boolean;
    function    ReadWord: word;
    function    ReadSmall: smallint;
    function    ReadInt: integer;
    function    ReadLong: cardinal;
    function    ReadInt64: int64;
    function    ReadCurrency: currency;
    function    ReadDouble: double;
    function    ReadShort: shortint;
    function    ReadSingle: single;
    function    ReadDateTime: TDateTime;
    function    ReadGUID: TGUID;

    function    ReadString: string; overload;

    function    CopyTo(const Writer: TDbLibWriter; CopyLen: integer):  integer; overload;
    function    CopyTo(const Stream: TStream; const CopyLen: integer):  integer; overload;
    function    CopyTo(const Binary: TDbLibBuffer; const CopyLen: integer):  integer; overload;

    function    ReadStream: TStream;
    function    ReadData: TDbLibBuffer;
    function    ContentToStream: TStream;
    function    ContentToData: TDbLibBuffer;

    {$IFDEF HEX_SUPPORT_VARIANTS}
    function    ReadVariant: variant;
    {$ENDIF}
    procedure   Reset; virtual;

    function    Read(var Data; DataLen: integer):  integer; virtual; abstract;
  end;

  // Abstract writer class
  TDbLibWriter = class
  private
    FOffset:    Int64;
    FEncoding:  TEncoding;
  protected
    procedure   Advance(const Value: integer);
    procedure   __FillWord(dstAddr: PWord; const inCount: integer;
                const Value: Word);
  public
    property    TextEncoding: TEncoding read FEncoding write FEncoding;
    procedure   WritePointer(const Value: pointer);
    {$IFDEF HEX_SUPPORT_VARIANTS}
    procedure   WriteVariant(const Value: variant);
    {$ENDIF}
    procedure   WriteCRLF(const Times: integer=1);
    procedure   Writebyte(const Value: byte);
    procedure   WriteBool(const Value: boolean);
    procedure   WriteWord(const Value: word);
    procedure   WriteShort(const Value: shortint);
    procedure   WriteSmall(const Value: smallInt);
    procedure   WriteInt(const Value: integer);
    procedure   WriteLong(const Value: cardinal);
    procedure   WriteInt64(const Value: int64);
    procedure   WriteCurrency(const Value: currency);
    procedure   WriteDouble(const Value: double);
    procedure   WriteSingle(const Value: single);
    procedure   WriteDateTime(const Value: TDateTime);

    procedure   Writestring(const Text: string); overload;
    procedure   Writestring(const Text: string;const Encoding: TEncoding); overload;

    procedure   WriteStreamContent(const Content: TStream; const Disposable: boolean = false);
    procedure   WriteDataContent(const Content: TDbLibBuffer; const Disposable: boolean = false);
    procedure   WriteStream(const Value: TStream; const Disposable: boolean);
    procedure   WriteData(const Data: TDbLibBuffer; const Disposable: boolean);
    procedure   WriteGUID(const Value: TGUID);
    procedure   WriteFile(const Filename: string);
    function    CopyFrom(const Reader: TDbLibReader;DataLen: Int64):  Int64; overload;
    function    CopyFrom(const Stream: TStream;const DataLen: Int64):  Int64; overload;
    function    CopyFrom(const Data: TDbLibBuffer;const DataLen: Int64):  Int64; overload;
    function    Write(const Data; DataLen: integer):  integer; virtual;abstract;
    property    Position: Int64 read FOffset;
    procedure   Reset; virtual;
  end;

  (* Reader class for buffers *)
  TDbLibReaderBuffer = class(TDbLibReader)
  private
    FData:      TDbLibBuffer;
  public
    function    Read(var Data; DataLen: integer):  integer; override;
    constructor Create(const Source: TDbLibBuffer);reintroduce;
  end;

  (* Writer class for buffers *)
  TDbLibWriterBuffer = class(TDbLibWriter)
  private
    FData: TDbLibBuffer;
  public
    property Data: TDbLibBuffer read FData;
    function Write(const Data; DataLen: integer):  integer; override;
    constructor Create(const Target: TDbLibBuffer);reintroduce;
  end;

  (* reader class for stream *)
  TDbLibReaderStream = class(TDbLibReader)
  private
    FStream:    TStream;
  public
    function    Read(var Data; DataLen: integer):  integer; override;
    constructor Create(const Source: TStream);reintroduce;
  end;

  (* writer class for stream *)
  TDbLibWriterStream = class(TDbLibWriter)
  private
    FStream:    TStream;
  public
    property    DataStream: TStream read FStream;
    function    Write(const Data; DataLen: integer):  integer; override;
    constructor Create(const Target: TStream);reintroduce;
  end;


implementation

resourcestring
  ERR_HEX_READER_INVALIDSOURCE        = 'Invalid source medium error';
  ERR_HEX_READER_FAILEDREAD           = 'Read failed on source medium';
  ERR_HEX_READER_INVALIDDATASOURCE    = 'Invalid data source for read operation';
  ERR_HEX_READER_INVALIDOBJECT        = 'Invalid object for read operation';
  ERR_HEX_READER_INVALIDHEADER        = 'Invalid header, expected %d not %d';

  // Error messages for TDbLibWriter decendants
  ERR_HEX_WRITER_INVALIDTARGET        = 'Invalid target medium error';
  ERR_HEX_WRITER_FAILEDWRITE          = 'Write failed on target medium';
  ERR_HEX_WRITER_INVALIDDATASOURCE    = 'Invalid data source for write operation';
  ERR_HEX_WRITER_INVALIDOBJECT        = 'Invalid object for write operation';


//##########################################################################
// TDbLibReader
//##########################################################################

procedure TDbLibReader.Reset;
begin
  FOffset:=0;
end;

procedure TDbLibReader.Advance(const Value:integer);
begin
  if Value > 0 then
    FOffset := FOffset + Value;
end;

function TDbLibReader.Readbyte: byte;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadBool: boolean;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadWord:Word;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadSmall:SmallInt;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadPointer:Pointer;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadInt: integer;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadLong:cardinal;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadInt64:Int64;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadCurrency:Currency;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadGUID:TGUID;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadShort:Shortint;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadSingle:Single;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadDouble:Double;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function TDbLibReader.ReadDateTime:TDateTime;
begin
  if Read(result, SizeOf(result)) < SizeOf(result) then
    raise EDbLibReader.Create(ERR_HEX_READER_FAILEDREAD);
end;

function  TDbLibReader.CopyTo(const Writer: TDbLibWriter; CopyLen: integer): integer;
var
  FRead:        integer;
  FbytesToRead: integer;
  FWritten:     integer;
  FCache:       TDbLibIOCache;
begin
  if Writer <> nil then
  begin
    result := 0;
    While CopyLen > 0 do
    begin
      FBytesToRead := EnsureRange(SizeOf(FCache), 0, CopyLen);

      FRead := Read(FCache,FbytesToRead);
      if FRead > 0 then
      begin
        FWritten := Writer.Write(FCache, FRead);
        dec(CopyLen, FWritten);
        inc(result, FWritten);
      end else
        break;
    end;
  end else
  raise EDbLibReader.Create(ERR_HEX_READER_INVALIDOBJECT);
end;

function TDbLibReader.CopyTo(const Binary: TDbLibBuffer; const CopyLen: integer): integer;
var
  FWriter: TDbLibWriter;
begin
  if Binary <> nil then
  begin
    FWriter := TDbLibWriterBuffer.Create(Binary);
    try
      result:=CopyTo(FWriter,CopyLen);
    finally
      FWriter.free;
    end;
  end else
  raise EDbLibReader.Create(ERR_HEX_READER_INVALIDOBJECT);
end;

function  TDbLibReader.CopyTo(const Stream: TStream;
          const CopyLen:integer):  integer;
var
  FWriter: TDbLibWriterStream;
begin
  if Stream<>NIL then
  begin
    FWriter:=TDbLibWriterStream.Create(Stream);
    try
      result:=CopyTo(FWriter,CopyLen);
    finally
      FWriter.free;
    end;
  end else
  raise EDbLibReader.Create(ERR_HEX_READER_INVALIDOBJECT);
end;

function TDbLibReader.ContentToStream: TStream;
var
  FRead:  integer;
  FCache: TDbLibIOCache;
begin
  result := TMemoryStream.Create;
  try
    While True do
    begin
      FRead := Read(FCache, SizeOf(FCache));
      if FRead > 0 then
        result.WriteBuffer(FCache,FRead)
      else
        break;
    end;
  except
    on e: exception do
    begin
      FreeAndNil(result);
      raise EDbLibReader.Create(e.message);
    end;
  end;
end;

function TDbLibReader.ReadNone(Length: integer):  integer;
var
  FToRead:  integer;
  FRead:    integer;
  FCache:   TDbLibIOCache;
begin
  result := 0;
  if Length > 0 then
  begin
    try
      While Length > 0 do
      begin
        FToRead := EnsureRange(SizeOf(FCache), 0, Length);
        FRead := Read(FCache, FToRead);
        if FRead > 0 then
        begin
          Length := Length - FRead;
          result := result + FRead;
        end else
          break;
      end;
    except
      on e: exception do
      raise EDbLibReader.Create(e.message);
    end;
  end;
end;

function TDbLibReader.ContentToData: TDbLibBuffer;
var
  FRead:  integer;
  FCache: TDbLibIOCache;
begin
  result := TDbLibBufferMemory.Create();
  try
    While True do
    begin
      FRead := Read(FCache, SizeOf(FCache));
      if FRead > 0 then
        result.Append(FCache, FRead)
      else
        break;
    end;
  except
    on e: exception do
    begin
      FreeAndNil(result);
      raise EDbLibReader.Create(e.message);
    end;
  end;
end;

function TDbLibReader.ReadString: string;
var
  LBytes: integer;
  LEncoding: word;
  LTemp: TBytes;
begin
  // Read the encoding
  LEncoding := ReadWord;

  // Read the length in bytes of the encoded data
  LBytes := ReadInt;

  // Anything to decode?
  if LBytes>0 then
  begin
    //LData := allocmem(LBytes);
    SetLength(LTemp, LBytes);
    try
      // Read the data chunk
      Read(LTemp[0], LBytes);

      case LEncoding of
      CNT_HEX_ANSI_STRING_HEADER:  result := TEncoding.ANSI.GetString(LTemp);
      CNT_HEX_ASCI_STRING_HEADER:  result := TEncoding.ASCII.GetString(LTemp);
      CNT_HEX_UNIC_STRING_HEADER:  result := TEncoding.Unicode.GetString(LTemp);
      CNT_HEX_UTF7_STRING_HEADER:  result := TEncoding.UTF7.GetString(LTemp);
      CNT_HEX_UTF8_STRING_HEADER:  result := TEncoding.UTF8.GetString(LTemp);
      CNT_HEX_DEFA_STRING_HEADER:  result := TEncoding.Default.GetString(LTemp);
      else
        raise EDbLibReader.CreateFmt
        (ERR_HEX_READER_INVALIDHEADER,[CNT_HEX_DEFA_STRING_HEADER,LEncoding]);
      end;

    finally
      setlength(LTemp, 0);
    end;
  end;
end;

{$IFDEF HEX_SUPPORT_VARIANTS}
function TDbLibReader.ReadVariant: Variant;
var
  FTemp:    Word;
  FKind:    TVarType;
  FCount,x: integer;
  FIsArray: boolean;

  function ReadVariantData:Variant;
  var
    FTyp: TVarType;
  begin
    FTyp := ReadWord();
    case FTyp of
    varError:     result := VarAsError(ReadLong);
    varVariant:   result := ReadVariant;
    varbyte:      result := Readbyte;
    varboolean:   result := ReadBool;
    varShortInt:  result := ReadShort;
    varWord:      result := ReadWord;
    varSmallint:  result := ReadSmall;
    varinteger:   result := ReadInt;
    varcardinal:  result := ReadLong;
    varInt64:     result := ReadInt64;
    varSingle:    result := ReadSingle;
    varDouble:    result := ReadDouble;
    varCurrency:  result := ReadCurrency;
    varDate:      result := ReadDateTime;
    varstring:    result := Readstring;
    varOleStr:    result := Readstring;
    end;
  end;

begin
  FTemp := ReadWord();
  if FTemp = CNT_HEX_VARIANT_HEADER then
  begin
    (* read datatype *)
    FKind := TVarType(ReadWord);

    if not (FKind in [varEmpty, varNull]) then
    begin
      (* read array declaration *)
      FIsArray:=ReadBool;

      if FIsArray then
      begin
        FCount := ReadInt;
        result := VarArrayCreate([0,FCount-1], FKind);
        for x := 1 to FCount do
          VarArrayPut(result, ReadVariantData, [0,x-1]);
      end else
        result := ReadVariantData();
    end else
      result := null;

  end else
  raise EDbLibReader.CreateFmt(ERR_HEX_READER_INVALIDHEADER,[CNT_HEX_VARIANT_HEADER,FTemp]);
end;
{$ENDIF}

function TDbLibReader.ReadData: TDbLibBuffer;
var
  FTotal:   Int64;
  FToRead:  integer;
  FRead:    integer;
  FCache:   TDbLibIOCache;
begin
  result := TDbLibBufferMemory.Create();
  try
    FTotal:=ReadInt64;
    While FTotal>0 do
    begin
      FToRead := EnsureRange(SizeOf(FCache),0,FTotal);
      FRead := Read(FCache, FToRead);
      if FRead>0 then
      begin
        result.Append(FCache,FRead);
        FTotal:=FTotal - FRead;
      end else
      Break;
    end;
  except
    on e: exception do
    begin
      FreeAndNil(result);
      raise EDbLibReader.Create(e.message);
    end;
  end;
end;

function TDbLibReader.ReadStream: TStream;
var
  FTotal:   Int64;
  FToRead:  integer;
  FRead:    integer;
  FCache:   TDbLibIOCache;
begin
  result := TMemoryStream.Create;
  try
    FTotal := ReadInt64();

    While FTotal>0 do
    begin
      FToRead := EnsureRange( SizeOf(FCache), 0, FTotal);
      FRead := Read(FCache, FToRead);
      if FRead > 0 then
      begin
        Result.WriteBuffer(FCache, FRead);
        FTotal := FTotal - FRead;
      end else
        break;
    end;

    result.Position := 0;
  except
    on e: exception do
    begin
      FreeAndNil(result);
      raise EDbLibReader.Create(e.message);
    end;
  end;
end;

//##########################################################################
// TDbLibWriter
//##########################################################################

procedure TDbLibWriter.Reset;
begin
  FOffset := 0;
end;

procedure TDbLibWriter.Advance(const Value: integer);
begin
  if Value > 0 then
    FOffset := FOffset + Value;
end;

{$IFDEF HEX_SUPPORT_VARIANTS}
procedure TDbLibWriter.WriteVariant(const Value: variant);
var
  FKind:    TVarType;
  FCount,x: integer;
  FIsArray: boolean;

  procedure WriteVariantData(const VarValue: variant);
  var
    FAddr:  PVarData;
  begin
    FAddr:=FindVarData(VarValue);
    if FAddr<>NIL then
    begin
      (* write datatype *)
      WriteWord(FAddr^.VType);

      (* write variant content *)
      Case FAddr^.VType of
      varVariant:   WriteVariantData(VarValue);
      varError:     WriteLong(TVarData(varValue).VError);
      varbyte:      Writebyte(FAddr^.Vbyte);
      varboolean:   WriteBool(FAddr^.Vboolean);
      varShortInt:  WriteShort(FAddr^.VShortInt);
      varWord:      WriteWord(FAddr^.VWord);
      varSmallint:  WriteSmall(FAddr^.VSmallInt);
      varinteger:   WriteInt(FAddr^.Vinteger);
      varcardinal:  WriteLong(FAddr^.Vcardinal);
      varInt64:     WriteInt64(FAddr^.VInt64);
      varSingle:    WriteSingle(FAddr^.VSingle);
      varDouble:    WriteDouble(FAddr^.VDouble);
      varCurrency:  WriteCurrency(FAddr^.VCurrency);
      varDate:      WriteDateTime(FAddr^.VDate);
      varstring:    Writestring(string(FAddr^.Vstring));
      varOleStr:    Writestring(string(FAddr^.Vstring));
      end;
    end;
  end;
begin
  (* Extract datatype & exclude array info *)
  FKind:=VarType(Value) and varTypeMask;

  (* Write variant header *)
  WriteWord(CNT_HEX_VARIANT_HEADER);

  (* write datatype *)
  WriteWord(FKind);

  (* Content is array? *)
  FIsArray:=VarIsArray(Value);

  (* write array declaration *)
  if not (FKind in [varEmpty,varNull]) then
  begin
    (* write TRUE if variant is an array *)
    WriteBool(FIsArray);

    (* write each item if array, or just the single one.. *)
    if FIsArray then
    begin
      (* write # of items *)
      FCount:=VarArrayHighBound(Value,1) - VarArrayLowBound(Value,1) + 1;
      WriteInt(FCount);

      (* write each element in array *)
      for x:=VarArrayLowBound(Value,1) to VarArrayHighBound(Value,1) do
      WriteVariantData(VarArrayGet(Value,[1,x-1]));

    end else
    WriteVariantData(Value);
  end;
end;
{$ENDIF}

procedure TDbLibWriter.Writebyte(const Value: byte);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteShort(const Value: shortint);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteBool(const Value: boolean);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteWord(const Value: Word);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteSmall(const Value: smallint);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WritePointer(const Value: pointer);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteInt(const Value: integer);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteLong(const Value: cardinal);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteGUID(const Value: TGUID);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteInt64(const Value: Int64);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteCurrency(const Value: Currency);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteSingle(const Value: single);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteDouble(const Value: double);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteDateTime(const Value: TDateTime);
begin
  if Write(Value, SizeOf(Value)) < SizeOf(Value) then
    raise EDbLibWriter.Create(ERR_HEX_WRITER_FAILEDWRITE);
end;

procedure TDbLibWriter.WriteFile(const Filename: string);
var
  FFile: TFileStream;
begin
  FFile := TFileStream.Create(Filename,fmOpenRead or fmShareDenyNone);
  try
    WriteStreamContent(FFile,False);
  finally
    FFile.free;
  end;
end;

procedure TDbLibWriter.WriteStreamContent(const Content: TStream;
  const Disposable: boolean = False);
var
  FTotal:     Int64;
  FRead:      integer;
  FWritten:   integer;
  FCache:     TDbLibIOCache;
begin
  if Content <> nil then
  begin
    try
      FTotal := Content.Size;
      if FTotal >0 then
      begin
        Content.Position := 0;
        Repeat
          FRead := Content.Read(FCache, SizeOf(FCache));
          if FRead>0 then
          begin
            FWritten := Write(FCache, FRead);
            FTotal := FTotal - FWritten;
          end;
        Until (FRead<1) or (FTotal<1);
      end;
    finally
      if Disposable then
        Content.free;
    end;
  end else
    raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

procedure TDbLibWriter.WriteDataContent(const Content: TDbLibBuffer;
  const Disposable: boolean = false);
var
  Fbytes:   integer;
  FRead:    integer;
  FWritten: integer;
  FOffset:  integer;
  FCache:   TDbLibIOCache;
begin
  if Content <> nil then
  begin
    try
      FOffset := 0;
      Fbytes := Content.Size;

      Repeat
        FRead := Content.Read(FOffset, SizeOf(FCache), FCache);
        if FRead > 0 then
        begin
          FWritten := Write(FCache, FRead);
          Fbytes := Fbytes - FWritten;
          FOffset := FOffset + FWritten;
        end;
      Until (FRead<1) or (Fbytes<1);

    finally
      if Disposable then
        Content.free;
    end;
  end else
    raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

procedure TDbLibWriter.WriteData(const Data: TDbLibBuffer;
  const Disposable: boolean);
var
  FTemp:  Int64;
begin
  if (Data <> nil) then
  begin
    try
      FTemp := Data.Size;
      WriteInt64(FTemp);
      if FTemp>0 then
        WriteDataContent(Data);
    finally
      if Disposable then
      Data.free;
    end;
  end else
  raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

procedure TDbLibWriter.WriteStream(const Value: TStream;
          const Disposable: boolean);
begin
  if Value <> nil then
  begin
    try
      WriteInt64(Value.Size);
      if Value.Size > 0 then
        WriteStreamContent(Value);
    finally
      if Disposable then
        Value.free;
    end;
  end else
  raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

function TDbLibWriter.CopyFrom(const Reader:TDbLibReader;
          DataLen:Int64): Int64;
var
  FRead:        integer;
  FbytesToRead: integer;
  FCache:       TDbLibIOCache;
begin
  if Reader<>NIL then
  begin
    result:=0;
    While DataLen>0 do
    begin
      FbytesToRead:=EnsureRange(SizeOf(FCache),0,DataLen);

      FRead:=Reader.Read(FCache,FbytesToRead);
      if FRead>0 then
      begin
        Write(FCache,FRead);
        DataLen:=DataLen - FRead;
        result:=result + FRead;
      end else
      Break;
    end;
  end else
  raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

function TDbLibWriter.CopyFrom(const Stream: TStream;
         const DataLen:Int64): Int64;
var
  FReader: TDbLibReaderStream;
begin
  if Stream<>NIL then
  begin
    FReader:=TDbLibReaderStream.Create(Stream);
    try
      result:=CopyFrom(FReader,DataLen);
    finally
      FReader.free;
    end;
  end else
  raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

function TDbLibWriter.CopyFrom(const Data:TDbLibBuffer;
         const DataLen:Int64): Int64;
var
  FReader: TDbLibReaderBuffer;
begin
  if Data<>NIL then
  begin
    FReader:=TDbLibReaderBuffer.Create(Data);
    try
      result:=CopyFrom(FReader,DataLen);
    finally
      FReader.free;
    end;
  end else
  raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDDATASOURCE);
end;

procedure TDbLibWriter.__FillWord(dstAddr: pword;
  const inCount: integer; const Value: word);
var
  FTemp:  cardinal;
  FLongs: integer;
begin
  FTemp := cardinal(Value shl 16 or Value);
  FLongs := cardinal(inCount shr 3);
  while FLongs > 0 do
  begin
    Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
    Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
    Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
    Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
    dec(FLongs);
  end;

  Case inCount mod 8 of
  1:  dstAddr^:=Value;
  2:  Pcardinal(dstAddr)^:=FTemp;
  3:  begin
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        dstAddr^:=Value;
      end;
  4:  begin
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        Pcardinal(dstAddr)^:=FTemp;
      end;
  5:  begin
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        dstAddr^:=Value;
      end;
  6:  begin
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        Pcardinal(dstAddr)^:=FTemp;
      end;
  7:  begin
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        Pcardinal(dstAddr)^:=FTemp; inc(Pcardinal(dstAddr));
        dstAddr^:=Value;
      end;
  end;
end;

procedure TDbLibWriter.WriteCRLF(const Times: integer=1);
var
  FLen:   integer;
  FWord:  Word;
  FData:  Pointer;
begin
  if Times>0 then
  begin
    FWord:=2573; // [#13,#10]

    if Times=1 then
    Write(FWord,SizeOf(FWord)) else

    if Times=2 then
    begin
      Write(FWord,SizeOf(FWord));
      Write(FWord,SizeOf(FWord));
    end else

    if Times>2 then
    begin
      FLen:=SizeOf(FWord) * Times;
      FData:=Allocmem(FLen);
      try
        __FillWord(FData, Times, FWord);
        Write(FData^,FLen);
      finally
        FreeMem(FData);
      end;
    end;
  end;
end;

procedure TDbLibWriter.Writestring(const Text: string);
begin
  Writestring(Text, FEncoding);
end;

procedure TDbLibWriter.Writestring(const Text: string;
    const Encoding: TEncoding);
var
  LBytes: TBytes;
  LSize:  integer;
begin
  if Encoding = TEncoding.ANSI then
  begin
    LBytes := TEncoding.ANSI.GetBytes(Text);
    LSize := length(LBytes);
    WriteWord(CNT_HEX_ANSI_STRING_HEADER);
    WriteInt(LSize);
    if LSize>0 then
    Write(LBytes[0],LSize);
  end else
  if Encoding = TEncoding.ASCII then
  begin
    LBytes := TEncoding.ASCII.GetBytes(Text);
    LSize := length(LBytes);
    WriteWord(CNT_HEX_ASCI_STRING_HEADER);
    WriteInt(LSize);
    if LSize>0 then
    Write(LBytes[0],LSize);
  end else
  if Encoding = TEncoding.Unicode then
  begin
    LBytes := TEncoding.Unicode.GetBytes(Text);
    LSize := length(LBytes);
    WriteWord(CNT_HEX_UNIC_STRING_HEADER);
    WriteInt(LSize);
    if LSize>0 then
    Write(LBytes[0],LSize);
  end else
  if Encoding = TEncoding.UTF7 then
  begin
    LBytes := TEncoding.UTF7.GetBytes(Text);
    LSize := length(LBytes);
    WriteWord(CNT_HEX_UTF7_STRING_HEADER);
    WriteInt(LSize);
    if LSize>0 then
    Write(LBytes[0],LSize);
  end else
  if Encoding = TEncoding.UTF8 then
  begin
    LBytes := TEncoding.UTF8.GetBytes(Text);
    LSize := length(LBytes);
    WriteWord(CNT_HEX_UTF8_STRING_HEADER);
    WriteInt(LSize);
    if LSize>0 then
    Write(LBytes[0],LSize);
  end else
  if Encoding = TEncoding.Default then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    LSize := length(LBytes);
    WriteWord(CNT_HEX_DEFA_STRING_HEADER);
    WriteInt(LSize);
    if LSize>0 then
    Write(LBytes[0],LSize);
  end;
end;

//##########################################################################
// TDbLibReaderBuffer
//##########################################################################

constructor TDbLibReaderBuffer.Create(const Source:TDbLibBuffer);
begin
  inherited Create;
  TextEncoding := TEncoding.Default;

  if Source = nil then
    raise EDbLibReader.Create(ERR_HEX_READER_INVALIDSOURCE) else

  FData:=Source;
end;

function TDbLibReaderBuffer.Read(var Data;DataLen:integer):  integer;
begin
  if DataLen > 0 then
  begin
    result:=FData.Read(Position,DataLen,data);
    Advance(result);
  end else
    result := 0;
end;

//##########################################################################
// TDbLibWriterBuffer
//##########################################################################

constructor TDbLibWriterBuffer.Create(const Target:TDbLibBuffer);
begin
  inherited Create;
  TextEncoding := TEncoding.Default;
  if Target <> nil then
    FData := Target
  else
    raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDTARGET);
end;

function TDbLibWriterBuffer.Write(const Data; DataLen: integer):  integer;
var
  FTotal: Int64;
begin
  if DataLen > 0 then
  begin
    FTotal := FData.Size;

    if FTotal > 0 then
    begin
      if Position < FTotal then
        FData.Write(Position, DataLen, Data)
      else
        FData.Append(Data,DataLen);
    end else
      FData.Append(Data,DataLen);

    { if ( (FTotal > 0) and (Position < FTotal) ) then
      FData.Write(Position, DataLen, Data)
    else
      FData.Append(Data,DataLen);   }

    Advance(DataLen);
    result:=DataLen;
  end else
  result:=0;
end;

//##########################################################################
// TDbLibReaderStream
//##########################################################################

constructor TDbLibReaderStream.Create(const Source: TStream);
begin
  inherited Create;
  TextEncoding := TEncoding.Default;
  if source <> nil then
    FStream := Source
  else
    raise EDbLibReader.Create(ERR_HEX_READER_INVALIDSOURCE);
end;

function TDbLibReaderStream.Read(var Data; DataLen: integer):  integer;
begin
  result := 0;
  if DataLen > 0 then
  begin
    result := FStream.Read(Data, DataLen);
    Advance(result);
  end;
end;

//##########################################################################
// TDbLibWriterStream
//##########################################################################

constructor TDbLibWriterStream.Create(const Target: TStream);
begin
  inherited Create;
  TextEncoding := TEncoding.Default;
  if Target <> nil then
    FStream := target
  else
    raise EDbLibWriter.Create(ERR_HEX_WRITER_INVALIDTARGET);
end;

function TDbLibWriterStream.Write(const Data; DataLen: integer):  integer;
begin
  result := 0;
  if DataLen > 0 then
  begin
    result := FStream.Write(Data, DataLen);
    Advance(result);
  end;
end;


end.

